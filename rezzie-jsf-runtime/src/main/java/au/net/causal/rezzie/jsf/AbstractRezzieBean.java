package au.net.causal.rezzie.jsf;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Locale;

import javax.faces.context.FacesContext;

import au.net.causal.rezzie.Rezzie;

public abstract class AbstractRezzieBean implements Serializable
{
	private transient Rezzie rezzie;
	private Locale locale;
	
	protected AbstractRezzieBean()
	{
		//TODO we can cache bundles and rezzie later
		locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		rezzie = new Rezzie();
	}
	
	private void readObject(ObjectInputStream stream)
	throws IOException, ClassNotFoundException 
	{
		stream.defaultReadObject();
		rezzie = new Rezzie(); //recreate rezzie
	}
	
	protected <B> B getBundle(Class<B> bundleType)
	{
		return(rezzie.getBundle(bundleType, locale));
	}
}
