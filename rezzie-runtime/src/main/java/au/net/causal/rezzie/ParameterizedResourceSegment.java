package au.net.causal.rezzie;

import java.util.Locale;

import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.ParameterProcessor;

public class ParameterizedResourceSegment<P, C>
{
	private final ParameterProcessor<P, C> parameterProcessor;
	private final C configuration;
	private final ParameterMetadata<P> metadata;
	
	public ParameterizedResourceSegment(ParameterProcessor<P, C> parameterProcessor, C configuration, ParameterMetadata<P> metadata)
	{
		if (parameterProcessor == null)
			throw new NullPointerException("parameterProcessor == null");
		if (configuration == null)
			throw new NullPointerException("configuration == null");
		if (metadata == null)
			throw new NullPointerException("metadata == null");
		
		this.parameterProcessor = parameterProcessor;
		this.configuration = configuration;
		this.metadata = metadata;
	}

	public String parameterToString(P parameter, Locale locale)
	{
		return(parameterProcessor.parameterToString(parameter, configuration, metadata, locale));
	}
	
	public Class<P> getParameterType()
	{
		return(parameterProcessor.getParameterType());
	}
}
