package au.net.causal.rezzie.testing;

import javax.xml.bind.annotation.XmlValue;

public class MyItem
{
	@XmlValue
	public String value;
}
