package au.net.causal.rezzie;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IllformedLocaleException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import au.net.causal.rezzie.locator.RezzieResourceLocator;

public class Rezzie
{
	private final BundleReader bundleReader = new BundleReader();
	private final RezzieResourceLocator resourceLocator = new RezzieResourceLocator();
	private final LocaleLookup defaultLocaleLookup = new LocaleLookup();
	private final DocumentBuilderFactory documentBuilderFactory;
	
	/**
	 * Used for looking up bundle parents.
	 */
	private final ResourceBundle.Control bundleControl = ResourceBundle.Control.getControl(ResourceBundle.Control.FORMAT_DEFAULT);
	
	public Rezzie()
	{
		documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
	}
	
	public <B> B getBundle(Class<B> bundleType, Locale locale)
	{
		if (bundleType == null)
			throw new NullPointerException("bundleType == null");
		if (locale == null)
			throw new NullPointerException("locale == null");
		
		List<? extends RezzieBundle> bundles = createBundles(bundleType, locale, true);
		return(bundleType.cast(Proxy.newProxyInstance(bundleType.getClassLoader(), new Class<?>[] {bundleType}, new RezzieBundleHandler(locale, bundles))));
	}
	
	public Set<Locale> getSupportedLocales(Class<?> bundleType)
	{
		if (bundleType == null)
			throw new NullPointerException("bundleType == null");
		
		return(getSupportedLocales(Collections.singleton(bundleType)));
	}
	
	public Set<Locale> getSupportedLocales(Class<?> bundleType, Class<?>... additionalBundleTypes)
	{
		if (bundleType == null)
			throw new NullPointerException("bundleType == null");
		if (additionalBundleTypes == null)
			throw new NullPointerException("additionalBundleTypes == null");
		
		Set<Locale> locales = new HashSet<>();
		locales.addAll(getSupportedLocales(bundleType));
		for (Class<?> additionalBundleType : additionalBundleTypes)
		{
			locales.addAll(getSupportedLocales(additionalBundleType));
		}
		return(locales);
	}
	
	public Set<Locale> getSupportedLocales(Collection<? extends Class<?>> bundleTypes)
	{
		if (bundleTypes == null)
			throw new NullPointerException("bundleTypes == null");
		
		Set<Locale> locales = new HashSet<>();
		for (Class<?> bundleType : bundleTypes)
		{
			for (Locale locale : Locale.getAvailableLocales())
			{
				if (isLocaleSupportedDirectly(bundleType, locale))
					locales.add(locale);
			}
		}
		
		return(locales);
	}
	
	public boolean isLocaleSupportedDirectly(Class<?> bundleType, Locale locale)
	{
		if (bundleType == null)
			throw new NullPointerException("bundleType == null");
		if (locale == null)
			throw new NullPointerException("locale == null");
		
		//If the bundle exists then we have support
		String bundleXmlName = resourceLocator.getBundleName(bundleType, locale);
		URL bundleXmlUrl = bundleType.getResource("/" + bundleXmlName);
		if (bundleXmlUrl != null)
			return(true);
		
		//If a remapping exists then we have support
		String remapName = resourceLocator.getRemapName(bundleType, locale);
		URL remapUrl = bundleType.getResource("/" + remapName);
		if (remapUrl != null)
			return(true);
		
		//If we get here then there is no direct support
		return(false);
	}
	
	public boolean isLocaleSupported(Class<?> bundleType, Locale locale)
	{
		if (bundleType == null)
			throw new NullPointerException("bundleType == null");
		if (locale == null)
			throw new NullPointerException("locale == null");
		
		List<Locale> localeCandidates = getCandidateLocales(bundleType.getCanonicalName(), locale);
		for (Locale localeCandidate : localeCandidates)
		{
			if (isLocaleSupportedDirectly(bundleType, localeCandidate))
				return(true);
		}
		
		//No support
		return(false);
	}
	
	private List<RezzieBundle> createBundles(Class<?> bundleInterface, Locale locale, boolean doDefaultLocaleLookup)
	{
		//Load schema
		String schemaName = resourceLocator.getCompleteSchemaName(bundleInterface);
		URL schemaUrl = bundleInterface.getResource("/" + schemaName);
		if (schemaUrl == null)
			throw new RuntimeException("Schema " + schemaName + " not found."); //TODO exception handling
		Document schemaDoc;
		try
		{
			schemaDoc = documentBuilderFactory.newDocumentBuilder().parse(schemaUrl.toExternalForm());
		}
		catch (IOException | ParserConfigurationException | SAXException e)
		{
			throw new RuntimeException("Error loading schema: " + e, e);
		}
		
		//Load XML for the locale TODO need a smarter locale lookup
		List<Locale> localeCandidates = getCandidateLocales(bundleInterface.getCanonicalName(), locale);

		List<RezzieBundle> bundles = new ArrayList<>();
		for (Locale localeCandidate : localeCandidates)
		{
			//Look up resource for this candidate if it exists
			String candidateBundleXmlName = resourceLocator.getBundleName(bundleInterface, localeCandidate);
			URL candidateBundleXmlUrl = bundleInterface.getResource("/" + candidateBundleXmlName);
			if (candidateBundleXmlUrl != null)
			{
				Document xmlDoc;
				try
				{
					xmlDoc = documentBuilderFactory.newDocumentBuilder().parse(candidateBundleXmlUrl.toExternalForm());
				}
				catch (IOException | ParserConfigurationException | SAXException e)
				{
					throw new RuntimeException("Error loading XML: " + e, e); //TODO
				}
				
				try
				{
					RezzieBundle candidateBundle = bundleReader.readFromXml(xmlDoc, schemaDoc, bundleInterface);
					bundles.add(candidateBundle);
				}
				catch (JAXBException | XPathException | XMLStreamException e)
				{
					throw new RuntimeException("Error loading bundle: " + e, e); //TODO
				}
			}
			else
			{
				//There might be a remap file
				String candidateRemapName = resourceLocator.getRemapName(bundleInterface, localeCandidate);
				URL candidateRemapUrl = bundleInterface.getResource("/" + candidateRemapName);
				if (candidateRemapUrl != null)
				{
					try
					{
						Locale remapLocale = readRemapLocale(candidateRemapUrl);
						return(createBundles(bundleInterface, remapLocale, doDefaultLocaleLookup));
					}
					catch (IOException e)
					{
						throw new RuntimeException("Error loading remap file " + candidateRemapUrl.toExternalForm() + ": " + e.getMessage(), e); //TODO better exception
					}
				}
			}
		}
		
		//If no bundles were found for the specified locale, see if there is a default locale for this bundle and use that
		if (bundles.isEmpty() && doDefaultLocaleLookup)
		{
			Locale defaultLocale = defaultLocaleLookup.lookUpDefaultLocale(bundleInterface);
			if (defaultLocale != null)
				bundles = createBundles(bundleInterface, defaultLocale, false);
		}

		if (bundles.isEmpty())
			throw new RuntimeException("No bundles found for " + bundleInterface.getCanonicalName() + ", locale " + locale);
		
		return(bundles);
	}
	
	private Locale readRemapLocale(URL remapFile)
	throws IOException
	{
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(remapFile.openStream(), "UTF-8")))
		{
			String s = reader.readLine();
			if (s == null)
				throw new IllformedLocaleException("Remap file " + remapFile.toExternalForm() + " does not have any content.");
			
			s = s.trim();
			
			try
			{
				Locale locale = new Locale.Builder().setLanguageTag(s).build();
				return(locale);
			}
			catch (IllformedLocaleException e)
			{
				//Add the remap file information
				IllformedLocaleException ex = new IllformedLocaleException(remapFile.toExternalForm() + ": " + e.getMessage());
				ex.initCause(e);
				throw ex;
			}
		}
	}
	
	/**
	 * Returns a list of locales, from most specific to least specific that can be used for looking up resources for <code>locale</code>.
	 * See {@link ResourceBundle.Control#getCandidateLocales(String, Locale)} for more details on how this works.
	 * 
	 * Base name doesn't really matter but RB's API uses it so we might as well pass it in.
	 * The resource bundle doesn't actually have to exist at all.
	 * 
	 * @param baseName name of the resource bundle.  Not used.
	 * @param locale the locale.
	 * 
	 * @return a list of locales.
	 * 
	 * @see ResourceBundle.Control#getCandidateLocales(String, Locale)
	 */
	private List<Locale> getCandidateLocales(String baseName, Locale locale)
	{
		return(bundleControl.getCandidateLocales(baseName, locale));
	}

}
