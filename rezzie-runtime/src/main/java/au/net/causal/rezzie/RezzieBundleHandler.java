package au.net.causal.rezzie;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import com.google.common.collect.ImmutableList;

public class RezzieBundleHandler implements InvocationHandler
{
	private final Locale locale;
	private final List<? extends RezzieBundle> bundles;

	public RezzieBundleHandler(Locale locale, List<? extends RezzieBundle> bundles)
	{
		if (locale == null)
			throw new NullPointerException("locale == null");
		if (bundles == null)
			throw new NullPointerException("bundles == null");
		
		this.locale = locale;
		this.bundles = ImmutableList.copyOf(bundles);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		String itemName = method.getName();
		
		for (RezzieBundle bundle : bundles)
		{
			String text = bundle.getText(itemName, locale, args);
			if (text != null)
				return(text);
		}
		
		//TODO
		throw new RuntimeException("No resource bundle message for '" + itemName + "' in " +  method.getDeclaringClass().getCanonicalName() + ".");
	}

}
