package au.net.causal.rezzie;

import java.util.Locale;

import au.net.causal.rezzie.annotation.DefaultLocale;
import au.net.causal.rezzie.annotation.NoDefaultLocale;

/**
 * Looks up default locales for Rezzie bundles.
 * 
 * @author prunge
 */
public class LocaleLookup
{
	public Locale lookUpDefaultLocale(Class<?> rezzieBundleClass)
	{
		DefaultLocale dLocale = rezzieBundleClass.getAnnotation(DefaultLocale.class);
		NoDefaultLocale ndLocale = rezzieBundleClass.getAnnotation(NoDefaultLocale.class);
		
		//If not found on class, attempt lookup on package
		if (dLocale == null && ndLocale == null)
		{
			Package pkg = rezzieBundleClass.getPackage();
			if (pkg != null)
			{
				dLocale = pkg.getAnnotation(DefaultLocale.class);
				ndLocale = pkg.getAnnotation(NoDefaultLocale.class);
			}
		}
		
		if (dLocale != null)
			return(new Locale.Builder().setLanguage(dLocale.language()).setRegion(dLocale.country()).setVariant(dLocale.variant()).build());
		
		//No default locale set
		return(null);
	}
}
