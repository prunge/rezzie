package au.net.causal.rezzie;

import java.util.Locale;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class RezzieBundle
{
	private final Map<String, ? extends BundleItem> resourceMap;
	
	public RezzieBundle(Map<String, ? extends BundleItem> resourceMap)
	{
		if (resourceMap == null)
			throw new NullPointerException("resourceMap == null");
		
		this.resourceMap = ImmutableMap.copyOf(resourceMap);
	}
	
	public String getText(String itemName, Locale locale, Object... args)
	{
		BundleItem item = resourceMap.get(itemName);
		if (item == null)
			return(null);
		
		return(item.getText(locale, args));
	}
}
