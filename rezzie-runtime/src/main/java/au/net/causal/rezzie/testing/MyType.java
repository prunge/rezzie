package au.net.causal.rezzie.testing;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import au.net.causal.rezzie.parameter.annotation.SubstituteWithEnum;

@XmlRootElement
public class MyType
{
	private String name;
	private String item;

	/**
	 * The name of something.
	 * 
	 * @return the name of something.
	 */
	@XmlAttribute
	public String getName()
	{
		return(name);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@XmlAttribute(required=true)
	@SubstituteWithEnum
	public String getItem()
	{
		return(item);
	}

	public void setItem(String item)
	{
		this.item = item;
	}
	
	
}
