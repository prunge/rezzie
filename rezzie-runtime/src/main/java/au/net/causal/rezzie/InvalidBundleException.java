package au.net.causal.rezzie;

public class InvalidBundleException extends RuntimeException
{

	public InvalidBundleException()
	{
	}

	public InvalidBundleException(String message)
	{
		super(message);
	}
}
