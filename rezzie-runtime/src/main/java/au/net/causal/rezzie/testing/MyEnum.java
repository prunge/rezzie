package au.net.causal.rezzie.testing;

import javax.xml.bind.annotation.XmlEnumValue;

public enum MyEnum
{
	@XmlEnumValue("one")
	ONE,
	TWO,
	THREE;
}
