package au.net.causal.rezzie;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;

import org.apache.ws.commons.util.NamespaceContextImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import au.net.causal.rezzie.annotation.WhiteSpace;
import au.net.causal.rezzie.annotation.WhiteSpaceMode;
import au.net.causal.rezzie.parameter.ChoiceSupport;
import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.ParameterMetadataImpl;
import au.net.causal.rezzie.parameter.ParameterProcessor;
import au.net.causal.rezzie.parameter.ParameterProcessorRegistry;
import au.net.causal.rezzie.reflection.TypeUtils;

public class BundleReader
{	
	private static final Logger log = LoggerFactory.getLogger(BundleReader.class);
	
	private static final WhiteSpaceMode DEFAULT_PARAMETERS_WHITE_SPACE_MODE = WhiteSpaceMode.PRESERVE;
	private static final WhiteSpaceMode DEFAULT_TEXT_WHITE_SPACE_MODE = WhiteSpaceMode.COLLAPSE;
	
	private final XPath xPath;
	private final ParameterProcessorRegistry parameterProcessorRegistry = new ParameterProcessorRegistry(); //TODO classloading?

	public BundleReader()
	{
		xPath = XPathFactory.newInstance().newXPath();
		NamespaceContextImpl nc = new NamespaceContextImpl();
		nc.startPrefixMapping("xsd", XMLConstants.W3C_XML_SCHEMA_NS_URI);
		xPath.setNamespaceContext(nc);
	}
	
	public RezzieBundle readFromXml(Document xmlDoc, Document schemaDoc, Class<?> bundleInterface)
	throws XMLStreamException, JAXBException, XPathException
	{
		if (xmlDoc == null)
			throw new NullPointerException("xmlDoc == null");
		if (schemaDoc == null)
			throw new NullPointerException("schemaDoc == null");
		
		Map<String, BundleItem> resourceMap = new LinkedHashMap<>();
		
		NodeList childList = xmlDoc.getDocumentElement().getChildNodes();
		for (int i = 0; i < childList.getLength(); i++)
		{
			Node node = childList.item(i);
			if (node instanceof Element)
			{
				Element itemElement = (Element)node;
				String itemName = itemElement.getLocalName();
				Method bundleMethod = findMethodWithName(itemName, bundleInterface);
				if (bundleMethod == null)
					throw new RuntimeException("No corresponding method for bundle item '" + itemName + "' in " + bundleInterface.getCanonicalName());
				
				BundleItem item = readItemXml(itemElement.getLocalName(), itemElement, itemElement, schemaDoc, bundleMethod);
				resourceMap.put(item.getName(), item);
			}
		}
		
		return(new RezzieBundle(resourceMap));
	}
	
	private BundleItem readItemXml(String bundleItemName, Element itemElement, Element baseItemElement, Document schemaDoc, Method bundleMethod)
	throws JAXBException, XPathException
	{
		WhiteSpace whiteSpaceConfig = readWhiteSpaceConfiguration(baseItemElement, bundleMethod);
		
		if (bundleMethod.getParameterTypes().length == 0)
			return(new BundleItem(bundleItemName, Collections.singletonList(itemElement.getTextContent()), whiteSpaceConfig.text(), whiteSpaceConfig.parameters()));

		//Find this item in the schema
		String elementTypeName = xPath.evaluate("xsd:schema/xsd:complexType[@name='BundleType']/xsd:all/xsd:element[@name='" + bundleItemName + "']/@type", schemaDoc);
		
		if ("".equals(elementTypeName))
			elementTypeName = null;
		if (elementTypeName == null)
			throw new RuntimeException("Could not find type for element '" + bundleItemName + "'.");
		elementTypeName = discardPrefix(elementTypeName);
		
		String groupName = xPath.evaluate("xsd:schema/xsd:complexType[@name='" + elementTypeName + "']/xsd:group/@ref", schemaDoc);
		if ("".equals(groupName))
			groupName = null;
		if (groupName == null)
			throw new RuntimeException("Could not find group for element '" + bundleItemName + "'.");
		groupName = discardPrefix(groupName);
		
		NodeList schemaParameters =  (NodeList)xPath.evaluate("xsd:schema/xsd:group[@name='" + groupName + "']/xsd:choice/xsd:element", schemaDoc, XPathConstants.NODESET);
		
		//For choices there will be an extra element at the end but this will never correspond to an arg so it's OK to ignore
		
		//If it has no parameters then nothing will be found
		if (schemaParameters == null || schemaParameters.getLength() == 0)
			throw new RuntimeException("No schema parameters found but expected " + bundleMethod.getParameterTypes().length);
		
		List<String> parameterNames = new ArrayList<>();
		for (int i = 0; i < schemaParameters.getLength(); i++)
		{
			Element schemaParameterElement = (Element)schemaParameters.item(i);
			String parameterName = schemaParameterElement.getAttribute("name");
			parameterNames.add(parameterName);
		}
		
		//Look at the XML itself which will contain a mixture of text and elements
		NodeList itemChildren = itemElement.getChildNodes();
		List<Object> bundleSegments = new ArrayList<>();
		for (int i = 0; i < itemChildren.getLength(); i++)
		{
			Node itemChild = itemChildren.item(i);
			if (itemChild instanceof Text)
			{
				Text itemText = (Text)itemChild;
				String s = itemText.getTextContent();
				bundleSegments.add(s);
			}
			else if (itemChild instanceof Element)
			{
				Element childElement = (Element)itemChild;
				if ("choice".equals(childElement.getLocalName()))
				{
					ChoiceSegment choice = buildChoiceSegment(bundleItemName, childElement, baseItemElement, parameterNames, bundleMethod, schemaDoc);
					bundleSegments.add(choice);
				}
				else
				{
					Element itemParameterElement = (Element)itemChild;
					String itemParameterName = itemParameterElement.getLocalName();
					int parameterIndex = parameterNames.indexOf(itemParameterName);
					
					Class<?> parameterType = bundleMethod.getParameterTypes()[parameterIndex];
					if (parameterType.isPrimitive())
						parameterType = TypeUtils.wrapperType(parameterType);
					
					ParameterProcessor<?, ?> processor = parameterProcessorRegistry.getProcessor(parameterType);
					if (processor == null)
						throw new RuntimeException("No processor found for parameter type " + parameterType + " for " + bundleMethod.toGenericString());
					
					//Build the configuration
					ParameterizedResourceSegment<?, ?> segment = buildResourceSegment(processor, itemParameterElement, bundleMethod, parameterIndex);
					bundleSegments.add(segment);
				}
			}
				
		}
		
		return(new BundleItem(bundleItemName, bundleSegments, whiteSpaceConfig.text(), whiteSpaceConfig.parameters()));
	}
	
	private WhiteSpace readWhiteSpaceConfiguration(Element itemElement, Method bundleMethod)
	{
		WhiteSpaceMode parameters = null;
		WhiteSpaceMode text = null;
		
		//First read from XML if it is specified
		String textWhiteSpaceXml = itemElement.getAttribute("whiteSpace");
		String parameterWhiteSpaceXml = itemElement.getAttribute("parameterWhiteSpace");
		if (textWhiteSpaceXml == null || textWhiteSpaceXml.isEmpty())
			textWhiteSpaceXml = itemElement.getOwnerDocument().getDocumentElement().getAttribute("whiteSpace");
		if (parameterWhiteSpaceXml == null || parameterWhiteSpaceXml.isEmpty())
			parameterWhiteSpaceXml = itemElement.getOwnerDocument().getDocumentElement().getAttribute("parameterWhiteSpace");
		
		try
		{
			if (textWhiteSpaceXml != null && !textWhiteSpaceXml.isEmpty())
				text = WhiteSpaceMode.valueOf(textWhiteSpaceXml.toUpperCase(Locale.ENGLISH));
		}
		catch (IllegalArgumentException e)
		{
			log.warn("Invalid enum value '" + textWhiteSpaceXml + "' for whiteSpace attribute");
			//Just ignore, schema validation gives the user error anyway
		}
		try
		{
			if (parameterWhiteSpaceXml != null && !parameterWhiteSpaceXml.isEmpty())
				parameters = WhiteSpaceMode.valueOf(parameterWhiteSpaceXml.toUpperCase(Locale.ENGLISH));
		}
		catch (IllegalArgumentException e)
		{
			log.warn("Invalid enum value '" + parameterWhiteSpaceXml + "' for parameterWhiteSpace attribute");
			//Just ignore, schema validation gives the user error anyway
		}
			
		//If not specified, read from annotations, working up from method to package
		WhiteSpace methodWhiteSpace = bundleMethod.getAnnotation(WhiteSpace.class);
		if (methodWhiteSpace != null)
		{
			if (parameters == null)
				parameters = methodWhiteSpace.parameters();
			if (text == null)
				text = methodWhiteSpace.text();
		}
		if (parameters == WhiteSpaceMode.DEFAULT)
			parameters = null;
		if (text == WhiteSpaceMode.DEFAULT)
			text = null;
		
		Class<?> bundleClass = bundleMethod.getDeclaringClass();
		WhiteSpace bundleClassWhiteSpace = bundleClass.getAnnotation(WhiteSpace.class);
		if (bundleClassWhiteSpace != null)
		{
			if (parameters == null)
				parameters = bundleClassWhiteSpace.parameters();
			if (text == null)
				text = bundleClassWhiteSpace.text();
		}
		if (parameters == WhiteSpaceMode.DEFAULT)
			parameters = null;
		if (text == WhiteSpaceMode.DEFAULT)
			text = null;
		
		Package bundlePackage = bundleClass.getPackage();
		if (bundlePackage != null)
		{
			WhiteSpace bundlePackageWhiteSpace = bundlePackage.getAnnotation(WhiteSpace.class);
			if (bundlePackageWhiteSpace != null)
			{
				if (parameters == null)
					parameters = bundlePackageWhiteSpace.parameters();
				if (text == null)
					text = bundlePackageWhiteSpace.text();
			}
		}
		if (parameters == WhiteSpaceMode.DEFAULT)
			parameters = null;
		if (text == WhiteSpaceMode.DEFAULT)
			text = null;
		
		//If nothing specified use defaults
		if (parameters == null)
			parameters = DEFAULT_PARAMETERS_WHITE_SPACE_MODE;
		if (text == null)
			text = DEFAULT_TEXT_WHITE_SPACE_MODE;
		
		return(new WhiteSpaceConfiguration(text, parameters));
	}

	private static String discardPrefix(String qualifiedName)
	{
		int colonIndex = qualifiedName.indexOf(':');
		if (colonIndex < 0)
			return(qualifiedName);
		
		return(qualifiedName.substring(colonIndex + 1));
	}
	
	private ChoiceSegment buildChoiceSegment(String bundleItemName, Element choiceElement, Element baseItemElement, List<String> parameterNames, Method bundleMethod, Document schemaDoc)
	throws JAXBException, XPathException
	{
		List<ChoiceSegment.ChoiceElement<?, ?>> segmentElements = new ArrayList<>();
		BundleItem otherwiseItem = null;
		
		NodeList childNodes = choiceElement.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			if (node instanceof Element)
			{
				Element itemElement = (Element)node;
				String parameterName = itemElement.getLocalName();
				
				if (!"otherwise".equals(parameterName))
				{
					int parameterIndex = parameterNames.indexOf(parameterName);
					Class<?> parameterType = bundleMethod.getParameterTypes()[parameterIndex];
					ParameterProcessor<?, ?> processor = parameterProcessorRegistry.getProcessor(parameterType);
					if (processor == null)
						throw new RuntimeException("No processor found for parameter type " + parameterType + " for " + bundleMethod.toGenericString());
					else if (!(processor instanceof ChoiceSupport<?, ?>))
						throw new RuntimeException("Choice not supported for parameter type " + parameterType + " for " + bundleMethod.toGenericString());
					
					ChoiceSupport<?, ?> choiceProcessor = (ChoiceSupport<?, ?>)processor;
					ChoiceSegment.ChoiceElement<?, ?> segmentElement = buildChoiceSegmentElement(bundleItemName, choiceProcessor, itemElement, baseItemElement, parameterIndex, bundleMethod, schemaDoc);
					segmentElements.add(segmentElement);
				}
				else
					otherwiseItem = readItemXml(bundleItemName, itemElement, baseItemElement, schemaDoc, bundleMethod);
			}
		}
		
		return(new ChoiceSegment(segmentElements, otherwiseItem));
	}
	
	private <P, C> ChoiceSegment.ChoiceElement<P, C> buildChoiceSegmentElement(String bundleItemName, ChoiceSupport<P, C> choiceProcessor, Element itemElement, Element baseItemElement, int parameterIndex, 
														Method bundleMethod, Document schemaDoc)
	throws JAXBException, XPathException
	{
		BundleItem content = readItemXml(bundleItemName, itemElement, baseItemElement, schemaDoc, bundleMethod);
		
		//TODO cache JAXB contexts
		//TODO pass the schema in for additional validation
		JAXBContext jaxbContext = JAXBContext.newInstance(choiceProcessor.getChoiceConfigurationType());
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		//Remove all body content from the existing element since that is what the JAXB config object expects
		Node bodylessElement = itemElement.cloneNode(true);
		NodeList bodylessChildren = bodylessElement.getChildNodes();
		
		//Iterate reverse order so removal doesn't affect the index
		for (int i = bodylessChildren.getLength() - 1; i >= 0; i--)
		{
			Node child = bodylessChildren.item(i);
			bodylessElement.removeChild(child);
		}
		
		JAXBElement<C> rawConfig = unmarshaller.unmarshal(bodylessElement, choiceProcessor.getChoiceConfigurationType());
		C choiceConfiguration = rawConfig.getValue();
		
		ChoiceSegment.ChoiceElement<P, C> choiceElement = new ChoiceSegment.ChoiceElement<>(choiceProcessor, choiceConfiguration, parameterIndex, content, bundleMethod);
		
		return(choiceElement);
	}
	
	private <P, C> ParameterizedResourceSegment<P, C> buildResourceSegment(ParameterProcessor<P, C> processor, Element parameterElement,
														Method bundleMethod, int parameterIndex)
	throws JAXBException
	{
		//Build metadata
		ParameterMetadata<P> metadata = new ParameterMetadataImpl<>(bundleMethod, parameterIndex, processor.getParameterType());
		
		//TODO cache JAXB contexts
		//TODO pass the schema in for additional validation
		JAXBContext jaxbContext = JAXBContext.newInstance(processor.getConfigurationType());
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		JAXBElement<C> rawConfig = unmarshaller.unmarshal(parameterElement, processor.getConfigurationType());
		C processorConfig = rawConfig.getValue();
		
		ParameterizedResourceSegment<P, C> segment = new ParameterizedResourceSegment<>(processor, processorConfig, metadata);
		return(segment);
	}
	
	private Method findMethodWithName(String name, Class<?> clazz)
	{
		for (Method method : clazz.getMethods())
		{
			if (name.equals(method.getName()))
				return(method);
		}
		
		return(null);
	}
	
	/*
	private static String xmlToString(Node node)
	{
		StringWriter w = new StringWriter();
		
		try
		{
			Transformer xform = TransformerFactory.newInstance().newTransformer();
			xform.transform(new DOMSource(node), new StreamResult(w));
			
			return(w.toString());
		}
		catch (TransformerException e)
		{
			throw new RuntimeException(e);
		}
	}
	*/
	
	private static class WhiteSpaceConfiguration implements WhiteSpace
	{
		private final WhiteSpaceMode text;
		private final WhiteSpaceMode parameters;

		public WhiteSpaceConfiguration(WhiteSpaceMode text, WhiteSpaceMode parameters)
		{
			this.text = text;
			this.parameters = parameters;
		}

		@Override
		public Class<? extends Annotation> annotationType()
		{
			return(WhiteSpace.class);
		}

		@Override
		public WhiteSpaceMode text()
		{
			return(text);
		}

		@Override
		public WhiteSpaceMode parameters()
		{
			return(parameters);
		}
	}
}
