package au.net.causal.rezzie;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import au.net.causal.rezzie.annotation.WhiteSpaceMode;

import com.google.common.collect.ImmutableList;

public class BundleItem
{
	private final String name;
	private final WhiteSpaceMode textWhiteSpaceMode;
	private final WhiteSpaceMode parameterWhiteSpaceMode;
	
	/**
	 * Contains {@link String}s, {@link ParameterizedResourceSegment}s and {@link ChoiceSegment}s.
	 */
	private final List<?> segments;
	
	public BundleItem(String name, List<?> segments, WhiteSpaceMode textWhiteSpaceMode, WhiteSpaceMode parameterWhiteSpaceMode)
	{
		if (name == null)
			throw new NullPointerException("name == null");
		if (segments == null)
			throw new NullPointerException("segments == null");
		if (textWhiteSpaceMode == null)
			throw new NullPointerException("textWhiteSpaceMode == null");
		if (parameterWhiteSpaceMode == null)
			throw new NullPointerException("parameterWhiteSpaceMode == null");
		
		this.name = name;
		this.segments = ImmutableList.copyOf(segments);
		this.textWhiteSpaceMode = textWhiteSpaceMode;
		this.parameterWhiteSpaceMode = parameterWhiteSpaceMode;
	}
	
	public String getName()
	{
		return(name);
	}

	public String getText(Locale locale, Object... args)
	{
		StringBuilder buf = new StringBuilder();
		
		int parameterIndex = 0;
		boolean start = true;
		boolean end;
		for (Iterator<?> i = segments.iterator(); i.hasNext();)
		{
			Object segment = i.next();
			end = !i.hasNext();
			
			if (segment instanceof ParameterizedResourceSegment<?, ?>)
			{
				ParameterizedResourceSegment<?, ?> pSegment = (ParameterizedResourceSegment<?, ?>)segment;
				Object parameter = args[parameterIndex];
				parameterIndex++;
				
				String s = invokeSegmentToStringSafely(pSegment, parameter, locale);
				s = parameterWhiteSpaceMode.processString(s, true, true);
				buf.append(s);
			}
			else if (segment instanceof ChoiceSegment)
			{
				ChoiceSegment cSegment = (ChoiceSegment)segment;
				String s = cSegment.getText(locale, args);
				if (s != null)
					buf.append(s);
			}
			else
			{
				String s = segment.toString();
				s = textWhiteSpaceMode.processString(s, start, end);
				buf.append(s);
			}
			
			start = false;
		}
		
		return(buf.toString());
	}
	
	private <P> String invokeSegmentToStringSafely(ParameterizedResourceSegment<P, ?> segment, Object parameter, Locale locale)
	{
		P safeParameter = segment.getParameterType().cast(parameter);
		return(segment.parameterToString(safeParameter, locale));
	}
}
