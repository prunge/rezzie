package au.net.causal.rezzie;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import au.net.causal.rezzie.parameter.ChoiceSupport;
import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.ParameterMetadataImpl;

public class ChoiceSegment
{
	private final List<? extends ChoiceElement<?, ?>> choiceElements;
	private final BundleItem otherwiseElement;
	
	public ChoiceSegment(List<? extends ChoiceElement<?, ?>> choiceElements, BundleItem otherwiseElement)
	{
		if (choiceElements == null)
			throw new NullPointerException("choiceElements == null");
		
		this.choiceElements = new ArrayList<>(choiceElements);
		this.otherwiseElement = otherwiseElement;
	}

	public String getText(Locale locale, Object... args)
	{
		for (ChoiceElement<?, ?> choice : choiceElements)
		{
			if (choice.isApplicable(args))
				return(choice.getText(locale, args));
		}
		
		if (otherwiseElement != null)
			return(otherwiseElement.getText(locale, args));
		
		return(null);
	}

	public static class ChoiceElement<P, C>
	{
		private final ChoiceSupport<P, C> choiceProcessor;
		private final C choiceConfiguration;
		private final int choiceArgIndex;
		private final BundleItem content;
		private final Method bundleMethod;
		
		public ChoiceElement(ChoiceSupport<P, C> choiceProcessor, C choiceConfiguration, int choiceArgIndex, BundleItem content, Method bundleMethod)
		{
			if (choiceProcessor == null)
				throw new NullPointerException("choiceProcessor == null");
			if (choiceConfiguration == null)
				throw new NullPointerException("choiceConfiguration == null");
			if (content == null)
				throw new NullPointerException("content == null");
			if (bundleMethod == null)
				throw new NullPointerException("bundleMethod == null");
			
			this.choiceProcessor = choiceProcessor;
			this.choiceConfiguration = choiceConfiguration;
			this.choiceArgIndex = choiceArgIndex;
			this.content = content;
			this.bundleMethod = bundleMethod;
		}

		public String getText(Locale locale, Object... args)
		{
			return(content.getText(locale, args));
		}
		
		public boolean isApplicable(Object... args)
		{
			Object choiceArg = args[choiceArgIndex];
			P safeChoiceArg = choiceProcessor.getParameterType().cast(choiceArg);
			ParameterMetadata<P> metadata = new ParameterMetadataImpl<>(bundleMethod, choiceArgIndex, choiceProcessor.getParameterType());
			return(choiceProcessor.applyChoice(safeChoiceArg, choiceConfiguration, metadata));
		}
		
	}
}
