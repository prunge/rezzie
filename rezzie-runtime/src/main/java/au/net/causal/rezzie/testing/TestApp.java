package au.net.causal.rezzie.testing;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.jvnet.jaxb.reflection.JAXBModelFactory;
import org.jvnet.jaxb.reflection.model.core.AttributePropertyInfo;
import org.jvnet.jaxb.reflection.model.core.ClassInfo;
import org.jvnet.jaxb.reflection.model.core.EnumConstant;
import org.jvnet.jaxb.reflection.model.core.PropertyInfo;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeEnumLeafInfo;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeNonElement;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeTypeInfoSet;

import au.net.causal.rezzie.parameter.annotation.SubstituteWithEnum;

public class TestApp
{
	public static void main(String... args)
	throws Exception
	{
		//JAXBContext context = JAXBContext.newInstance(MyType.class);
		JAXBContext context = JAXBContext.newInstance(MyType.class.getPackage().getName());
		context.generateSchema(new SchemaOutputResolver()
		{
			@Override
			public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
			{
				System.out.println("Namespace: " + namespaceUri + " " + suggestedFileName);
				StreamResult result = new StreamResult(System.out);
				result.setSystemId("test.xsd");
				return(result);
			}
		});
		
		if (true)
			return;
		
		RuntimeTypeInfoSet infoSet = JAXBModelFactory.create(MyType.class);
		System.out.println("Type name: " + infoSet.getClassInfo(MyType.class).getTypeName());
		//infoSet.getClassInfo(MyType.class);
		RuntimeNonElement r = infoSet.getTypeInfo(MyType.class);
		//System.out.println("r: " + r);
		
		if (r instanceof ClassInfo<?, ?>)
		{
			ClassInfo<?, ?> ci = (ClassInfo<?, ?>)r;
			for (PropertyInfo<?, ?> pi : ci.getProperties())
			{
				if (pi instanceof AttributePropertyInfo<?, ?>)
				{
					AttributePropertyInfo<?, ?> ai = (AttributePropertyInfo<?, ?>)pi;
					System.out.println(ai.getName() + ": " + ai.getXmlName() + " - " + ai.getTarget().getTypeName());
					if (ai.hasAnnotation(SubstituteWithEnum.class))
						System.out.println("Substitute " + ai.getXmlName() + " with enum");
					
					if (ai.isRequired())
						System.out.println("(required attribute)");
				}
			}
		}
		
		/*
		RuntimeTypeInfoSet infoSet = JAXBModelFactory.create(MyEnum.class);
		RuntimeEnumLeafInfo enumInfo = infoSet.enums().get(MyEnum.class);
		for (EnumConstant<?, ?> c : enumInfo.getConstants())
		{
			System.out.println(c.getName() + ": " + c.getLexicalValue());
		}
		
		System.out.println("Parsed: " + enumInfo.getTransducer().parse("one"));
			
		//System.out.println("Type name: " + infoSet.getClassInfo(MyType.class).getTypeName());
		 *
		 */
	}
}
