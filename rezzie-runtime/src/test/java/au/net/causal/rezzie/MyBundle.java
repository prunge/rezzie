package au.net.causal.rezzie;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyBundle
{
	public String message(String name);
}
