package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class TestBundleReader
{

	@Test
	public void testSingleParameter()
	throws Exception
	{
		BundleReader reader = new BundleReader();
		RezzieBundle bundle = reader.readFromXml(readXmlDocument("bundle1.xml"), readXmlDocument("schema1.xsd"), MyBundle.class);
		
		String result = bundle.getText("message", Locale.ENGLISH, "John Galah");
		assertEquals("Wrong result.", "Good morning John Galah, what will be for eating?", result);
		
	}
	
	private static Document readXmlDocument(String resourceName)
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		
		URL resource = TestBundleReader.class.getResource(resourceName);
		assertNotNull("Missing test resource: " + resourceName, resource);
		
		try
		{
			return(dbf.newDocumentBuilder().parse(resource.toExternalForm()));
		}
		catch (IOException | SAXException | ParserConfigurationException e)
		{
			throw new RuntimeException(e);
		}
	}

}
