package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyNumberBundle;

/**
 * Test for choice parameters.
 * 
 * @author prunge
 */
public class TestChoiceParameters
{
	@Test
	public void testChoiceFirstOption()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		String message = bundle.message(1);
		assertEquals("Wrong message.", "Are you sure you wish to process this file?", message);
	}
	
	@Test
	public void testChoiceSecondOption()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		String message = bundle.message(2);
		assertEquals("Wrong message.", "Are you sure you wish to process these 2 files?", message);
	}
	
	@Test
	public void testGreaterThan()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		assertEquals("Wrong message.", "Zero", bundle.message2(0));
		assertEquals("Wrong message.", "Your number is 1", bundle.message2(1));
		assertEquals("Wrong message.", "Your number is 10", bundle.message2(10));
		assertEquals("Wrong message.", "Whoa, 11 is a big number!", bundle.message2(11));
	}
	
	@Test
	public void testGreaterThanOrEqual()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		assertEquals("Wrong message.", "Zero", bundle.message3(0));
		assertEquals("Wrong message.", "Your number is 1", bundle.message3(1));
		assertEquals("Wrong message.", "Your number is 9", bundle.message3(9));
		assertEquals("Wrong message.", "Whoa, 10 is a big number!", bundle.message3(10));
		assertEquals("Wrong message.", "Whoa, 11 is a big number!", bundle.message3(11));
	}
	
	@Test
	public void testLessThan()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		assertEquals("Wrong message.", "Zero", bundle.message4(0));
		assertEquals("Wrong message.", "1 is a small number", bundle.message4(1));
		assertEquals("Wrong message.", "9 is a small number", bundle.message4(9));
		assertEquals("Wrong message.", "Your number is 10", bundle.message4(10));
		assertEquals("Wrong message.", "Your number is 11", bundle.message4(11));
	}
	
	@Test
	public void testLessThanOrEqual()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle bundle = rezzie.getBundle(MyNumberBundle.class, Locale.ENGLISH);
		
		assertEquals("Wrong message.", "Zero", bundle.message5(0));
		assertEquals("Wrong message.", "1 is a small number", bundle.message5(1));
		assertEquals("Wrong message.", "10 is a small number", bundle.message5(10));
		assertEquals("Wrong message.", "Your number is 11", bundle.message5(11));
	}
}
