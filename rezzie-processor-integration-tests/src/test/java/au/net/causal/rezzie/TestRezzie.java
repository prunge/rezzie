package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Locale;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import au.net.causal.rezzie.apt.integrationtest.MyBundle;
import au.net.causal.rezzie.apt.integrationtest.MyBundleWithDefaultLocale;
import au.net.causal.rezzie.apt.integrationtest.MyPartialBundle;
import au.net.causal.rezzie.apt.integrationtest.MyRemappedBundle;
import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

public class TestRezzie
{
	@Test
	public void testExactLocale()
	{
		Rezzie rezzie = new Rezzie();
		MyBundle bundle = rezzie.getBundle(MyBundle.class, Locale.ENGLISH);
		
		String result = bundle.messageWithParameter("John Smith");
		assertEquals("Wrong message.", "Good morning John Smith, what will be for eating?", result);
	}
	
	/**
	 * We specify a locale of en_AU, but only the en bundle actually exists.
	 */
	@Test
	public void testLocaleCandidate()
	{
		Rezzie rezzie = new Rezzie();
		MyBundle bundle = rezzie.getBundle(MyBundle.class, new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		
		String result = bundle.messageWithParameter("John Smith");
		assertEquals("Wrong message.", "Good morning John Smith, what will be for eating?", result);
	}

	/**
	 * A completely different locale, testing that the default locale lookup works.
	 */
	@Test
	public void testLocaleDefaults()
	{
		Rezzie rezzie = new Rezzie();
		MyBundleWithDefaultLocale bundle = rezzie.getBundle(MyBundleWithDefaultLocale.class, Locale.GERMAN);
		
		String result = bundle.greeting();
		assertEquals("Wrong message.", "I have a default locale!", result);
	}
	
	/**
	 * Single locale support test.
	 */
	@Test
	public void testGetSupportedLocalesSingle()
	{
		Rezzie rezzie = new Rezzie();
		Set<Locale> supportedLocales = rezzie.getSupportedLocales(MyBundle.class);
		assertEquals("Wrong result.", Collections.singleton(Locale.ENGLISH), supportedLocales);
	}
	
	/**
	 * Multi locale support test.
	 */
	@Test
	public void testGetSupportedLocalesMulti()
	{
		Rezzie rezzie = new Rezzie();
		Set<Locale> supportedLocales = rezzie.getSupportedLocales(MyPartialBundle.class);
		assertEquals("Wrong result.", ImmutableSet.of(Locale.ENGLISH, new Locale(Languages.ENGLISH, Countries.AUSTRALIA)), supportedLocales);
	}
	
	/**
	 * Multi locale support test.
	 */
	@Test
	public void testGetSupportedLocalesWithRemap()
	{
		Rezzie rezzie = new Rezzie();
		Set<Locale> supportedLocales = rezzie.getSupportedLocales(MyRemappedBundle.class);
		assertEquals("Wrong result.", ImmutableSet.of(Locale.ENGLISH, new Locale(Languages.ENGLISH, Countries.AUSTRALIA), new Locale(Languages.ENGLISH, Countries.UK)), supportedLocales);
	}
}
