package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyNumberBundle2;
import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

/**
 * Date parameter tests.
 * 
 * @author prunge
 */
public class TestNumberParameters
{
	/**
	 * Simple number parameter test.
	 */
	@Test
	public void testDefaultNumber()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle2 bundle = rezzie.getBundle(MyNumberBundle2.class, Locale.ENGLISH);
		
		String message = bundle.message(2);
		assertEquals("Wrong message.", "Your number is 2", message);
	}
	
	/**
	 * Tests customizing the number format with an annotation.
	 */
	@Test
	public void testAnnotationCustomization()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle2 bundle = rezzie.getBundle(MyNumberBundle2.class, Locale.ENGLISH);
		
		String message = bundle.doubleMessage(2.1);
		assertEquals("Wrong message.", "Formatted number is 2.10", message);
	}
	
	/**
	 * Tests customizing the number format via bundle attributes.
	 */
	@Test
	public void testBundleCustomization()
	{
		Rezzie rezzie = new Rezzie();
		MyNumberBundle2 bundle = rezzie.getBundle(MyNumberBundle2.class, new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		
		String message = bundle.message(2);
		assertEquals("Wrong message.", "Your number is 2.000", message);
	}
}
