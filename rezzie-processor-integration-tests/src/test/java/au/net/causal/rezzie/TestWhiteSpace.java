package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyWhiteSpaceBundle;
import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

/**
 * Test for primitive parameters.
 * 
 * @author prunge
 */
public class TestWhiteSpace
{
	@Test
	public void testDefaultSettings()
	{
		Rezzie rezzie = new Rezzie();
		MyWhiteSpaceBundle bundle = rezzie.getBundle(MyWhiteSpaceBundle.class, Locale.ENGLISH);
		
		String message = bundle.message("John Galah");
		assertEquals("Wrong message.", "Good morning, John Galah.", message);
	}
	
	@Test
	public void testPreserveAnnotation()
	{
		Rezzie rezzie = new Rezzie();
		MyWhiteSpaceBundle bundle = rezzie.getBundle(MyWhiteSpaceBundle.class, Locale.ENGLISH);
		
		String message = bundle.message2("John Galah");
		assertEquals("Wrong message.", "Good morning,\t\t\tJohn Galah.", message);
	}
	
	@Test
	public void testPreserveOnBundle()
	{
		Rezzie rezzie = new Rezzie();
		MyWhiteSpaceBundle bundle = rezzie.getBundle(MyWhiteSpaceBundle.class, new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		
		String message = bundle.message("John Galah");
		assertEquals("Wrong message.", "G'day,\t\t\tJohn Galah.", message);
	}
	
	@Test
	public void testPreserveOnBundleElement()
	{
		Rezzie rezzie = new Rezzie();
		MyWhiteSpaceBundle bundle = rezzie.getBundle(MyWhiteSpaceBundle.class, new Locale(Languages.ENGLISH, Countries.UK));
		
		String message = bundle.message("John Galah");
		assertEquals("Wrong message.", "Greetings,\t\t\tJohn Galah.", message);
	}
}
