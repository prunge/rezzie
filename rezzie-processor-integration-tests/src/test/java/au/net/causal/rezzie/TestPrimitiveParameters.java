package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyPrimitiveBundle;

/**
 * Test for primitive parameters.
 * 
 * @author prunge
 */
public class TestPrimitiveParameters
{
	@Test
	public void testPrimitives()
	{
		Rezzie rezzie = new Rezzie();
		MyPrimitiveBundle bundle = rezzie.getBundle(MyPrimitiveBundle.class, Locale.ENGLISH);
		
		String message = bundle.message(1.1);
		assertEquals("Wrong message.", "Your value is '1.1'", message);
	}
}
