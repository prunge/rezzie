package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyEnum;
import au.net.causal.rezzie.apt.integrationtest.MyEnumBundle;

/**
 * Test for enum parameters.
 * 
 * @author prunge
 */
public class TestEnumChoiceParameters
{
	@Test
	public void testSelection()
	{
		Rezzie rezzie = new Rezzie();
		MyEnumBundle bundle = rezzie.getBundle(MyEnumBundle.class, Locale.ENGLISH);
		
		String message = bundle.messageWithParameter(MyEnum.ONE);
		assertEquals("Wrong message.", "Make it so, number one", message);
	}
	
	@Test
	public void testOtherwise()
	{
		Rezzie rezzie = new Rezzie();
		MyEnumBundle bundle = rezzie.getBundle(MyEnumBundle.class, Locale.ENGLISH);
		
		String message = bundle.messageWithParameter(MyEnum.THREE);
		assertEquals("Wrong message.", "You picked THREE", message);
	}
}
