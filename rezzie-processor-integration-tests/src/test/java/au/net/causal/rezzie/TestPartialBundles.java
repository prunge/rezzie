package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyPartialBundle;
import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

/**
 * Tests for Rezzie partial bundle support.
 * 
 * @author prunge
 */
public class TestPartialBundles
{
	/**
	 * The generic English language test.
	 */
	@Test
	public void testGenericEnglish()
	{
		Rezzie rezzie = new Rezzie();
		MyPartialBundle bundle = rezzie.getBundle(MyPartialBundle.class, Locale.ENGLISH);
		
		String hello = bundle.sayHello();
		String gday = bundle.gday();
		assertEquals("Wrong text.", "Hello there", hello);
		assertEquals("Wrong text.", "Good day to you", gday);
	}
	
	/**
	 * The Australian english overrides g'day only.
	 */
	@Test
	public void testAussieEnglish()
	{
		Rezzie rezzie = new Rezzie();
		MyPartialBundle bundle = rezzie.getBundle(MyPartialBundle.class, new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		
		String hello = bundle.sayHello();
		String gday = bundle.gday();
		assertEquals("Wrong text.", "Hello there", hello);
		assertEquals("Wrong text.", "G'day mate!", gday);
	}
}
