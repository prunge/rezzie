package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyRemappedBundle;
import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

/**
 * Remapping tests.
 * 
 * @author prunge
 */
public class TestRemapping
{
	/**
	 * The original standard English message.
	 */
	@Test
	public void testOriginal()
	{
		Rezzie rezzie = new Rezzie();
		MyRemappedBundle bundle = rezzie.getBundle(MyRemappedBundle.class, new Locale(Languages.ENGLISH, Countries.US));
		
		String message = bundle.message();
		String message2 = bundle.message2();
		assertEquals("Wrong message.", "Please select a color:", message);
		assertEquals("Wrong message.", "Thank you", message2);
	}
	
	/**
	 * UK uses 'colour' instead of 'color' for one particular message.
	 */
	@Test
	public void testOverride()
	{
		Rezzie rezzie = new Rezzie();
		MyRemappedBundle bundle = rezzie.getBundle(MyRemappedBundle.class, new Locale(Languages.ENGLISH, Countries.UK));
		
		String message = bundle.message();
		String message2 = bundle.message2();
		assertEquals("Wrong message.", "Please select a colour:", message);
		assertEquals("Wrong message.", "Thank you", message2);
	}
	
	/**
	 * Australia is remapped to use en-UK's bundle.
	 */
	@Test
	public void testRemap()
	{
		Rezzie rezzie = new Rezzie();
		MyRemappedBundle bundle = rezzie.getBundle(MyRemappedBundle.class, new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		
		String message = bundle.message();
		String message2 = bundle.message2();
		assertEquals("Wrong message.", "Please select a colour:", message);
		assertEquals("Wrong message.", "Thank you", message2);
	}
}
