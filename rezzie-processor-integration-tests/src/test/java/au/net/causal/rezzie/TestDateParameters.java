package au.net.causal.rezzie;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.junit.Test;

import au.net.causal.rezzie.apt.integrationtest.MyDateBundle;

/**
 * Date parameter tests.
 * 
 * @author prunge
 */
public class TestDateParameters
{
	/**
	 * Simple test for a date parameter.
	 */
	@Test
	public void testDate()
	{
		Rezzie rezzie = new Rezzie();
		MyDateBundle bundle = rezzie.getBundle(MyDateBundle.class, Locale.ENGLISH);
		
		String message = bundle.sayHello(new GregorianCalendar(1977, Calendar.JANUARY, 9).getTime());
		assertEquals("Wrong message.", "Hello, the current date is 1977-01-09", message);
	}
}
