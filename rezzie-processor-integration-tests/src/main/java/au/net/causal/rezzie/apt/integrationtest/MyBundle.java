package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyBundle
{
	public String message();
	public String messageWithParameter(String name);
	public String message3();
}
