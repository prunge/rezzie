package au.net.causal.rezzie.apt.integrationtest;

import java.util.Locale;

import au.net.causal.rezzie.parameter.ZeroConfigurationParameterProcessor;

public class IntegerProcessor extends ZeroConfigurationParameterProcessor<Integer>
{
	public IntegerProcessor()
	{
		super(Integer.class);
	}
	
	@Override
	protected String parameterToString(Integer parameter, Locale locale)
	{
		return(parameter.toString());
	}
}
