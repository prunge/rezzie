package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.parameter.impl.annotation.NumberFormat;

@ResourceBundleTemplate
public interface MyNumberBundle2
{
	public String message(Integer value);
	
	@NumberFormat(minimumFractionDigits=2)
	public String doubleMessage(double value);
}
