package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.annotation.WhiteSpace;
import au.net.causal.rezzie.annotation.WhiteSpaceMode;

@ResourceBundleTemplate
public interface MyWhiteSpaceBundle
{
	public String message(String name);
	
	@WhiteSpace(text=WhiteSpaceMode.PRESERVE)
	public String message2(String name);
}
