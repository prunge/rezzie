package au.net.causal.rezzie.apt.integrationtest;

import java.util.Date;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyDateBundle
{
	public String sayHello(Date date);
}
