package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyPrimitiveBundle
{
	public String message(double value);
}
