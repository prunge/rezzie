package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyEnumBundle
{
	public String messageWithParameter(MyEnum me);
}
