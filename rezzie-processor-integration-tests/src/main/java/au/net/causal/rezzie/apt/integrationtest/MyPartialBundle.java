package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyPartialBundle
{
	public String sayHello();
	public String gday();
}
