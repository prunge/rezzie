package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyRemappedBundle
{
	public String message();
	public String message2();
}
