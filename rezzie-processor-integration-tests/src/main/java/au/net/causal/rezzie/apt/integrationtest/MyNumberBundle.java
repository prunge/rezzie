package au.net.causal.rezzie.apt.integrationtest;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;

@ResourceBundleTemplate
public interface MyNumberBundle
{
	public String message(Integer value);
	public String message2(Integer value);
	public String message3(Integer value);
	public String message4(Integer value);
	public String message5(Integer value);
}
