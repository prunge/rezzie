package au.net.causal.rezzie.apt;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.jvnet.jaxb.reflection.JAXBModelFactory;
import org.jvnet.jaxb.reflection.model.core.AttributePropertyInfo;
import org.jvnet.jaxb.reflection.model.core.ClassInfo;
import org.jvnet.jaxb.reflection.model.core.NonElement;
import org.jvnet.jaxb.reflection.model.core.PropertyInfo;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeTypeInfoSet;

import au.net.causal.rezzie.apt.ChoiceResourceParameter.EnumSubstitutedAttribute;
import au.net.causal.rezzie.parameter.ChoiceSupport;
import au.net.causal.rezzie.parameter.ParameterProcessor;
import au.net.causal.rezzie.parameter.ParameterProcessorRegistry;
import au.net.causal.rezzie.parameter.annotation.SubstituteWithEnum;
import au.net.causal.rezzie.reflection.TypeUtils;

public class ElementReader
{
	private final Types typeUtils;
	private final ClassLoader typeLoader = ElementReader.class.getClassLoader();
	private final ParameterProcessorRegistry parameterProcessorRegistry = new ParameterProcessorRegistry(typeLoader);
	
	//Make concurrent just in case the compiler is firing with multiple threads
	private final Map<Class<?>, JaxbInfo> jaxbTypeNameCache = new ConcurrentHashMap<>();
	
	public ElementReader(Types typeUtils)
	{
		if (typeUtils == null)
			throw new NullPointerException("typeUtils == null");
		
		this.typeUtils = typeUtils;
	}
	
	public BundleElement read(TypeElement bundleType, ProblemHandler problemHandler)
	{
		BundleElement bundleElement = new BundleElement(bundleType);
		
		for (Element element : bundleType.getEnclosedElements())
		{
			if (element.getKind() == ElementKind.METHOD && element instanceof ExecutableElement)
			{
				ExecutableElement methodElement = (ExecutableElement)element;
				ResourceMethod rMethod = new ResourceMethod(methodElement);
				readMethodParameterInfo(methodElement, bundleType, rMethod, problemHandler);
				bundleElement.addMethod(rMethod);
			}
		}
		
		return(bundleElement);
	}
	
	private void readMethodParameterInfo(ExecutableElement method, TypeElement type, ResourceMethod rMethod, ProblemHandler problemHandler)
	{
		//If the method does not have parameters, type is a straight xsd:string
		for (VariableElement parameter : method.getParameters())
		{
			TypeMirror variableType = parameter.asType();
			Class<?> runtimeType = toRuntimeType(variableType);
			
			//Special handling for enums
			if (runtimeType == null && isEnumType(variableType))
				runtimeType = Enum.class;

			if (runtimeType == null)
				problemHandler.handleProblem("Parameter type " + variableType + " does not have a runtime type and is not supported for resource bundle templates.", parameter);
			else
			{
				ResourceParameter rParameter = readMethodParameterInfoForParameter(parameter, runtimeType, method, type, problemHandler);
				if (rParameter != null)
					rMethod.addParameter(rParameter);
			}
		}
	}
	
	private boolean isEnumType(TypeMirror variableType)
	{
		if (variableType.getKind() == TypeKind.DECLARED && variableType instanceof DeclaredType)
		{
			DeclaredType dvType = (DeclaredType)variableType;
			if (dvType.asElement().getKind() == ElementKind.ENUM)
				return(true);
		}

		return(false);
	}
	
	private JaxbInfo lookUpInfoForJaxbType(Class<?> jaxbType)
	throws JAXBException
	{
		//Use cache if possible
		JaxbInfo jaxbInfo = jaxbTypeNameCache.get(jaxbType);
		if (jaxbInfo != null)
			return(jaxbInfo);
		
		RuntimeTypeInfoSet infoSet = JAXBModelFactory.create(jaxbType);
		if (infoSet == null)
			return(null);
		
		NonElement<?, ?> jaxbClassInfo = infoSet.getClassInfo(jaxbType);
		if (jaxbClassInfo == null)
			return(null);

		//Determine any enum substituted attributes
		Set<EnumSubstitutedAttribute> subAttributes = new LinkedHashSet<>();
		if (jaxbClassInfo instanceof ClassInfo<?, ?>)
		{
			ClassInfo<?, ?> ci = (ClassInfo<?, ?>)jaxbClassInfo;
			for (PropertyInfo<?, ?> pi : ci.getProperties())
			{
				if (pi instanceof AttributePropertyInfo<?, ?>)
				{
					AttributePropertyInfo<?, ?> ai = (AttributePropertyInfo<?, ?>)pi;
					if (ai.hasAnnotation(SubstituteWithEnum.class))
						subAttributes.add(new EnumSubstitutedAttribute(ai.getXmlName(), ai.getTarget().getTypeName(), ai.isRequired()));
				}
			}
		}
		
		QName name = jaxbClassInfo.getTypeName();
		jaxbInfo = new JaxbInfo(name, subAttributes);
		
		jaxbTypeNameCache.put(jaxbType, jaxbInfo);
		
		return(jaxbInfo);
	}
	
	private <P> ResourceParameter readMethodParameterInfoForParameter(VariableElement parameter, Class<P> parameterType, ExecutableElement method, TypeElement type, 
			ProblemHandler problemHandler)
	{
		Class<P> parameterTypeToUse = parameterType;
		if (parameterTypeToUse.isPrimitive())
			parameterTypeToUse = TypeUtils.wrapperType(parameterType);
		
		ParameterProcessor<? super P, ?> parameterProcessor = parameterProcessorRegistry.getProcessor(parameterTypeToUse);
		if (parameterProcessor == null)
		{
			problemHandler.handleProblem("Parameter type " + parameterType.getCanonicalName() + " is not supported for resource bundle templates.", parameter);
			return(null);
		}
		
		Class<?> jaxbConfigType = parameterProcessor.getConfigurationType();
		QName parameterComplexTypeName;
		try
		{
			JaxbInfo jaxbInfo = lookUpInfoForJaxbType(jaxbConfigType);
			if (jaxbInfo == null)
			{
				problemHandler.handleProblem("Error looking up JAXB configuration infoset for parameter processor " + parameterProcessor.getClass().getCanonicalName() + ".", parameter);
				return(null);
			}
			else
				parameterComplexTypeName = jaxbInfo.getTypeName();
		}
		catch (JAXBException e)
		{
			problemHandler.handleProblem("Error looking up JAXB configuration for parameter processor " + parameterProcessor.getClass().getCanonicalName() + ": " + e, parameter);
			return(null);
		}
		
		//Read choice configuration
		if (parameterProcessor instanceof ChoiceSupport<?, ?>)
		{
			ChoiceSupport<?, ?> choiceSupport = (ChoiceSupport<?, ?>)parameterProcessor;
			Class<?> choiceJaxbConfigType = choiceSupport.getChoiceConfigurationType();
			JaxbInfo choiceJaxbInfo;
			try
			{
				choiceJaxbInfo = lookUpInfoForJaxbType(choiceJaxbConfigType);
				
				if (choiceJaxbInfo == null)
				{
					problemHandler.handleProblem("Error looking up JAXB choice configuration infoset for parameter processor " + parameterProcessor.getClass().getCanonicalName() + ".", parameter);
					return(null);
				}
			}
			catch (JAXBException e)
			{
				problemHandler.handleProblem("Error looking up JAXB choice configuration class info for parameter processor " + parameterProcessor.getClass().getCanonicalName() + ".", parameter);
				return(null);
			}
			
			return(new ChoiceResourceParameter(parameter, new MappedXmlType(jaxbConfigType, parameterComplexTypeName), new MappedXmlType(choiceJaxbConfigType, choiceJaxbInfo.getTypeName()), choiceJaxbInfo.getEnumSubstitutedAttributes()));
		}
		else
			return(new ResourceParameter(parameter, new MappedXmlType(jaxbConfigType, parameterComplexTypeName)));
	}
	
	private Class<?> toRuntimeType(TypeMirror type)
	{
		switch (type.getKind())
		{
			//Primitives
			case BOOLEAN:
				return(boolean.class);
			case BYTE:
				return(byte.class);
			case CHAR:
				return(char.class);
			case DOUBLE:
				return(double.class);
			case FLOAT:
				return(float.class);
			case INT:
				return(int.class);
			case LONG:
				return(long.class);
			case SHORT:
				return(short.class);
				
			//Reference type
			case DECLARED:
			case ARRAY:
			{
				Element element = typeUtils.asElement(type);
				if ((element.getKind().isClass() || element.getKind().isInterface()) && 
					element instanceof TypeElement)
				{
					TypeElement typeElement = (TypeElement)element;
					String typeName = typeElement.getQualifiedName().toString();
					
					try
					{
						return(Class.forName(typeName, false, typeLoader));
					}
					catch (ClassNotFoundException e)
					{
						return(null);
					}
				}
				else
					return(null);
			}
			
			default:
				return(null);
		}
	}
	
	public static interface ProblemHandler
	{
		public void handleProblem(String message, Element element);
	}
	
	private static class JaxbInfo
	{
		private final QName typeName;
		private final Set<? extends EnumSubstitutedAttribute> enumSubstitutedAttributes;
		
		public JaxbInfo(QName typeName, Set<? extends EnumSubstitutedAttribute> enumSubstitutedAttributes)
		{
			this.typeName = typeName;
			this.enumSubstitutedAttributes = Collections.unmodifiableSet(new LinkedHashSet<>(enumSubstitutedAttributes));
		}

		public QName getTypeName()
		{
			return(typeName);
		}

		public Set<? extends EnumSubstitutedAttribute> getEnumSubstitutedAttributes()
		{
			return(enumSubstitutedAttributes);
		}
	}
}
