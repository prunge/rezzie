package au.net.causal.rezzie.apt.jaxb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

import org.jvnet.jaxb.reflection.model.nav.Navigator;
import org.jvnet.jaxb.reflection.runtime.Location;

public class AptNavigator implements Navigator<TypeMirror, DeclaredType, VariableElement, ExecutableElement>
{
	private final ProcessingEnvironment processingEnv;
	
	private final DeclaredType enumType;
	
	public AptNavigator(ProcessingEnvironment processingEnv)
	{
		if (processingEnv == null)
			throw new NullPointerException("processingEnv == null");
		
		this.processingEnv = processingEnv;
		TypeElement enumElement = processingEnv.getElementUtils().getTypeElement(Enum.class.getCanonicalName());
		this.enumType = processingEnv.getTypeUtils().getDeclaredType(enumElement);
	}

	@Override
	public DeclaredType getSuperClass(DeclaredType clazz)
	{
		Element element = clazz.asElement();
		if (element.getKind() == ElementKind.CLASS && element instanceof TypeElement)
		{
			TypeElement typeElement = (TypeElement)element;
			TypeMirror superType = typeElement.getSuperclass();
			if (superType.getKind() == TypeKind.DECLARED && superType instanceof DeclaredType)
				return((DeclaredType)superType);
		}
		
		return(null);
	}

	@Override
	public DeclaredType getBaseClass(TypeMirror type, DeclaredType baseType)
	{
		//This will be an absolute PITA to implement, I hope it is not needed for our enum processing
		//TODO
		
		return(baseType);
	}

	@Override
	public String getClassName(DeclaredType clazz)
	{
		return(clazz.toString());
	}

	@Override
	public String getTypeName(TypeMirror rawType)
	{
		return(rawType.toString());
	}

	@Override
	public String getClassShortName(DeclaredType clazz)
	{
		return(clazz.asElement().getSimpleName().toString());
	}

	@Override
	public Collection<? extends VariableElement> getDeclaredFields(DeclaredType clazz)
	{
		List<VariableElement> fields = new ArrayList<>();
		
		TypeElement t = (TypeElement)clazz.asElement();
		for (Element element : t.getEnclosedElements())
		{
			if (element.getKind() == ElementKind.FIELD && element instanceof VariableElement)
				fields.add((VariableElement)element);
		}
		
		return(fields);
	}

	@Override
	public VariableElement getDeclaredField(DeclaredType clazz, String fieldName)
	{
		for (VariableElement field : getDeclaredFields(clazz))
		{
			if (field.getSimpleName().toString().equals(fieldName))
				return(field);
		}
		
		return(null);
	}

	@Override
	public Collection<? extends ExecutableElement> getDeclaredMethods(DeclaredType clazz)
	{
		List<ExecutableElement> methods = new ArrayList<>();
		
		TypeElement t = (TypeElement)clazz.asElement();
		for (Element element : t.getEnclosedElements())
		{
			if (element.getKind() == ElementKind.METHOD && element instanceof ExecutableElement)
				methods.add((ExecutableElement)element);
		}
		
		return(methods);
	}
	
	private DeclaredType toDeclaredType(TypeMirror type)
	{
		if (type.getKind() == TypeKind.DECLARED && type instanceof DeclaredType)
			return((DeclaredType)type);
		else
			return(null);
	}

	@Override
	public DeclaredType getDeclaringClassForField(VariableElement field)
	{
		return(toDeclaredType(field.getEnclosingElement().asType()));
	}

	@Override
	public DeclaredType getDeclaringClassForMethod(ExecutableElement method)
	{
		return(toDeclaredType(method.getEnclosingElement().asType()));
	}

	@Override
	public TypeMirror getFieldType(VariableElement f)
	{
		return(f.asType());
	}

	@Override
	public String getFieldName(VariableElement field)
	{
		return(field.getSimpleName().toString());
	}

	@Override
	public String getMethodName(ExecutableElement m)
	{
		return(m.getSimpleName().toString());
	}

	@Override
	public TypeMirror getReturnType(ExecutableElement m)
	{
		return(m.getReturnType());
	}

	@Override
	public TypeMirror[] getMethodParameters(ExecutableElement method)
	{
		TypeMirror[] paramTypes = new TypeMirror[method.getParameters().size()];
		int i = 0;
		for (VariableElement parameter : method.getParameters())
		{
			paramTypes[i] = parameter.asType();
			i++;
		}
		return(paramTypes);
	}

	@Override
	public boolean isStaticMethod(ExecutableElement method)
	{
		return(method.getModifiers().contains(Modifier.STATIC));
	}

	@Override
	public boolean isSubClassOf(TypeMirror sub, TypeMirror sup)
	{
		return(processingEnv.getTypeUtils().isSubtype(sub, sup));
	}

	@Override
	public TypeMirror ref(Class c)
	{
		if (c.isPrimitive())
			return(getPrimitive(c));
		
		TypeElement te = processingEnv.getElementUtils().getTypeElement(c.getCanonicalName());
		if (te == null)
			return(null);
		else
			return(te.asType());
	}

	@Override
	public DeclaredType use(DeclaredType c)
	{
		return(c);
	}

	@Override
	public DeclaredType asDecl(TypeMirror type)
	{
		return(toDeclaredType(type));
	}

	@Override
	public DeclaredType asDecl(Class c)
	{
		return(toDeclaredType(ref(c)));
	}

	@Override
	public boolean isArray(TypeMirror t)
	{
		return(t.getKind() == TypeKind.ARRAY); 
	}
	
	@Override
	public boolean isArrayButNotByteArray(TypeMirror t)
	{
		if (t.getKind() == TypeKind.ARRAY && t instanceof ArrayType)
		{
			ArrayType array = (ArrayType)t;
			if (array.getComponentType().getKind() == TypeKind.BYTE)
				return(false);
			
			return(true);
		}
		else
			return(false);
	}

	@Override
	public TypeMirror getComponentType(TypeMirror t)
	{
		if (t.getKind() == TypeKind.ARRAY && t instanceof ArrayType)
		{
			ArrayType array = (ArrayType)t;
			return(array.getComponentType());
		}
		else
			return(null);
	}

	@Override
	public TypeMirror getTypeArgument(TypeMirror t, int i) 
	{
		if (t.getKind() == TypeKind.DECLARED && t instanceof DeclaredType)
		{
			DeclaredType dt = (DeclaredType)t;
			return(dt.getTypeArguments().get(i));
		}
		else
			return(null);
	}

	@Override
	public boolean isParameterizedType(TypeMirror t)
	{
		if (t.getKind() == TypeKind.DECLARED && t instanceof DeclaredType)
		{
			DeclaredType dt = (DeclaredType)t;
			return(!dt.getTypeArguments().isEmpty());
		}
		else
			return(false);
	}

	@Override
	public boolean isPrimitive(TypeMirror t)
	{
		return(t.getKind().isPrimitive());
	}
	
	@Override
	public TypeMirror getPrimitive(Class primitiveType)
	{
		Types t = processingEnv.getTypeUtils();
		
		if (primitiveType == int.class)
			return(t.getPrimitiveType(TypeKind.INT));
		if (primitiveType == long.class)
			return(t.getPrimitiveType(TypeKind.LONG));
		if (primitiveType == short.class)
			return(t.getPrimitiveType(TypeKind.SHORT));
		if (primitiveType == byte.class)
			return(t.getPrimitiveType(TypeKind.BYTE));
		if (primitiveType == char.class)
			return(t.getPrimitiveType(TypeKind.CHAR));
		if (primitiveType == double.class)
			return(t.getPrimitiveType(TypeKind.DOUBLE));
		if (primitiveType == float.class)
			return(t.getPrimitiveType(TypeKind.FLOAT));
		if (primitiveType == boolean.class)
			return(t.getPrimitiveType(TypeKind.BOOLEAN));
		
		return(null);
	}

	@Override
	public Location getClassLocation(DeclaredType clazz)
	{
		return(null);
	}

	@Override
	public Location getFieldLocation(VariableElement field)
	{
		return(null);
	}

	@Override
	public Location getMethodLocation(ExecutableElement getter)
	{
		return(null);
	}

	@Override
	public boolean hasDefaultConstructor(DeclaredType clazz)
	{
		Element e = clazz.asElement();
		int constructorCount = 0;
		if (e.getKind() == ElementKind.CLASS && e instanceof TypeElement)
		{
			TypeElement te = (TypeElement)e;
			for (Element ce : te.getEnclosedElements())
			{
				if (ce.getKind() == ElementKind.CONSTRUCTOR && ce instanceof ExecutableElement)
				{
					constructorCount++;
					ExecutableElement ctor = (ExecutableElement)ce;
					if (ctor.getParameters().isEmpty())
						return(true);
				}
			}
			
			if (constructorCount == 0)
				return(true);
			else
				return(false);
		}
		else
			return(false);
	}

	@Override
	public boolean isStaticField(VariableElement field)
	{
		return(field.getModifiers().contains(Modifier.STATIC));
	}

	@Override
	public boolean isPublicMethod(ExecutableElement method)
	{
		return(method.getModifiers().contains(Modifier.PUBLIC));
	}

	@Override
	public boolean isPublicField(VariableElement field)
	{
		return(field.getModifiers().contains(Modifier.PUBLIC));
	}

	@Override
	public boolean isEnum(DeclaredType clazz)
	{
		boolean isAnEnum = processingEnv.getTypeUtils().isAssignable(clazz, enumType);
		return(isAnEnum);
	}

	@Override
	public <P> TypeMirror erasure(TypeMirror contentInMemoryType)
	{
		return(processingEnv.getTypeUtils().erasure(contentInMemoryType));
	}

	@Override
	public boolean isAbstract(DeclaredType clazz)
	{
		return(clazz.asElement().getModifiers().contains(Modifier.ABSTRACT));
	}

	@Override
	public boolean isFinal(DeclaredType clazz)
	{
		return(clazz.asElement().getModifiers().contains(Modifier.FINAL));
	}

	@Override
	public VariableElement[] getEnumConstants(DeclaredType clazz)
	{
		Element e = clazz.asElement();
		if (e.getKind() == ElementKind.ENUM && e instanceof TypeElement)
		{
			List<VariableElement> ec = new ArrayList<>();
			
			TypeElement te = (TypeElement)e;
			for (Element item : te.getEnclosedElements())
			{
				if (item.getKind() == ElementKind.ENUM_CONSTANT && item instanceof VariableElement)
					ec.add((VariableElement)item);
			}
			
			return(ec.toArray(new VariableElement[ec.size()]));
		}
		else
			return(new VariableElement[0]);
	}

	@Override
	public TypeMirror getVoidType()
	{
		return(processingEnv.getTypeUtils().getPrimitiveType(TypeKind.VOID));
	}

	@Override
	public String getPackageName(DeclaredType clazz)
	{
		return(processingEnv.getElementUtils().getPackageOf(clazz.asElement()).getQualifiedName().toString());
	}

	@Override
	public DeclaredType findClass(String className, DeclaredType referencePoint)
	{
		TypeElement type = processingEnv.getElementUtils().getTypeElement(className);
		return(toDeclaredType(type.asType()));
	}

	@Override
	public boolean isBridgeMethod(ExecutableElement method)
	{
		return(false);
	}

	@Override
	public boolean isOverriding(ExecutableElement method, DeclaredType base)
	{
		TypeElement typeContainingMethod = (TypeElement)method.getEnclosingElement();
		
		//Check all methods in base with the same name
		for (ExecutableElement checkMethod : getDeclaredMethods(base))
		{
			if (checkMethod.getSimpleName().contentEquals(method.getSimpleName()) && 
				processingEnv.getElementUtils().overrides(method, checkMethod, typeContainingMethod))
			{
				return(true);
			}
		}
		
		return(false);
	}

	@Override
	public boolean isInterface(DeclaredType clazz)
	{
		return(clazz.asElement().getKind() == ElementKind.INTERFACE);
	}

	@Override
	public boolean isTransient(VariableElement f)
	{
		return(f.getModifiers().contains(Modifier.TRANSIENT));
	}

	@Override
	public boolean isInnerClass(DeclaredType clazz)
	{
		return(false);
	}
	
}
