package au.net.causal.rezzie.apt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.ExecutableElement;

public class ResourceMethod
{
	private final ExecutableElement method;
	private final List<ResourceParameter> parameters = new ArrayList<>();
	private final List<ChoiceResourceParameter> choiceParameters = new ArrayList<>();
	
	public ResourceMethod(ExecutableElement method)
	{
		if (method == null)
			throw new NullPointerException("method == null");
		
		this.method = method;
	}
	
	public ExecutableElement getMethod()
	{
		return(method);
	}
	
	public String getName()
	{
		return(getMethod().getSimpleName().toString());
	}
	
	public String getXmlGroupName()
	{
		return("MG_" + getName());
	}
	
	public String getXmlBaseComplexTypeName()
	{
		return("MT_" + getName());
	}
	
	public String getXmlOtherwiseComplexTypeName()
	{
		return("MT_" + getName() + "_Otherwise");
	}
	
	public String getXmlChoiceComplexTypeName(ChoiceResourceParameter choice)
	{
		return("MTC_" + getName() + "_" + choice.getName());
	}
	
	public List<? extends ResourceParameter> getParameters()
	{
		return(Collections.unmodifiableList(parameters));
	}
	
	public List<? extends ChoiceResourceParameter> getChoiceParameters()
	{
		return(Collections.unmodifiableList(choiceParameters));
	}
	
	public void addParameter(ResourceParameter parameter)
	{
		if (parameter == null)
			throw new NullPointerException("parameter == null");
		
		parameters.add(parameter);
		
		if (parameter instanceof ChoiceResourceParameter)
			choiceParameters.add((ChoiceResourceParameter)parameter);
	}
	
	public Set<? extends MappedXmlType> getAllParameterXmlTypes()
	{
		Set<MappedXmlType> parameterTypes = new LinkedHashSet<>();
		
		for (ResourceParameter parameter : parameters)
		{
			parameterTypes.add(parameter.getParameterXmlType());
		}
		
		return(parameterTypes);
	}
	
	public Set<? extends MappedXmlType> getAllChoiceXmlTypes()
	{
		Set<MappedXmlType> choiceTypes = new LinkedHashSet<>();
		
		for (ChoiceResourceParameter parameter : choiceParameters)
		{
			choiceTypes.add(parameter.getChoiceXmlType());
		}
		
		return(choiceTypes);
	}
	
}
