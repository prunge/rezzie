package au.net.causal.rezzie.apt.jaxb;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttachmentRef;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlInlineBinaryData;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSchemaTypes;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.jvnet.jaxb.reflection.model.annotation.AnnotationReader;
import org.jvnet.jaxb.reflection.model.annotation.Locatable;
import org.jvnet.jaxb.reflection.model.core.ErrorHandler;

public class AptAnnotationReader implements AnnotationReader<TypeMirror, DeclaredType, VariableElement, ExecutableElement>
{
	private final ProcessingEnvironment processingEnv;

	private static final List<Class<? extends Annotation>> JAXB_ANNOTATIONS = Collections.unmodifiableList(Arrays.<Class<? extends Annotation>>asList(
			XmlAccessorOrder.class,
			XmlAccessorType.class,
			XmlAnyAttribute.class,
			XmlAnyElement.class,
			XmlAttachmentRef.class,
			XmlAttribute.class,
			XmlElement.class,
			XmlElements.class,
			XmlElementRef.class,
			XmlElementRefs.class,
			XmlElementDecl.class,
			XmlElementWrapper.class,
			XmlEnum.class,
			XmlEnumValue.class,
			XmlID.class,
			XmlIDREF.class,
			XmlInlineBinaryData.class,
			XmlList.class,
			XmlMimeType.class,
			XmlMixed.class,
			XmlNs.class,
			XmlRegistry.class,
			XmlRootElement.class,
			XmlSchema.class,
			XmlSchemaType.class,
			XmlSchemaTypes.class,
			XmlSeeAlso.class,
			XmlTransient.class,
			XmlType.class,
			XmlValue.class
			));
	
	public AptAnnotationReader(ProcessingEnvironment processingEnv)
	{
		if (processingEnv == null)
			throw new NullPointerException("processingEnv == null");
		
		this.processingEnv = processingEnv;
	}
	
	@Override
	public void setErrorHandler(ErrorHandler errorHandler)
	{
		//Not used
	}

	@Override
	public <A extends Annotation> A getFieldAnnotation(Class<A> annotation, VariableElement field, Locatable srcpos)
	{
		return(field.getAnnotation(annotation));
	}

	@Override
	public boolean hasFieldAnnotation(Class<? extends Annotation> annotationType, VariableElement field)
	{
		return(field.getAnnotation(annotationType) != null);
	}

	@Override
	public boolean hasClassAnnotation(DeclaredType clazz, Class<? extends Annotation> annotationType)
	{
		return(clazz.asElement().getAnnotation(annotationType) != null);
	}

	@Override
	public Annotation[] getAllFieldAnnotations(VariableElement field, Locatable srcPos)
	{
		List<Annotation> list = new ArrayList<>();
		for (Class<? extends Annotation> annotation : JAXB_ANNOTATIONS)
		{
			Annotation a = getFieldAnnotation(annotation, field, srcPos);
			if (a != null)
				list.add(a);
		}
		return(list.toArray(new Annotation[list.size()]));
	}

	@Override
	public <A extends Annotation> A getMethodAnnotation(Class<A> annotation, ExecutableElement getter, ExecutableElement setter, Locatable srcpos)
	{
		A a = getter.getAnnotation(annotation);
		if (a == null)
			a = setter.getAnnotation(annotation);
		
		return(a);
	}

	@Override
	public boolean hasMethodAnnotation(Class<? extends Annotation> annotation, String propertyName, ExecutableElement getter, ExecutableElement setter,
			Locatable srcPos)
	{
		return(getMethodAnnotation(annotation, getter, setter, srcPos) != null);
	}

	@Override
	public Annotation[] getAllMethodAnnotations(ExecutableElement method, Locatable srcPos)
	{
		List<Annotation> list = new ArrayList<>();
		for (Class<? extends Annotation> annotation : JAXB_ANNOTATIONS)
		{
			Annotation a = getMethodAnnotation(annotation, method, srcPos);
			if (a != null)
				list.add(a);
		}
		return(list.toArray(new Annotation[list.size()]));
	}

	@Override
	public <A extends Annotation> A getMethodAnnotation(Class<A> annotation, ExecutableElement method, Locatable srcpos)
	{
		return(method.getAnnotation(annotation));
	}

	@Override
	public boolean hasMethodAnnotation(Class<? extends Annotation> annotation, ExecutableElement method)
	{
		return(method.getAnnotation(annotation) != null);
	}

	@Override
	public <A extends Annotation> A getMethodParameterAnnotation(Class<A> annotation, ExecutableElement method, int paramIndex, Locatable srcPos)
	{
		return(method.getParameters().get(paramIndex).getAnnotation(annotation));
	}

	@Override
	public <A extends Annotation> A getClassAnnotation(Class<A> annotation, DeclaredType clazz, Locatable srcpos)
	{
		return(clazz.asElement().getAnnotation(annotation));
	}

	@Override
	public <A extends Annotation> A getPackageAnnotation(Class<A> annotation, DeclaredType clazz, Locatable srcpos)
	{
		PackageElement pkg = processingEnv.getElementUtils().getPackageOf(clazz.asElement());
		if (pkg == null)
			return(null);
		
		return(pkg.getAnnotation(annotation));
	}

	@Override
	public DeclaredType getClassValue(Annotation a, String name)
	{
		//We don't need this for our functionality
		//TODO could implement this if I wasn't lzy
		return(null);
	}

	@Override
	public DeclaredType[] getClassArrayValue(Annotation a, String name)
	{
		//We don't need this for our functionality
		//TODO could implement this if I wasn't lzy
		return(new DeclaredType[0]);
	}
	
}
