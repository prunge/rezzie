package au.net.causal.rezzie.apt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.TypeElement;

public class BundleElement
{
	private final TypeElement type;
	private final List<ResourceMethod> methods = new ArrayList<>();

	public BundleElement(TypeElement type)
	{
		if (type == null)
			throw new NullPointerException("type == null");
		
		this.type = type;
	}
	
	public TypeElement getType()
	{
		return(type);
	}
	
	public List<? extends ResourceMethod> getMethods()
	{
		return(Collections.unmodifiableList(methods));
	}
	
	public void addMethod(ResourceMethod method)
	{
		if (method == null)
			throw new NullPointerException("method == null");
		
		methods.add(method);
	}
	
	public List<? extends DuplicateMethod> findDuplicateMethods()
	{
		List<DuplicateMethod> duplicateList = new ArrayList<>();
		
		Map<String, List<ResourceMethod>> methodMap = new LinkedHashMap<>();
		
		for (ResourceMethod method : methods)
		{
			List<ResourceMethod> list = methodMap.get(method.getName());
			if (list == null)
			{
				list = new ArrayList<>();
				methodMap.put(method.getName(), list);
			}
			list.add(method);
		}
		
		for (Map.Entry<String, List<ResourceMethod>> entry : methodMap.entrySet())
		{
			if (entry.getValue().size() > 1)
			{
				DuplicateMethod dm = new DuplicateMethod(entry.getKey(), entry.getValue());
				duplicateList.add(dm);
			}
		}
		
		return(duplicateList);
	}
	
	public Set<? extends MappedXmlType> getAllParameterXmlTypes()
	{
		Set<MappedXmlType> parameterTypes = new LinkedHashSet<>();
		
		for (ResourceMethod method : methods)
		{
			parameterTypes.addAll(method.getAllParameterXmlTypes());
		}
		
		return(parameterTypes);
	}
	
	public Set<? extends MappedXmlType> getAllChoiceXmlTypes()
	{
		Set<MappedXmlType> choiceTypes = new LinkedHashSet<>();
		
		for (ResourceMethod method : methods)
		{
			choiceTypes.addAll(method.getAllChoiceXmlTypes());
		}
		
		return(choiceTypes);
	}
	
	public static class DuplicateMethod
	{
		private final String name;
		private final List<? extends ResourceMethod> methods;
		
		public DuplicateMethod(String name, List<? extends ResourceMethod> methods)
		{
			this.name = name;
			this.methods = Collections.unmodifiableList(new ArrayList<>(methods));
		}

		public String getName()
		{
			return(name);
		}

		public List<? extends ResourceMethod> getMethods()
		{
			return(methods);
		}
	}

}
