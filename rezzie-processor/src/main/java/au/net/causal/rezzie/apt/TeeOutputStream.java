package au.net.causal.rezzie.apt;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class TeeOutputStream extends FilterOutputStream
{
	private final OutputStream branch;

	public TeeOutputStream(OutputStream primary, OutputStream branch)
	{
		super(primary);
		
		if (primary == null)
			throw new NullPointerException("primary == null");
		if (branch == null)
			throw new NullPointerException("branch == null");
		
		this.branch = branch;
	}

	@Override
	public void write(int b) throws IOException
	{
		super.write(b);
		branch.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException
	{
		super.write(b);
		branch.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{
		super.write(b, off, len);
		branch.write(b, off, len);
	}

	@Override
	public void flush() throws IOException
	{
		super.flush();
		branch.flush();
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		branch.close();
	}
}
