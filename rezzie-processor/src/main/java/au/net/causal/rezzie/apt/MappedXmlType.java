package au.net.causal.rezzie.apt;

import javax.xml.namespace.QName;

public class MappedXmlType
{
	private final Class<?> jaxbType;
	private final QName xmlTypeName;
	
	public MappedXmlType(Class<?> jaxbType, QName xmlTypeName)
	{
		if (jaxbType == null)
			throw new NullPointerException("jaxbType == null");
		if (xmlTypeName == null)
			throw new NullPointerException("xmlTypeName == null");
		
		this.jaxbType = jaxbType;
		this.xmlTypeName = xmlTypeName;
	}

	public Class<?> getJaxbType()
	{
		return(jaxbType);
	}

	public QName getXmlTypeName()
	{
		return(xmlTypeName);
	}

	@Override
	public int hashCode()
	{
		return(getJaxbType().hashCode());
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MappedXmlType other = (MappedXmlType) obj;
		if(!jaxbType.equals(other.jaxbType))
			return false;
		
		return true;
	}
	
	
}
