package au.net.causal.rezzie.apt;

import javax.lang.model.element.VariableElement;

public class ResourceParameter
{
	private final VariableElement parameter;
	private final MappedXmlType parameterXmlType;

	public ResourceParameter(VariableElement parameter, MappedXmlType parameterXmlType)
	{
		if (parameter == null)
			throw new NullPointerException("parameter == null");
		if (parameterXmlType == null)
			throw new NullPointerException("parameterXmlType == null");
		
		this.parameter = parameter;
		this.parameterXmlType = parameterXmlType;
	}
	
	public VariableElement getParameter()
	{
		return(parameter);
	}
	
	public String getName()
	{
		return(getParameter().getSimpleName().toString());
	}
	
	public MappedXmlType getParameterXmlType()
	{
		return(parameterXmlType);
	}

}
