package au.net.causal.rezzie.apt;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.tools.FileObject;

/**
 * Writes data to memory, then when the stream is closed writes the data to multiple {@link FileObject}s.
 * <p>
 * 
 * Normally I'd use a tee output stream to write to each output file simultaneously, therefore not having to store data in memory.  However, due to a 
 * feature/bug in Eclipse APT, for some bizarre reason when opening files with the same name in source and class output folders, teeing causes data to be 
 * duplicated in the class output folder's file.  My guess is they are trying to do some smart trickery by copying these files as they are being written by 
 * an annotation processor, but it is stuffing us up big time.
 * 
 * @author prunge
 */
public class MultiTargetOutputStream extends FilterOutputStream
{
	private final ByteArrayOutputStream bos;
	private final FileObject[] files;
	private boolean closed;
	
	public MultiTargetOutputStream(FileObject... files)
	{
		super(new ByteArrayOutputStream());
		bos = (ByteArrayOutputStream)out;
		this.files = files;
	}

	@Override
	public void close() throws IOException
	{
		if (closed)
			return;
		
		super.close();
		
		//Now we write to each file object
		byte[] data = bos.toByteArray();
		for (FileObject file : files)
		{
			try (OutputStream os = file.openOutputStream())
			{
				os.write(data);
			}
		}
		
		closed = true;
	}
}
