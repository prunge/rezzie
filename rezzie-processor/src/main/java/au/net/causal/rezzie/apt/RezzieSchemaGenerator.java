package au.net.causal.rezzie.apt;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.jvnet.jaxb.reflection.JAXBModelFactory;
import org.jvnet.jaxb.reflection.model.core.EnumConstant;
import org.jvnet.jaxb.reflection.model.core.EnumLeafInfo;
import org.jvnet.jaxb.reflection.model.core.TypeInfoSet;

import au.net.causal.rezzie.annotation.DefaultLocale;
import au.net.causal.rezzie.annotation.NoDefaultLocale;
import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.annotation.WhiteSpaceMode;
import au.net.causal.rezzie.apt.BundleElement.DuplicateMethod;
import au.net.causal.rezzie.apt.ChoiceResourceParameter.EnumSubstitutedAttribute;
import au.net.causal.rezzie.apt.ElementReader.ProblemHandler;
import au.net.causal.rezzie.apt.jaxb.AptAnnotationReader;
import au.net.causal.rezzie.apt.jaxb.AptNavigator;
import au.net.causal.rezzie.apt.jaxb.IgnoreErrorHandler;
import au.net.causal.rezzie.locator.RezzieResourceLocator;

@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes("au.net.causal.rezzie.annotation.ResourceBundleTemplate")
@SupportedOptions(RezzieSchemaGenerator.OPTION_DEBUG)
public class RezzieSchemaGenerator extends AbstractProcessor
{
	public static final String OPTION_DEBUG = "au.net.causal.rezzie.apt.RezzieSchemaGenerator.debug";
	
	private boolean debugMode;
	private DeclaredType stringType;
	private DeclaredType enumType;
	
	private XMLOutputFactory xmlOutputFactory;
	
	private ElementReader elementReader;
	
	private final RezzieResourceLocator resourceLocator = new RezzieResourceLocator();
	
	private Map<String, EnumLeafInfo<TypeMirror, DeclaredType>> jaxbEnumInfoCache = new HashMap<>();
	
	@Override
	public void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);
		debugMode = Boolean.parseBoolean(processingEnv.getOptions().get(OPTION_DEBUG));
		
		TypeElement stringElement = processingEnv.getElementUtils().getTypeElement(String.class.getCanonicalName());
		if (stringElement == null)
			processingEnv.getMessager().printMessage(Kind.ERROR, "String type could not be found.");
		else
			stringType = processingEnv.getTypeUtils().getDeclaredType(stringElement);
		
		TypeElement enumElement = processingEnv.getElementUtils().getTypeElement(Enum.class.getCanonicalName());
		if (enumElement == null)
			processingEnv.getMessager().printMessage(Kind.ERROR,  "Enum type could not be found.");
		else
			enumType = processingEnv.getTypeUtils().getDeclaredType(enumElement);
		
		xmlOutputFactory = XMLOutputFactory.newInstance();
		elementReader = new ElementReader(processingEnv.getTypeUtils());
	}
	
	private void debug(String message)
	{
		if (debugMode)
			processingEnv.getMessager().printMessage(Kind.NOTE, message);
	}
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		Set<MappedXmlType> importTypes = new LinkedHashSet<>();
		
		for (Element element : roundEnv.getElementsAnnotatedWith(ResourceBundleTemplate.class))
		{
			if (element.getKind() != ElementKind.INTERFACE && element instanceof TypeElement)
				processingEnv.getMessager().printMessage(Kind.ERROR, "Resource bundle templates must be interfaces.", element);
			else
			{
				TypeElement type = (TypeElement)element;
				
				try
				{
					BundleElement bundle = processType(type);
					importTypes.addAll(bundle.getAllParameterXmlTypes());
					importTypes.addAll(bundle.getAllChoiceXmlTypes());
				}
				catch (IOException e)
				{
					processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error creating schema file for " + type.getQualifiedName() + ": " + e, type);
				}
				catch (XMLStreamException e)
				{
					processingEnv.getMessager().printMessage(Kind.ERROR, "XML error creating schema file for " + type.getQualifiedName() + ": " + e, type);
				}
				
				try
				{
					writeTemplates(type);
				}
				catch (IOException e)
				{
					processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error creating template XML file for " + type.getQualifiedName() + ": " + e, type);
				}
				catch (XMLStreamException e)
				{
					processingEnv.getMessager().printMessage(Kind.ERROR, "XML error creating template XML file for " + type.getQualifiedName() + ": " + e, type);
				}
			}
		}
		
		//Generate the schema files for the used parameter types
		for (MappedXmlType importType : importTypes)
		{
			try
			{
				generateParameterSchema(importType);
			}
			catch (IOException e)
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error creating schema file for parameter configuration type " + importType.getJaxbType().getCanonicalName() + ": " + e);
			}
			catch (JAXBException e)
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "XML error creating schema file for parameter configuration type " +  importType.getJaxbType().getCanonicalName() + ": " + e);
			}
			/*
			catch (Throwable t)
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "Some other error occurred: " + t);
				t.printStackTrace();
			}
			*/
		}
		
		return(false);
	}
	
	/**
	 * Creates a stream that writes to files both in the source output and the class output locations.
	 * 
	 * @param fileName the name of the file, relative to the base directory.
	 * 
	 * @return an output stream.
	 * 
	 * @throws IOException if an I/O error occurs.
	 */
	private OutputStream createSourceAndTargetFile(String fileName)
	throws IOException
	{
		FileObject sourceFile = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", fileName);
		FileObject targetFile = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", fileName);
		
		debug("Source file: " + sourceFile.toUri());
		debug("Target file: " + targetFile.toUri());
		
		MultiTargetOutputStream os = new MultiTargetOutputStream(sourceFile, targetFile);
		return(os);
		
		//TODO one day when Eclipse irons out its APT bugs we can re-enable this to save memory
		/*
		OutputStream sourceOs = sourceFile.openOutputStream();
		try
		{
			OutputStream targetOs = targetFile.openOutputStream();
			TeeOutputStream os = new TeeOutputStream(sourceOs, targetOs);
			return(os);
		}
		catch (IOException e)
		{
			//Target open failed, close the source
			try
			{
				sourceOs.close();
			}
			catch (IOException ex)
			{
				e.addSuppressed(ex);
			}
			
			throw e;
		}
		*/
	}
	
	private void generateParameterSchema(final MappedXmlType pInfo)
	throws IOException, JAXBException
	{
		final String fileName = resourceLocator.getParameterSchemaName(pInfo.getJaxbType());
		try (OutputStream schemaOs = createSourceAndTargetFile(fileName))
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(pInfo.getJaxbType());
			jaxbContext.generateSchema(new SchemaOutputResolver()
			{
				@Override
				public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
				{
					if (pInfo.getXmlTypeName().getNamespaceURI().equals(namespaceUri))
					{
						StreamResult result = new StreamResult(schemaOs);
						result.setSystemId(fileName);
						return(result);
					}
					else
						return(null);
				}
			});
		}
	}
	
	private BundleElement processType(TypeElement type)
	throws IOException, XMLStreamException
	{
		debug("Processing " + type.getQualifiedName());
		
		BundleElement bundleElement = elementReader.read(type, new AptProblemHandler());
		
		URI fullSchemaNamespaceUri = resourceLocator.getCompleteSchemaNamespaceUri(type);
		String fullFileName = resourceLocator.getCompleteSchemaName(type);
		generateSchema(bundleElement, fullSchemaNamespaceUri, fullFileName, false);

		URI partialSchemaNamespaceUri = resourceLocator.getPartialSchemaNamespaceUri(type);
		String partialFileName = resourceLocator.getPartialSchemaName(type);
		generateSchema(bundleElement, partialSchemaNamespaceUri, partialFileName, true);
		
		checkDefaultLocale(type);
		
		return(bundleElement);
	}
	
	private void generateSchema(BundleElement bundle, URI schemaNamespaceUri, String fileName, boolean partial)
	throws IOException, XMLStreamException
	{
		try (OutputStream schemaOs = createSourceAndTargetFile(fileName))
		{
			XMLStreamWriter schemaWriter = xmlOutputFactory.createXMLStreamWriter(schemaOs, StandardCharsets.UTF_8.name());
			try
			{
				//Write the schema header
				schemaWriter.writeStartDocument();
				schemaWriter.writeStartElement("xsd", "schema", XMLConstants.W3C_XML_SCHEMA_NS_URI);
				schemaWriter.writeNamespace("xsd", XMLConstants.W3C_XML_SCHEMA_NS_URI);
				schemaWriter.writeNamespace("r", schemaNamespaceUri.toString());
				schemaWriter.writeAttribute("targetNamespace", schemaNamespaceUri.toString());
				schemaWriter.writeAttribute("elementFormDefault", "qualified");
				
				//Imports for parameter types
				Set<MappedXmlType> importTypes = new LinkedHashSet<>();
				importTypes.addAll(bundle.getAllParameterXmlTypes());
				importTypes.addAll(bundle.getAllChoiceXmlTypes());
				for (MappedXmlType mType : importTypes)
				{
					schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "import");
					schemaWriter.writeAttribute("namespace", mType.getXmlTypeName().getNamespaceURI());
					
					String parameterSchemaFileName = resourceLocator.getParameterSchemaName(mType.getJaxbType());
					String relativeName = resourceLocator.relativizeFromFile(parameterSchemaFileName, fileName);
					schemaWriter.writeAttribute("schemaLocation", relativeName);
					schemaWriter.writeEndElement();
				}
				
				//Root element
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
				schemaWriter.writeAttribute("name", "bundle");
				schemaWriter.writeAttribute("type", "r:BundleType");
				schemaWriter.writeEndElement(); //element
				
				//Start of complex type definition
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexType");
				schemaWriter.writeAttribute("name", "BundleType");
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "all");
				
				//Iterate over all methods, generating their code
				for (ResourceMethod method : bundle.getMethods())
				{
					processMethod(method, bundle, schemaNamespaceUri, schemaWriter, partial);
				}
				
				//Validate that there aren't methods with the same name
				for (DuplicateMethod dupeMethod : bundle.findDuplicateMethods())
				{
					for (ResourceMethod method : dupeMethod.getMethods())
					{
						processingEnv.getMessager().printMessage(Kind.ERROR, "Multiple methods with the same name found.  Resource bundle template methods must have unique names.", method.getMethod());
					}					
				}
				
				schemaWriter.writeEndElement(); //xsd:all
				
				//White space configuration attributes
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attributeGroup");
				schemaWriter.writeAttribute("ref", "r:RA_WhiteSpaceAttributes");
				schemaWriter.writeEndElement(); //attributeGroup
				
				schemaWriter.writeEndElement(); //xsd:complexType

				//Generate some internal types and groups for whitespace handling, etc.
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attributeGroup");
				schemaWriter.writeAttribute("name", "RA_WhiteSpaceAttributes");
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attribute");
				schemaWriter.writeAttribute("name", "whiteSpace");
				schemaWriter.writeAttribute("type", "r:RT_WhiteSpaceEnum");
				schemaWriter.writeEndElement(); //attribute
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attribute");
				schemaWriter.writeAttribute("name", "parameterWhiteSpace");
				schemaWriter.writeAttribute("type", "r:RT_WhiteSpaceEnum");
				schemaWriter.writeEndElement(); //attribute
				schemaWriter.writeEndElement(); //attributeGroup
				
				writeEnumerationSimpleType("RT_WhiteSpaceEnum", schemaWriter, Arrays.asList(WhiteSpaceMode.PRESERVE, WhiteSpaceMode.REPLACE, WhiteSpaceMode.COLLAPSE));
				
				//Generate the types for the methods
				for (ResourceMethod method : bundle.getMethods())
				{
					generateComponentsForMethod(method, bundle, schemaNamespaceUri, schemaWriter);
				}
				
				//Generate enum substitution types for choices
				Set<String> generatedEnumSubstitutionTypes = new HashSet<>();
				for (ResourceMethod method : bundle.getMethods())
				{
					for (ChoiceResourceParameter param : method.getChoiceParameters())
					{
						//Only for enum parameter types
						TypeMirror parameterTypeMirror = param.getParameter().asType();
						if (parameterTypeMirror.getKind() == TypeKind.DECLARED && parameterTypeMirror instanceof DeclaredType)
						{
							DeclaredType parameterType = (DeclaredType)parameterTypeMirror;
							boolean isAnEnum = processingEnv.getTypeUtils().isAssignable(parameterType, enumType);
							//TODO verify there is no Eclipse bug messing us up here
							debug("Is " + parameterType + " assignable to " + enumType + ": " + isAnEnum);
							if (isAnEnum)
								generateEnumSubstitutionType(param, parameterType, method.getMethod(), schemaWriter, generatedEnumSubstitutionTypes);
						}
					}
				}
				
				//Write XML ending				
				schemaWriter.writeEndElement(); //xsd:schema
				schemaWriter.writeEndDocument();
			}
			finally
			{
				schemaWriter.close();
			}
		}
	}
	
	private EnumLeafInfo<TypeMirror, DeclaredType> getJaxbEnumInfo(TypeMirror enumType)
	{
		if (enumType.getKind() == TypeKind.DECLARED && enumType instanceof DeclaredType)
		{
			DeclaredType dType = (DeclaredType)enumType;
			EnumLeafInfo<TypeMirror, DeclaredType> info = jaxbEnumInfoCache.get(dType.toString());
			if (info == null)
			{
				TypeInfoSet<TypeMirror, DeclaredType, VariableElement, ExecutableElement> infoSet = 
						JAXBModelFactory.create(new AptAnnotationReader(processingEnv), new AptNavigator(processingEnv), new IgnoreErrorHandler(), Collections.singleton(dType));
				
				info = infoSet.enums().get(dType);
				
				if (info != null)
					jaxbEnumInfoCache.put(dType.toString(), info);
			}
			
			return(info);
		}
		else
			return(null);
	}
	
	private void generateEnumSubstitutionType(ChoiceResourceParameter parameter, DeclaredType parameterType, Element method, XMLStreamWriter schemaWriter, Set<String> alreadyGeneratedEnumSubstitutionTypes)
	throws XMLStreamException
	{
		EnumLeafInfo<TypeMirror, DeclaredType> enumInfo = getJaxbEnumInfo(parameterType);
		if (enumInfo == null)
		{
			processingEnv.getMessager().printMessage(Kind.ERROR, "Error looking up JAXB information for enum " + parameterType.toString(), method);
			return;
		}
		
		String subTypeName = parameter.enumSubstitutedTypeName(enumInfo.getTypeName());
		if (alreadyGeneratedEnumSubstitutionTypes.contains(subTypeName))
			return;
		
		debug("Generating enum sub type: " + subTypeName);
		
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexType");
		schemaWriter.writeAttribute("name", subTypeName);
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexContent");
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "restriction");
		schemaWriter.writeNamespace("c", parameter.getChoiceXmlType().getXmlTypeName().getNamespaceURI());
		schemaWriter.writeAttribute("base", "c:" + parameter.getChoiceXmlType().getXmlTypeName().getLocalPart());
		
		//Each enum attribute adds a restriction to enum type
		for (EnumSubstitutedAttribute attribute : parameter.getEnumSubstitutedAttributes())
		{
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attribute");
			schemaWriter.writeAttribute("name", attribute.getAttributeName().getLocalPart());
			if (attribute.isRequired())
				schemaWriter.writeAttribute("use", "required");
			
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "simpleType");
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "restriction");
			if (attribute.getAttributeBaseType().getNamespaceURI().equals(XMLConstants.W3C_XML_SCHEMA_NS_URI))
				schemaWriter.writeAttribute("base", "xsd:" + attribute.getAttributeBaseType().getLocalPart());
			else
			{
				schemaWriter.writeNamespace("e", attribute.getAttributeBaseType().getNamespaceURI());
				schemaWriter.writeAttribute("base", "e:" + attribute.getAttributeBaseType().getLocalPart());
			}
			
			//The enumeration values
			for (EnumConstant<?, ?> c : enumInfo.getConstants())
			{
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "enumeration");
				schemaWriter.writeAttribute("value", c.getLexicalValue());
				schemaWriter.writeEndElement();
			}
		
			schemaWriter.writeEndElement(); //restriction
			schemaWriter.writeEndElement(); //simpleType
			schemaWriter.writeEndElement(); //attribute
		}
		
		schemaWriter.writeEndElement(); //restriction
		schemaWriter.writeEndElement(); //complexContent
		schemaWriter.writeEndElement(); //complexType
		
		alreadyGeneratedEnumSubstitutionTypes.add(subTypeName);
	}
	
	private <E extends Enum<E>> void writeEnumerationSimpleType(String name, XMLStreamWriter schemaWriter, Collection<? extends E> enumValues)
	throws XMLStreamException
	{
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "simpleType");
		schemaWriter.writeAttribute("name", name);
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "restriction");
		schemaWriter.writeAttribute("base", "xsd:token");
		
		for (E item : enumValues)
		{
			String itemName = item.name();
			itemName = itemName.toLowerCase(Locale.ENGLISH);
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "enumeration");
			schemaWriter.writeAttribute("value", itemName);
			schemaWriter.writeEndElement(); //enumeration
		}
		
		schemaWriter.writeEndElement(); //restriction
		schemaWriter.writeEndElement(); //simpleType
	}
	
	private void generateComplexTypeForMethod(ChoiceResourceParameter choiceParameter, ResourceMethod method, BundleElement bundle, URI schemaNamespaceUri, XMLStreamWriter schemaWriter, boolean otherwise)
	throws IOException, XMLStreamException
	{
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexType");
		if (otherwise)
			schemaWriter.writeAttribute("name", method.getXmlOtherwiseComplexTypeName());
		else if (choiceParameter == null)
			schemaWriter.writeAttribute("name", method.getXmlBaseComplexTypeName());
		else
			schemaWriter.writeAttribute("name", method.getXmlChoiceComplexTypeName(choiceParameter));
		
		schemaWriter.writeAttribute("mixed", "true");
		
		if (choiceParameter != null)
		{
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexContent");
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "extension");
			
			EnumLeafInfo<TypeMirror, DeclaredType> enumInfo = getJaxbEnumInfo(choiceParameter.getParameter().asType());
			
			if (enumInfo == null || choiceParameter.getEnumSubstitutedAttributes().isEmpty())
			{
				schemaWriter.writeNamespace("c", choiceParameter.getChoiceXmlType().getXmlTypeName().getNamespaceURI());
				schemaWriter.writeAttribute("base", "c:" + choiceParameter.getChoiceXmlType().getXmlTypeName().getLocalPart());
			}
			else
			{
				QName enumTypeName = enumInfo.getTypeName();
				schemaWriter.writeAttribute("base", "r:" + choiceParameter.enumSubstitutedTypeName(enumTypeName));
			}
		}
		
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "group");
		schemaWriter.writeAttribute("ref", "r:" + method.getXmlGroupName());
		
		schemaWriter.writeAttribute("minOccurs", "0");
		schemaWriter.writeAttribute("maxOccurs", "unbounded");
		
		schemaWriter.writeEndElement(); //group
		
		//White space configuration attributes (but only for the base type - we don't want people reconfiguring whitespace inside choices)
		if (choiceParameter == null && !otherwise)
		{
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "attributeGroup");
			schemaWriter.writeAttribute("ref", "r:RA_WhiteSpaceAttributes");
			schemaWriter.writeEndElement(); //attributeGroup
		}
		
		if (choiceParameter != null)
		{
			schemaWriter.writeEndElement(); //extension
			schemaWriter.writeEndElement(); //complexContent
		}
		
		schemaWriter.writeEndElement(); //complexType
	}
	
	private void generateGroupForMethod(ResourceMethod method, BundleElement bundle, URI schemaNamespaceUri, XMLStreamWriter schemaWriter)
	throws IOException, XMLStreamException
	{
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "group");
		schemaWriter.writeAttribute("name", method.getXmlGroupName());
		
		//TODO XML Schema 1.1 allows all with maxOccurs=unbounded, so potentially use an all here if we can one day
		
		/*
		if (method.getChoiceParameters().isEmpty())
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "all");
		else
		*/
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "choice");
		
		//Elements for each parameter
		for (ResourceParameter methodParameter : method.getParameters())
		{
			processParameter(methodParameter, method, bundle, schemaNamespaceUri, schemaWriter);		
		}
		
		//If there are choices we add the 'choice' as well
		if (!method.getChoiceParameters().isEmpty())
		{
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
			schemaWriter.writeAttribute("name", "choice");
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "complexType");
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "sequence");
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "choice");
			schemaWriter.writeAttribute("maxOccurs", "unbounded");
			
			for (ChoiceResourceParameter choiceParameter : method.getChoiceParameters())
			{
				schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
				schemaWriter.writeAttribute("name", choiceParameter.getName());
				schemaWriter.writeAttribute("type", "r:" + method.getXmlChoiceComplexTypeName(choiceParameter));
				schemaWriter.writeEndElement(); //element
			}
			
			schemaWriter.writeEndElement(); //choice
			
			//Otherwise element
			schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
			schemaWriter.writeAttribute("name", "otherwise");
			schemaWriter.writeAttribute("type", "r:" + method.getXmlOtherwiseComplexTypeName());
			schemaWriter.writeEndElement(); //element
			
			schemaWriter.writeEndElement(); //sequence
			schemaWriter.writeEndElement(); //complexType
			schemaWriter.writeEndElement(); //element
		}
		
		schemaWriter.writeEndElement(); //all or choice

		schemaWriter.writeEndElement(); //group
	}
	
	private void generateComponentsForMethod(ResourceMethod method, BundleElement bundle, URI schemaNamespaceUri, XMLStreamWriter schemaWriter)
	throws IOException, XMLStreamException
	{
		//Don't need anything if there are no parameters
		if (method.getParameters().isEmpty())
			return;
		
		generateGroupForMethod(method, bundle, schemaNamespaceUri, schemaWriter);
		
		//Base
		generateComplexTypeForMethod(null, method, bundle, schemaNamespaceUri, schemaWriter, false);
		
		//Otherwise
		if (!method.getChoiceParameters().isEmpty())
			generateComplexTypeForMethod(null, method, bundle, schemaNamespaceUri, schemaWriter, true);
		
		//Choice types
		for (ChoiceResourceParameter choiceParameter : method.getChoiceParameters())
		{
			generateComplexTypeForMethod(choiceParameter, method, bundle, schemaNamespaceUri, schemaWriter, false);
		}
	}
	
	/**
	 * Checks whether a default locale or explicitly no default locale has been set up on the type or its package and gives a warning if it hasn't.
	 * 
	 * @param type the resource bundle template type.
	 */
	private void checkDefaultLocale(TypeElement type)
	{
		PackageElement pkg = processingEnv.getElementUtils().getPackageOf(type);
		
		DefaultLocale dLocale = type.getAnnotation(DefaultLocale.class);
		NoDefaultLocale ndLocale = type.getAnnotation(NoDefaultLocale.class);
		Element sourceElement = type;
		if (dLocale == null && ndLocale == null)
		{
			//Attempt lookup on package
			if (pkg != null)
			{
				sourceElement = pkg;
				dLocale = pkg.getAnnotation(DefaultLocale.class);
				ndLocale = pkg.getAnnotation(NoDefaultLocale.class);
			}
		}

		//Eclipse APT tool doesn't cope well with messages for package elements so we workaround by always messaging on the type and add info to the message
		String messagePrefix = "";
		if (sourceElement != type)
		{
			sourceElement = type;
			messagePrefix = "(on package) ";
		}
		
		//At most only one can be set
		if (dLocale != null && ndLocale != null)
			processingEnv.getMessager().printMessage(Kind.ERROR, messagePrefix + "@" + DefaultLocale.class.getSimpleName() + " and @" + NoDefaultLocale.class.getSimpleName() + " cannot be used at the same time.", sourceElement);
		else if (dLocale == null && ndLocale == null)
			processingEnv.getMessager().printMessage(Kind.WARNING, "No default locale has been set for this bundle.  Use " + "@" + DefaultLocale.class.getSimpleName() + " or @" + NoDefaultLocale.class.getSimpleName() + " on this class or its package to set it up.", type);
		else if (dLocale != null)
		{
			//Validate that language on @DefaultLocale is not empty
			String lang = dLocale.language();
			if (lang == null || lang.isEmpty())
				processingEnv.getMessager().printMessage(Kind.ERROR, messagePrefix + "Invalid language on @" + DefaultLocale.class.getSimpleName() + " - language cannot be empty string.", sourceElement);
		}		
	}
	
	private void processMethod(ResourceMethod method, BundleElement bundleElement, URI schemaNamespaceUri, XMLStreamWriter schemaWriter, boolean partial)
	throws XMLStreamException
	{
		validateMethod(method.getMethod(), bundleElement.getType());
		
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
		schemaWriter.writeAttribute("name", method.getName());
		
		if (partial)
			schemaWriter.writeAttribute("minOccurs", "0");
		
		//If the method does not have parameters, type is a straight xsd:string
		if (method.getParameters().isEmpty())
			schemaWriter.writeAttribute("type", "xsd:string");
		else
			schemaWriter.writeAttribute("type", "r:" + method.getXmlBaseComplexTypeName());
		
		schemaWriter.writeEndElement(); //element
	}
	
	private <P> void processParameter(ResourceParameter parameter, ResourceMethod method, BundleElement bundleElement, URI schemaNamespaceUri, 
										XMLStreamWriter schemaWriter)
	throws XMLStreamException
	{
		schemaWriter.writeStartElement(XMLConstants.W3C_XML_SCHEMA_NS_URI, "element");
		schemaWriter.writeNamespace("p", parameter.getParameterXmlType().getXmlTypeName().getNamespaceURI());
		schemaWriter.writeAttribute("name", parameter.getName());
		schemaWriter.writeAttribute("type", "p:" + parameter.getParameterXmlType().getXmlTypeName().getLocalPart());
		
		schemaWriter.writeEndElement(); //element
	}
	
	private void validateMethod(ExecutableElement method, TypeElement type)
	{
		//Must have return type of string
		
		//This logic should work but on Eclipse it fails *sometimes* so do check against type name instead of type itself
		//if (!processingEnv.getTypeUtils().isSameType(stringType, method.getReturnType()))
		
		if (!stringType.toString().equals(method.getReturnType().toString()))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Resource bundle template methods must return " + stringType.toString() + " (actually returns " + method.getReturnType() + ").", method);
	}
	
	private void writeTemplates(TypeElement type)
	throws IOException, XMLStreamException
	{
		URI fullSchemaNamespaceUri = resourceLocator.getCompleteSchemaNamespaceUri(type);
		String fullFileName = resourceLocator.getCompleteBundleTemplateName(type);
		writeTemplate(type, fullSchemaNamespaceUri, fullFileName, false);
		
		URI partialSchemaNamespaceUri = resourceLocator.getPartialSchemaNamespaceUri(type);
		String partialFileName = resourceLocator.getPartialBundleTemplateName(type);
		writeTemplate(type, partialSchemaNamespaceUri, partialFileName, true);		
	}
	
	private void writeTemplate(TypeElement type, 
					URI schemaNamespaceUri, String fileName, boolean partial)
	throws IOException, XMLStreamException
	{
		//Intentionally only write this to source output not binary output
		FileObject xmlFile = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", fileName);
		try (OutputStream xmlOs = xmlFile.openOutputStream())
		{
			XMLStreamWriter xmlWriter = xmlOutputFactory.createXMLStreamWriter(xmlOs, StandardCharsets.UTF_8.name());
			
			try
			{
				xmlWriter.writeStartDocument();
				
				xmlWriter.writeStartElement(XMLConstants.DEFAULT_NS_PREFIX, "bundle", schemaNamespaceUri.toString());
				xmlWriter.writeDefaultNamespace(schemaNamespaceUri.toString());
				xmlWriter.writeNamespace("xsi", XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);
				
				String schemaName;
				if (partial)
					schemaName = resourceLocator.getPartialSchemaName(type);
				else
					schemaName = resourceLocator.getCompleteSchemaName(type);
				
				String schemaRelativeName = resourceLocator.relativizeFromFile(schemaName, fileName);
				xmlWriter.writeAttribute(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI, "schemaLocation", schemaNamespaceUri + " " + schemaRelativeName);
				
				xmlWriter.writeEndElement(); //bundle
				xmlWriter.writeEndDocument();
			}
			finally
			{
				xmlWriter.close();
			}
		}
	}
	
	private class AptProblemHandler implements ProblemHandler
	{
		@Override
		public void handleProblem(String message, Element element)
		{
			processingEnv.getMessager().printMessage(Kind.ERROR, message, element);
		}
	}
}
