package au.net.causal.rezzie.apt.jaxb;

import org.jvnet.jaxb.reflection.model.core.ErrorHandler;
import org.jvnet.jaxb.reflection.runtime.IllegalAnnotationException;

public class IgnoreErrorHandler implements ErrorHandler
{
	@Override
	public void error(IllegalAnnotationException e)
	{
	}

}
