package au.net.causal.rezzie.apt;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.lang.model.element.VariableElement;
import javax.xml.namespace.QName;

public class ChoiceResourceParameter extends ResourceParameter
{
	private final MappedXmlType choiceXmlType;
	private final Set<? extends EnumSubstitutedAttribute> enumSubstitutedAttributes;

	public ChoiceResourceParameter(VariableElement parameter, MappedXmlType parameterXmlType, MappedXmlType choiceXmlType, Set<? extends EnumSubstitutedAttribute> enumSubstitutedAttributes)
	{
		super(parameter, parameterXmlType);
		
		if (choiceXmlType == null)
			throw new NullPointerException("choiceXmlType == null");
		
		this.choiceXmlType = choiceXmlType;
		this.enumSubstitutedAttributes = Collections.unmodifiableSet(new LinkedHashSet<>(enumSubstitutedAttributes)); 
	}

	public MappedXmlType getChoiceXmlType()
	{
		return(choiceXmlType);
	}
	
	public Set<? extends EnumSubstitutedAttribute> getEnumSubstitutedAttributes()
	{
		return(enumSubstitutedAttributes);
	}
	
	public String enumSubstitutedTypeName(QName enumTypeName)
	{
		return("ES_" + getChoiceXmlType().getXmlTypeName().getLocalPart() + "_" + enumTypeName.getLocalPart());
	}
	
	public static class EnumSubstitutedAttribute
	{
		private final QName attributeName;
		private final QName attributeBaseType;
		private final boolean required;
		
		public EnumSubstitutedAttribute(QName attributeName, QName attributeBaseType, boolean required)
		{
			this.attributeName = attributeName;
			this.attributeBaseType = attributeBaseType;
			this.required = required;
		}

		public QName getAttributeName()
		{
			return(attributeName);
		}

		public QName getAttributeBaseType()
		{
			return(attributeBaseType);
		}

		public boolean isRequired()
		{
			return(required);
		}
	}
}
