package au.net.causal.rezzie.service.intergrationtest;

import java.text.DateFormat;
import java.text.spi.DateFormatProvider;
import java.util.Locale;

import au.net.causal.rezzie.service.annotation.ServiceImplementation;

@ServiceImplementation(DateFormatProvider.class)
public class ExistingServiceImpl extends DateFormatProvider
{
	@Override
	public DateFormat getTimeInstance(int style, Locale locale)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public DateFormat getDateInstance(int style, Locale locale)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public DateFormat getDateTimeInstance(int dateStyle, int timeStyle, Locale locale)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Locale[] getAvailableLocales()
	{
		throw new UnsupportedOperationException();
	}
}
