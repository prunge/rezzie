package au.net.causal.rezzie.service.integrationtest;

import static org.junit.Assert.*;

import java.text.spi.DateFormatProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import org.junit.Test;

import au.net.causal.rezzie.service.intergrationtest.ExistingServiceImpl;
import au.net.causal.rezzie.service.intergrationtest.MyService;
import au.net.causal.rezzie.service.intergrationtest.MyServiceImpl;

public class TestProcessor
{
	@Test
	public void testMetaInfServicesWrittenForServiceSpec()
	{
		ServiceLoader<MyService> loader = ServiceLoader.load(MyService.class);
		List<MyService> implList = new ArrayList<>();
		for (MyService impl : loader)
		{
			implList.add(impl);
		}
		
		assertEquals("Wrong implementation count.", 1, implList.size());
		MyService service = implList.get(0);
		assertTrue("Should be implementation instance.", service instanceof MyServiceImpl);
	}

	@Test
	public void testMetaInfServicesWrittenForSpecified()
	{
		ServiceLoader<DateFormatProvider> loader = ServiceLoader.load(DateFormatProvider.class);
		List<DateFormatProvider> implList = new ArrayList<>();
		for (DateFormatProvider impl : loader)
		{
			implList.add(impl);
		}
		
		assertFalse("Must have at least one provider.", implList.isEmpty());
		int implCount = 0;
		for (DateFormatProvider dfp : implList)
		{
			if (dfp instanceof ExistingServiceImpl)
				implCount++;
		}
		assertEquals("Should have my service registered.", implCount, 1);
	}
}
