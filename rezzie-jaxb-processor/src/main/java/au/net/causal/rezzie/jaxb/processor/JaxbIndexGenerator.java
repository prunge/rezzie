package au.net.causal.rezzie.jaxb.processor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import au.net.causal.rezzie.jaxb.annotation.GenerateJaxbIndex;

@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes("*")
@SupportedOptions(JaxbIndexGenerator.OPTION_DEBUG)
public class JaxbIndexGenerator extends AbstractProcessor
{
	public static final String OPTION_DEBUG = "au.net.causal.rezzie.jaxb.processor.JaxbIndexGenerator.debug";
	
	private boolean debugMode;
	
	@Override
	public void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);
		debugMode = Boolean.parseBoolean(processingEnv.getOptions().get(OPTION_DEBUG));
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		debug("Hello from the JAXB index generator processor!");
		Collection<? extends JaxbIndexEntry> entries = buildEntries(roundEnv);
		for (JaxbIndexEntry entry : entries)
		{
			processPackage(entry, roundEnv);
		}
		
		return(false);
	}
	
	private Collection<? extends JaxbIndexEntry> buildEntries(RoundEnvironment roundEnv)
	{
		Map<String, JaxbIndexEntry> entryMap = new LinkedHashMap<>();
		
		for (Element element : roundEnv.getRootElements())
		{
			debug("Root element: " + element);
			if (element.getKind().isClass() && element instanceof TypeElement)
			{
				TypeElement type = (TypeElement)element;
				PackageElement pkg = processingEnv.getElementUtils().getPackageOf(type);
				
				//The sane thing to do would be this:
				//if (pkg.getAnnotation(GenerateJaxbIndex.class) != null)
				//but Eclipse does not always give annotations for a package (esp. if package-info was not modified)
				//so we have to check later
				{
					String pkgName = pkg.getQualifiedName().toString();
					JaxbIndexEntry entry = entryMap.get(pkgName);
					if (entry == null)
					{
						entry = new JaxbIndexEntry(pkg);
						entryMap.put(pkgName, entry);
					}
					entry.addClass(type);
				}
			}
			else if (element.getKind() == ElementKind.PACKAGE && element instanceof PackageElement)
			{
				PackageElement pkg = (PackageElement)element;
				//if (pkg.getAnnotation(GenerateJaxbIndex.class) != null)
				{
					String pkgName = pkg.getQualifiedName().toString();
					JaxbIndexEntry entry = entryMap.get(pkgName);
					if (entry == null)
					{
						entry = new JaxbIndexEntry(pkg);
						entryMap.put(pkgName, entry);
					}
				}
			}
		}
		return(entryMap.values());
	}
	
	private void processPackage(JaxbIndexEntry entry, RoundEnvironment roundEnv)
	{
		PackageElement pkg = entry.getPackage();
		debug("Generating jaxb.index file for " + pkg.getQualifiedName());
		
		debug("Package " + pkg + " " + pkg.getClass());
		
		//Scan evert class and enum in the package
		Set<String> names = new LinkedHashSet<>();
		debug("Starting to iterate the package");
		Collection<? extends Element> enclosedElements = pkg.getEnclosedElements();
		
		//Workaround for Yet Another Eclipse APT Bug - Eclipse always returned empty collection for enclosed elements of package, at least in Kepler
		if (enclosedElements.isEmpty())
		{
			debug("Workaround for Eclipse APT enclosed elements in package bug.");
			enclosedElements = entry.getClasses();
		}
			
		for (Element item : enclosedElements)
		{
			debug("Item: " + item + " " + item.getKind());
			if ((item.getKind() == ElementKind.CLASS || item.getKind() == ElementKind.ENUM) && item instanceof TypeElement)
			{
				TypeElement type = (TypeElement)item;
				String name = type.getSimpleName().toString();
				names.add(name);
			}
		}
		debug("Finished iterating.");
		debug(enclosedElements.toString());
				
		try
		{
			FileObject indexFile = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, pkg.getQualifiedName().toString(), "jaxb.index", pkg);
			
			Set<String> existingContent = attemptToReadJaxbFile(indexFile);
			
			//Since we can't check for existence of annotation on package all the time thanks to Eclipse APT bug we assume it is an annotated package if 
			//existing content exists
			if (pkg.getAnnotation(GenerateJaxbIndex.class) != null || !existingContent.isEmpty())
			{
				//Check for existence of each of these files
				for (Iterator<String> i = existingContent.iterator(); i.hasNext();)
				{
					String curSimpleName = i.next();
					TypeElement te = processingEnv.getElementUtils().getTypeElement(pkg.getQualifiedName() + "." + curSimpleName);
					if (te == null)
					{
						debug("Existing class '" + curSimpleName + "' no longer exists so will be removed.");
						i.remove();
					}
				}
				names.addAll(existingContent);
			
				if (names.isEmpty())
					return;

				List<String> nameList = new ArrayList<>(names);
				Collections.sort(nameList, String.CASE_INSENSITIVE_ORDER);
				
				debug(pkg.getQualifiedName() + ": " + nameList);
	
				debug("Generating index file " + pkg.getQualifiedName().toString() + "/jaxb.index (" + indexFile.toUri() + ")");
				try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(indexFile.openOutputStream(), StandardCharsets.UTF_8)))
				{
					for (String name : nameList)
					{
						w.write(name);
						w.newLine();
					}
				}
			}
		}
		catch (IOException e)
		{
			processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error generating jaxb.index file for " + pkg.getQualifiedName() + ": " + e);
		}
	}
	
	private Set<String> attemptToReadJaxbFile(FileObject file)
	throws IOException
	{
		//First try to open the file using standard API
		try
		{
			try (BufferedReader r = new BufferedReader(new InputStreamReader(file.openInputStream(), StandardCharsets.UTF_8)))
			{
				return(readJaxbFile(r));
			}
		}
		catch (IllegalStateException e)
		{
			//Occurs if file object is output only
			debug("Tried to open " + file.toUri() + " using APT API but it is write-only.");
		}
		
		//Next try to use the URI directly
		try
		{
			Path path = Paths.get(file.toUri());
			if (Files.exists(path))
			{
				try (BufferedReader r = Files.newBufferedReader(path, StandardCharsets.UTF_8))
				{
					return(readJaxbFile(r));
				}
			}
			else
				debug("File " + path + " does not exist so cannot read the service file.");
		}
		catch (FileSystemNotFoundException e)
		{
			//Some weird custom protocol URI, have to give up
			debug("Cannot open URI " + file.toUri());
		}
		
		//If we get here we give up
		return(Collections.emptySet());
		
	}
	
	private Set<String> readJaxbFile(BufferedReader r)
	throws IOException
	{
		Set<String> items = new LinkedHashSet<>();
		
		String line;
		do
		{
			line = r.readLine();
			if (line != null)
			{
				//Trim comments
				int commentStart = line.indexOf('#');
				if (commentStart >= 0)
					line = line.substring(0, commentStart);
				
				line = line.trim();
				
				if (!line.isEmpty())
					items.add(line);
			}
		}
		while (line != null);
		
		return(items);
	}
	
	private void debug(String message)
	{
		if (debugMode)
			processingEnv.getMessager().printMessage(Kind.NOTE, message);
	}
}
