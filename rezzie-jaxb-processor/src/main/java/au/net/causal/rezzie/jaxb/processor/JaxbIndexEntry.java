package au.net.causal.rezzie.jaxb.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

public class JaxbIndexEntry
{
	private final PackageElement pkg;
	private final List<TypeElement> classes = new ArrayList<>();
	
	public JaxbIndexEntry(PackageElement pkg)
	{
		this.pkg = pkg;
	}
	
	public void addClass(TypeElement clazz)
	{
		classes.add(clazz);
	}

	public PackageElement getPackage()
	{
		return(pkg);
	}

	public Collection<? extends TypeElement> getClasses()
	{
		return(Collections.unmodifiableList(classes));
	}
	
	
}
