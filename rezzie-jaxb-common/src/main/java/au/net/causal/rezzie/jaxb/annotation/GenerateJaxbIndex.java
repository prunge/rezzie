package au.net.causal.rezzie.jaxb.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When this annotation exists on a package, the Rezzie JAXB generator will generate <code>jaxb.index</code> files for classes in this package.
 * This allows JAXB to pick these classes up.
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.PACKAGE)
public @interface GenerateJaxbIndex
{
}
