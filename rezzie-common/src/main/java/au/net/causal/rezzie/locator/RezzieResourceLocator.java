package au.net.causal.rezzie.locator;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.lang.model.element.TypeElement;

/**
 * Contains the logic for where bundle schema and files are.
 * 
 * @author prunge
 */
public class RezzieResourceLocator
{
	public String getCompleteSchemaName(Class<?> bundleInterface)
	{
		return(getCompleteSchemaName(bundleInterface.getCanonicalName()));
	}
	
	public String getCompleteSchemaName(TypeElement bundleInterface)
	{
		return(getCompleteSchemaName(bundleInterface.getQualifiedName().toString()));
	}
	
	private String getCompleteSchemaName(String bundleInterfaceName)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterfaceName.replace('.', '/') + "/schema.xsd";
		return(fileName);
	}
	
	public String getPartialSchemaName(Class<?> bundleInterface)
	{
		return(getPartialSchemaName(bundleInterface.getCanonicalName()));
	}
	
	public String getPartialSchemaName(TypeElement bundleInterface)
	{
		return(getPartialSchemaName(bundleInterface.getQualifiedName().toString()));
	}
	
	private String getPartialSchemaName(String bundleInterfaceName)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterfaceName.replace('.', '/') + "/schema-partial.xsd";
		return(fileName);
	}
	
	public String getParameterSchemaName(Class<?> parameterConfigurationType)
	{
		String fileName = "META-INF/rezzie/parameter/" + parameterConfigurationType.getCanonicalName().replace('.', '/') + ".xsd";
		return(fileName);
	}
	
	public String getCompleteBundleTemplateName(TypeElement bundleInterface)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterface.getQualifiedName().toString().replace('.', '/') + "/bundle.xml.template";
		return(fileName);
	}
	
	public String getPartialBundleTemplateName(TypeElement bundleInterface)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterface.getQualifiedName().toString().replace('.', '/') + "/bundle-partial.xml.template";
		return(fileName);
	}
	
	public String getBundleName(Class<?> bundleInterface, Locale locale)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterface.getCanonicalName().replace('.', '/') + "/" + locale.toLanguageTag() + ".xml";
		return(fileName);
	}
	
	public String getRemapName(Class<?> bundleInterface, Locale locale)
	{
		String fileName = "META-INF/rezzie/bundle/" + bundleInterface.getCanonicalName().replace('.',  '/') + "/" + locale.toLanguageTag() + ".remap";
		return(fileName);
	}
	
	public String relativizeFromFile(String resourceName, String fromFile)
	{
		List<String> resourceSegments = Arrays.asList(resourceName.split(Pattern.quote("/")));
		List<String> fromSegments = Arrays.asList(fromFile.split(Pattern.quote("/")));

		//Trim off the last file name
		fromSegments = fromSegments.subList(0, fromSegments.size() - 1);
		
	    int commonCount = 0; 
	    int commonLength = 0;
	    int maxCount = Math.min(resourceSegments.size(), fromSegments.size());
	    
	    while (commonCount < maxCount) 
	    {
	        String resourceSegment = resourceSegments.get(commonCount);
	        String fromSegment = fromSegments.get(commonCount);
	        if (!resourceSegment.equals(fromSegment)) 
	        	break;
	        
	        commonCount++;
	        commonLength += resourceSegment.length() + 1;
	    }
	    
	    int resourceLength = resourceName.length();
	    int dirsUp = fromSegments.size() - commonCount;
	    String upStr = "../";
	    StringBuilder relativeBuf = new StringBuilder(dirsUp * upStr.length() + resourceLength - commonLength + 1);
	    for (int i = 0; i < dirsUp; i++)
	    {
	    	relativeBuf.append(upStr);
	    }
	    if (commonLength < resourceLength) 
	    	relativeBuf.append(resourceName.substring(commonLength));
	    
	    return(relativeBuf.toString());
	}
	
	public URI getCompleteSchemaNamespaceUri(Class<?> bundleInterface)
	{
		return(getCompleteSchemaNamespaceUri(bundleInterface.getCanonicalName()));
	}
	
	public URI getCompleteSchemaNamespaceUri(TypeElement bundleInterface)
	{
		return(getCompleteSchemaNamespaceUri(bundleInterface.getQualifiedName().toString()));
	}
	
	private URI getCompleteSchemaNamespaceUri(String bundleInterfaceName)
	{
		return(URI.create("urn:rezzie:bundle:complete:" + bundleInterfaceName));
	}
	
	public URI getPartialSchemaNamespaceUri(Class<?> bundleInterface)
	{
		return(getPartialSchemaNamespaceUri(bundleInterface.getCanonicalName()));
	}
	
	public URI getPartialSchemaNamespaceUri(TypeElement bundleInterface)
	{
		return(getPartialSchemaNamespaceUri(bundleInterface.getQualifiedName().toString()));
	}
	
	private URI getPartialSchemaNamespaceUri(String bundleInterfaceName)
	{
		return(URI.create("urn:rezzie:bundle:partial:" + bundleInterfaceName));
	}

}
