package au.net.causal.rezzie.parameter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ParameterMetadataImpl<P> implements ParameterMetadata<P>
{
	private final Method bundleMethod;
	private final int parameterIndex;
	private final Class<P> processorParameterType;

	public ParameterMetadataImpl(Method bundleMethod, int parameterIndex, Class<P> processorParameterType)
	{
		this.bundleMethod = bundleMethod;
		this.parameterIndex = parameterIndex;
		this.processorParameterType = processorParameterType;
	}
	
	private static <A extends Annotation> A findAnnotationOfType(Collection<? extends Annotation> annotations, Class<A> annotationType)
	{
		for (Annotation annotation : annotations)
		{
			if (annotationType.equals(annotation.annotationType()))
				return(annotationType.cast(annotation));
		}
		
		return(null);
	}

	@Override
	public <A extends Annotation> A getParameterAnnotation(Class<A> annotationType)
	{
		if (annotationType == null)
			throw new NullPointerException("annotationType == null");
		
		return(findAnnotationOfType(getParameterAnnotations(), annotationType));
	}

	@Override
	public Collection<? extends Annotation> getParameterAnnotations()
	{
		return(Collections.unmodifiableList(Arrays.asList(bundleMethod.getParameterAnnotations()[parameterIndex])));
	}

	@Override
	public <A extends Annotation> A getMethodAnnotation(Class<A> annotationType)
	{
		if (annotationType == null)
			throw new NullPointerException("annotationType == null");
		
		return(bundleMethod.getAnnotation(annotationType));
	}

	@Override
	public Collection<? extends Annotation> getMethodAnnotations()
	{
		return(Collections.unmodifiableList(Arrays.asList(bundleMethod.getAnnotations())));
	}

	@Override
	public <A extends Annotation> A getBundleAnnotation(Class<A> annotationType)
	{
		if (annotationType == null)
			throw new NullPointerException("annotationType == null");
		
		return(bundleMethod.getDeclaringClass().getAnnotation(annotationType));
	}

	@Override
	public Collection<? extends Annotation> getBundleAnnotations()
	{
		return(Collections.unmodifiableList(Arrays.asList(bundleMethod.getDeclaringClass().getAnnotations())));
	}

	@Override
	public <A extends Annotation> A getPackageAnnotation(Class<A> annotationType)
	{
		if (annotationType == null)
			throw new NullPointerException("annotationType == null");
		
		Package pkg = bundleMethod.getDeclaringClass().getPackage();
		if (pkg == null)
			return(null);
		
		return(pkg.getAnnotation(annotationType));
	}

	@Override
	public Collection<? extends Annotation> getPackageAnnotations()
	{
		Package pkg = bundleMethod.getDeclaringClass().getPackage();
		if (pkg == null)
			return(Collections.emptyList());
		
		return(Collections.unmodifiableList(Arrays.asList(pkg.getAnnotations())));
	}

	@Override
	public <A extends Annotation> A findFirstAnnotation(Class<A> annotationType)
	{
		if (annotationType == null)
			throw new NullPointerException("annotationType == null");
		
		A annotation = getParameterAnnotation(annotationType);
		if (annotation != null)
			return(annotation);
		
		annotation = getMethodAnnotation(annotationType);
		if (annotation != null)
			return(annotation);
		
		annotation = getBundleAnnotation(annotationType);
		if (annotation != null)
			return(annotation);
		
		annotation = getPackageAnnotation(annotationType);
		if (annotation != null)
			return(annotation);
		
		//If we get here it was not found anywhere
		return(null);
	}

	@Override
	public <A extends Annotation> List<? extends A> findAllAnnotations(Class<A> annotationType)
	{
		List<A> annotationList = new ArrayList<>();
		
		A parameterAnnotation = getParameterAnnotation(annotationType);
		if (parameterAnnotation != null)
			annotationList.add(parameterAnnotation);
		
		A methodAnnotation = getMethodAnnotation(annotationType);
		if (methodAnnotation != null)
			annotationList.add(methodAnnotation);
		
		A bundleAnnotation = getBundleAnnotation(annotationType);
		if (bundleAnnotation != null)
			annotationList.add(bundleAnnotation);
			
		A packageAnnotation = getPackageAnnotation(annotationType);
		if (packageAnnotation != null)
			annotationList.add(packageAnnotation);
		
		return(Collections.unmodifiableList(annotationList));
	}

	@Override
	public Class<? extends P> getActualParameterType()
	{
		Class<?> p = bundleMethod.getParameterTypes()[parameterIndex];
		
		//Parameter processors will always deal with wrapper types, so if actual parameter type is primitive we need a hack
		//(primitive Class's type parameters match their wrappers anyway so it is safe)
		if (p.isPrimitive())
		{
			@SuppressWarnings("unchecked")
			Class<? extends P> primitiveType = (Class<? extends P>)p;
			return(primitiveType);
		}
		
		return(bundleMethod.getParameterTypes()[parameterIndex].asSubclass(processorParameterType));
	}
}
