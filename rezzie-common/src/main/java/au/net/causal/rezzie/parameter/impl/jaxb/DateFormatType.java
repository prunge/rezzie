package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlEnumValue;

public enum DateFormatType
{
	@XmlEnumValue("date")
	DATE,
	
	@XmlEnumValue("time")
	TIME,
	
	@XmlEnumValue("date-and-time")
	DATE_AND_TIME;
}