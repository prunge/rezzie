package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlAttribute;

import au.net.causal.rezzie.parameter.annotation.SubstituteWithEnum;

public class EnumChoiceType
{
	private String equals;

	@SubstituteWithEnum
	@XmlAttribute
	public String getEquals()
	{
		return(equals);
	}

	public void setEquals(String equals)
	{
		this.equals = equals;
	}
}
