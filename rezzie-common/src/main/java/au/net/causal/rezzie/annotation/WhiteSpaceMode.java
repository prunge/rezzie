package au.net.causal.rezzie.annotation;

import java.util.regex.Pattern;

/**
 * Specifies how whitespace is handled.
 * <p>
 * 
 * Whitespace rules are the same as how they are handled in XML Schema.  See the 
 * <a href="http://www.w3.org/TR/xmlschema-2/#rf-whiteSpace">XML Schema documentation</a> for more information.
 * 
 * @author prunge
 */
public enum WhiteSpaceMode
{
	/**
	 * No normalization is done, the value is not changed.
	 */
	PRESERVE
	{
		@Override
		public String processString(String s, boolean start, boolean end)
		{
			if (s == null)
				throw new NullPointerException("s == null");
			
			//No changes
			return(s);
		}
	},
	
	/**
	 * All occurrences of #x9 (tab), #xA (line feed) and #xD (carriage return) are replaced with #x20 (space).
	 */
	REPLACE
	{
		private final Pattern WHITESPACE_PATTERN = Pattern.compile("\\s");
		
		@Override
		public String processString(String s, boolean start, boolean end)
		{
			if (s == null)
				throw new NullPointerException("s == null");
			
			return(WHITESPACE_PATTERN.matcher(s).replaceAll(" "));
		}
	},
	
	/**
	 * After the processing implied by {@link #REPLACE}, contiguous sequences of #x20's are collapsed to a single #x20, and leading and trailing #x20's are removed. 
	 */
	COLLAPSE
	{
		private final Pattern MULTI_WHITESPACE_PATTERN = Pattern.compile("\\s+");
		private final Pattern LEADING_WHITESPACE_PATTERN = Pattern.compile("^\\s+");
		private final Pattern TRAILING_WHITESPACE_PATTERN = Pattern.compile("\\s+$");
		
		@Override
		public String processString(String s, boolean start, boolean end)
		{
			if (s == null)
				throw new NullPointerException("s == null");
			
			//Multiple whitespace into single space
			s = MULTI_WHITESPACE_PATTERN.matcher(s).replaceAll(" ");
			
			//Remove leading and trailing whitespace
			if (start)
				s = LEADING_WHITESPACE_PATTERN.matcher(s).replaceAll("");
			if (end)
				s = TRAILING_WHITESPACE_PATTERN.matcher(s).replaceAll("");
			
			return(s);
		}
	},
	
	/**
	 * Use the default behaviour, possibly inherited from the parent element.
	 */
	DEFAULT
	{
		@Override
		public String processString(String s, boolean isStart, boolean isEnd)
		{
			throw new UnsupportedOperationException("Default whitespace mode does not have an action.");
		}
	};
	
	/**
	 * Performs white space transformation on a string.
	 * 
	 * @param s the string to process.
	 * @param start true if the text being processed is the logical start of a block of text.
	 * @param end true if the text being processed is the logical end of a block of text.
	 * 
	 * @return the transformed string.
	 * 
	 * @throws NullPointerException if <code>s</code> is null.
	 */
	public abstract String processString(String s, boolean start, boolean end);
}
