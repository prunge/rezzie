package au.net.causal.rezzie.parameter.impl.jaxb;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAttribute;

public class NumberChoiceType
{
	private BigDecimal equals;
	private BigDecimal greaterThan;
	private BigDecimal greaterThanOrEqualTo;
	private BigDecimal lessThan;
	private BigDecimal lessThanOrEqualTo;
	
	@XmlAttribute
	public BigDecimal getEquals()
	{
		return(equals);
	}

	public void setEquals(BigDecimal equals)
	{
		this.equals = equals;
	}

	@XmlAttribute
	public BigDecimal getGreaterThan()
	{
		return(greaterThan);
	}

	public void setGreaterThan(BigDecimal greaterThan)
	{
		this.greaterThan = greaterThan;
	}

	@XmlAttribute
	public BigDecimal getGreaterThanOrEqualTo()
	{
		return(greaterThanOrEqualTo);
	}

	public void setGreaterThanOrEqualTo(BigDecimal greaterThanOrEqualTo)
	{
		this.greaterThanOrEqualTo = greaterThanOrEqualTo;
	}

	@XmlAttribute
	public BigDecimal getLessThan()
	{
		return(lessThan);
	}

	public void setLessThan(BigDecimal lessThan)
	{
		this.lessThan = lessThan;
	}

	@XmlAttribute
	public BigDecimal getLessThanOrEqualTo()
	{
		return(lessThanOrEqualTo);
	}

	public void setLessThanOrEqualTo(BigDecimal lessThanOrEqualTo)
	{
		this.lessThanOrEqualTo = lessThanOrEqualTo;
	}
}
