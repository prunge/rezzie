package au.net.causal.rezzie.parameter.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.ParameterProcessor;
import au.net.causal.rezzie.parameter.impl.jaxb.DateConfigurationType;
import au.net.causal.rezzie.parameter.impl.jaxb.DateFormatStyle;
import au.net.causal.rezzie.parameter.impl.jaxb.DateFormatType;
import au.net.causal.rezzie.service.annotation.ServiceImplementation;

@ServiceImplementation
public class DateParameterProcessor extends ParameterProcessor<Date, DateConfigurationType>
{
	private static final DateConfigurationType DEFAULT_CONFIG;
	static
	{
		DEFAULT_CONFIG = new DateConfigurationType();
		DEFAULT_CONFIG.setDateStyle(DateFormatStyle.MEDIUM);
		DEFAULT_CONFIG.setTimeStyle(DateFormatStyle.MEDIUM);
		DEFAULT_CONFIG.setType(DateFormatType.DATE_AND_TIME);
	}
	
	public DateParameterProcessor()
	{
		super(Date.class, DateConfigurationType.class);
	}
	
	@Override
	public String parameterToString(Date parameter, DateConfigurationType config, ParameterMetadata<Date> metadata, Locale locale)
	{
		au.net.causal.rezzie.parameter.impl.annotation.DateFormat dfConfig = metadata.findFirstAnnotation(au.net.causal.rezzie.parameter.impl.annotation.DateFormat.class);
		if (dfConfig != null)
		{
			DateConfigurationType configFromAnnotation = DateConfigurationType.fromAnnotation(dfConfig);
			config.merge(configFromAnnotation);
		}
		
		//Defaults
		config.merge(DEFAULT_CONFIG);
		
		String localeStr = config.getLocale();
		if (localeStr != null && localeStr.isEmpty())
			localeStr = null;

		//Override locale if needed
		if (localeStr != null)
			locale = Locale.forLanguageTag(localeStr);
		
		DateFormat df;
		if (config.getPattern() != null)
		{
			df = new SimpleDateFormat(config.getPattern(), locale);
		}
		else
		{
			switch (config.getType())
			{
				case DATE:
					df = DateFormat.getDateInstance(config.getDateStyle().getDateFormatValue(), locale);
					break;
				case TIME:
					df = DateFormat.getTimeInstance(config.getTimeStyle().getDateFormatValue(), locale);
					break;
				case DATE_AND_TIME:
					df = DateFormat.getDateTimeInstance(config.getDateStyle().getDateFormatValue(), config.getTimeStyle().getDateFormatValue(), locale);
					break;
				default:
					throw new Error("Unsupported date format type: " + config.getType());
			}
		}
		
		return(df.format(parameter));
	}
}
