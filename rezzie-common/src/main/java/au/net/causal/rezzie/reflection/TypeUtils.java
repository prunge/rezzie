package au.net.causal.rezzie.reflection;

public final class TypeUtils
{
	private TypeUtils()
	{
	}
	
	@SuppressWarnings("unchecked") //dealing with primitive types is a pain, but each primitive has same type parameter as its wrapper so this is OK
	public static <P> Class<P> wrapperType(Class<P> primitiveType)
	{
		if (!primitiveType.isPrimitive())
			return(primitiveType);
		
		if (primitiveType == int.class)
			return((Class<P>)Integer.class);
		else if (primitiveType == long.class)
			return((Class<P>)Long.class);
		else if (primitiveType == short.class)
			return((Class<P>)Short.class);
		else if (primitiveType == byte.class)
			return((Class<P>)Byte.class);
		else if (primitiveType == double.class)
			return((Class<P>)Double.class);
		else if (primitiveType == float.class)
			return((Class<P>)Float.class);
		else if (primitiveType == char.class)
			return((Class<P>)Character.class);
		else if (primitiveType == boolean.class)
			return((Class<P>)Boolean.class);
		else if (primitiveType == void.class)
			return((Class<P>)Void.class);
		else
			throw new Error("Unknown primitive: " + primitiveType);
	}

}
