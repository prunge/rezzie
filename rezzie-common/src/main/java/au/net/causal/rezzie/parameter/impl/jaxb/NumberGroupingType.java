package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlEnumValue;

public enum NumberGroupingType
{
	@XmlEnumValue("default")
	DEFAULT,

	@XmlEnumValue("on")
	ON,
	
	@XmlEnumValue("off")
	OFF;
}