package au.net.causal.rezzie.parameter.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import au.net.causal.rezzie.parameter.ChoiceParameterProcessor;
import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberChoiceType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberConfigurationType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberFormatType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberGroupingType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberRoundingMode;
import au.net.causal.rezzie.service.annotation.ServiceImplementation;

@ServiceImplementation
public class NumberParameterProcessor extends ChoiceParameterProcessor<Number, NumberConfigurationType, NumberChoiceType>
{
	private final Set<Class<? extends Number>> KNOWN_INTEGER_TYPES = new HashSet<>(Arrays.<Class<? extends Number>>asList(
			Long.class, Integer.class, Short.class, Byte.class, 
			long.class, int.class, short.class, byte.class,
			BigInteger.class, 
			AtomicInteger.class, AtomicLong.class));
	
	/*
	private final Set<Class<? extends Number>> KNOWN_DECIMAL_TYPES = new HashSet<>(Arrays.<Class<? extends Number>>asList(
			Double.class, Float.class,
			double.class, float.class,
			BigDecimal.class));
	*/
	
	private static final NumberConfigurationType DEFAULT_CONFIG;
	static
	{
		DEFAULT_CONFIG = new NumberConfigurationType();
		DEFAULT_CONFIG.setGrouping(NumberGroupingType.DEFAULT);
		DEFAULT_CONFIG.setRoundingMode(NumberRoundingMode.DEFAULT);
		DEFAULT_CONFIG.setType(NumberFormatType.DEFAULT);
	}
	
	public NumberParameterProcessor()
	{
		super(Number.class, NumberConfigurationType.class, NumberChoiceType.class);
	}
	
	@Override
	public String parameterToString(Number parameter, NumberConfigurationType config, ParameterMetadata<Number> metadata, Locale locale)
	{
		if (parameter == null)
			return("");
		
		au.net.causal.rezzie.parameter.impl.annotation.NumberFormat nfConfig = metadata.findFirstAnnotation(au.net.causal.rezzie.parameter.impl.annotation.NumberFormat.class);
		if (nfConfig != null)
		{
			NumberConfigurationType configFromAnnotation = NumberConfigurationType.fromAnnotation(nfConfig);
			config.merge(configFromAnnotation);
		}
		
		//Defaults
		config.merge(DEFAULT_CONFIG);
		
		String localeStr = config.getLocale();
		if (localeStr != null && localeStr.isEmpty())
			localeStr = null;

		//Override locale if needed
		if (localeStr != null)
			locale = Locale.forLanguageTag(localeStr);
		
		NumberFormat nf;
		if (config.getPattern() != null)
		{
			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
			nf = new DecimalFormat(config.getPattern(), dfs);
		}
		else
		{
			switch (config.getType())
			{
				case DEFAULT:
				{
					if (KNOWN_INTEGER_TYPES.contains(metadata.getActualParameterType()))
						nf = NumberFormat.getIntegerInstance(locale);
					else
						nf = NumberFormat.getNumberInstance(locale);
					break;
				}
				case INTEGER:
					nf = NumberFormat.getIntegerInstance(locale);
					break;
				case NUMBER:
					nf = NumberFormat.getNumberInstance(locale);
					break;
				case PERCENT:
					nf = NumberFormat.getPercentInstance(locale);
					break;
				default:
					throw new Error("Unsupported number format type: " + config.getType());
			}
			
			if (config.getGrouping() == NumberGroupingType.ON)
				nf.setGroupingUsed(true);
			else if (config.getGrouping() == NumberGroupingType.OFF)
				nf.setGroupingUsed(false);
			
			if (config.getMinimumIntegerDigits() != null && config.getMinimumIntegerDigits() >= 0)
				nf.setMinimumIntegerDigits(config.getMinimumIntegerDigits());
			if (config.getMaximumIntegerDigits() != null && config.getMaximumIntegerDigits() >= 0)
				nf.setMaximumIntegerDigits(config.getMaximumIntegerDigits());
			if (config.getMinimumFractionDigits() != null && config.getMinimumFractionDigits() >= 0)
				nf.setMinimumFractionDigits(config.getMinimumFractionDigits());
			if (config.getMaximumFractionDigits() != null && config.getMaximumFractionDigits() >= 0)
				nf.setMaximumFractionDigits(config.getMaximumFractionDigits());
		}
		
		//Common settings
		if (config.getRoundingMode() != null && config.getRoundingMode().getMode() != null)
			nf.setRoundingMode(config.getRoundingMode().getMode());
		
		return(nf.format(parameter));
	}
	
	@Override
	public boolean applyChoice(Number parameter, NumberChoiceType choiceConfiguration, ParameterMetadata<Number> parameterMetadata)
	{
		if (parameter == null)
			return(false);
		
		BigDecimal value = numberToBigDecimal(parameter);
		BigDecimal equalsCheck = choiceConfiguration.getEquals();
		BigDecimal greaterThanCheck = choiceConfiguration.getGreaterThan();
		BigDecimal greaterThanOrEqualsCheck = choiceConfiguration.getGreaterThanOrEqualTo();
		BigDecimal lessThanCheck = choiceConfiguration.getLessThan();
		BigDecimal lessThanOrEqualsCheck = choiceConfiguration.getLessThanOrEqualTo();

		//Don't use equals() for comparing BigDecimals because it returns false if scales are different
		
		if (equalsCheck != null)
		{
			if (value.compareTo(equalsCheck) != 0)
				return(false);
		}
		if (greaterThanCheck != null)
		{
			if (value.compareTo(greaterThanCheck) <= 0)
				return(false);
		}
		if (greaterThanOrEqualsCheck != null)
		{
			if (value.compareTo(greaterThanOrEqualsCheck) < 0)
				return(false);
		}
		if (lessThanCheck != null)
		{
			if (value.compareTo(lessThanCheck) >= 0)
				return(false);
		}
		if (lessThanOrEqualsCheck != null)
		{
			if (value.compareTo(lessThanOrEqualsCheck) > 0)
				return(false);
		}
		
		//If we get here it passed
		return(true);
	}
	
	private BigDecimal numberToBigDecimal(Number n)
	{
		if (n instanceof BigDecimal)
			return((BigDecimal)n);
		if (n instanceof BigInteger)
			return(new BigDecimal((BigInteger)n));
		if (n instanceof Long || n instanceof Integer || n instanceof Short || n instanceof Byte || n instanceof AtomicInteger || n instanceof AtomicLong)
			return(new BigDecimal(n.longValue()));
		if (n instanceof Double || n instanceof Float)
			return(new BigDecimal(n.doubleValue()));
		else //if unknown type, take our chances with double representation
			return(new BigDecimal(n.doubleValue()));
	}
}
