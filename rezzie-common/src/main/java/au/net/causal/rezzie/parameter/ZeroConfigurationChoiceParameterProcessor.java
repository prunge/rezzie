package au.net.causal.rezzie.parameter;

import java.util.Locale;

import au.net.causal.rezzie.parameter.impl.jaxb.EmptyType;

public abstract class ZeroConfigurationChoiceParameterProcessor<P, X> extends ChoiceParameterProcessor<P, EmptyType, X>
{
	protected ZeroConfigurationChoiceParameterProcessor(Class<P> parameterType, Class<X> choiceConfigurationType)
	{
		super(parameterType, EmptyType.class, choiceConfigurationType);
	}
	
	@Override
	public final String parameterToString(P parameter, EmptyType configuration, ParameterMetadata<P> metadata, Locale locale)
	{
		return(parameterToString(parameter, locale));
	}
	
	protected abstract String parameterToString(P parameter, Locale locale);
}
