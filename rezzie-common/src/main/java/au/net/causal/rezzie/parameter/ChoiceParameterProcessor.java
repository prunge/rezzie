package au.net.causal.rezzie.parameter;

public abstract class ChoiceParameterProcessor<P, C, X> 
extends ParameterProcessor<P, C>
implements ChoiceSupport<P, X>
{
	private final Class<X> choiceConfigurationType;

	public ChoiceParameterProcessor(Class<P> parameterType, Class<C> configurationType, Class<X> choiceConfigurationType)
	{
		super(parameterType, configurationType);
		
		if (choiceConfigurationType == null)
			throw new NullPointerException("choiceConfigurationType == null");
		
		this.choiceConfigurationType = choiceConfigurationType;
	}
	
	@Override
	public final Class<X> getChoiceConfigurationType()
	{
		return(choiceConfigurationType);
	}
}
