package au.net.causal.rezzie.locale;

public final class Countries
{
	/**
	 * Private constructor to prevent instantiation.
	 */
	private Countries()
	{
	}
	
	public static final String FRANCE = "FR";
    public static final String GERMANY = "DE";
    public static final String ITALY = "IT";
    public static final String JAPAN = "JP";
    public static final String KOREA = "KR";
    public static final String CHINA = "CN";
    public static final String TAIWAN = "TW";
    public static final String UK = "GB";
    public static final String US = "US";
    public static final String CANADA = "CA";
    public static final String AUSTRALIA = "AU";
}
