package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlAttribute;

import au.net.causal.rezzie.parameter.impl.annotation.DateFormat;

public class DateConfigurationType
{
	private String pattern;
	private DateFormatStyle dateStyle;
	private DateFormatStyle timeStyle;
	private DateFormatType type;
	private String locale;
	
	@XmlAttribute
	public String getPattern()
	{
		return(pattern);
	}

	public void setPattern(String pattern)
	{
		this.pattern = pattern;
	}

	@XmlAttribute
	public DateFormatStyle getDateStyle()
	{
		return(dateStyle);
	}

	public void setDateStyle(DateFormatStyle style)
	{
		this.dateStyle = style;
	}
	
	@XmlAttribute
	public DateFormatStyle getTimeStyle()
	{
		return(timeStyle);
	}

	public void setTimeStyle(DateFormatStyle style)
	{
		this.timeStyle = style;
	}

	@XmlAttribute
	public DateFormatType getType()
	{
		return(type);
	}

	public void setType(DateFormatType type)
	{
		this.type = type;
	}
	
	@XmlAttribute
	public String getLocale()
	{
		return(locale);
	}

	public void setLocale(String locale)
	{
		this.locale = locale;
	}
	
	public static DateConfigurationType fromAnnotation(DateFormat annotation)
	{
		if (annotation == null)
			throw new NullPointerException("annotation == null");
		
		DateConfigurationType config = new DateConfigurationType();
		if (!annotation.pattern().isEmpty())
			config.setPattern(annotation.pattern());
		
		config.setDateStyle(annotation.dateStyle());
		config.setTimeStyle(annotation.timeStyle());
		config.setType(annotation.type());
		
		if (!annotation.locale().isEmpty())
			config.setLocale(annotation.locale());
		
		return(config);
	}
	
	public void merge(DateConfigurationType parentConfig)
	{
		if (getPattern() == null)
			setPattern(parentConfig.getPattern());
		
		if (getDateStyle() == null)
			setDateStyle(parentConfig.getDateStyle());
		if (getTimeStyle() == null)
			setTimeStyle(parentConfig.getTimeStyle());
			
		if (getType() == null)
			setType(parentConfig.getType());
		
		if (getLocale() == null)
			setLocale(parentConfig.getLocale());
	}
}
