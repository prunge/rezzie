package au.net.causal.rezzie.parameter.impl.jaxb;

import java.text.DateFormat;

import javax.xml.bind.annotation.XmlEnumValue;

public enum DateFormatStyle
{
	@XmlEnumValue("full")
	FULL(DateFormat.FULL),
	
	@XmlEnumValue("long")
	LONG(DateFormat.LONG),
	
	@XmlEnumValue("medium")
	MEDIUM(DateFormat.MEDIUM),
	
	@XmlEnumValue("short")
	SHORT(DateFormat.SHORT);
	
	private final int value;
	
	private DateFormatStyle(int value)
	{
		this.value = value;
	}
	
	public int getDateFormatValue()
	{
		return(value);
	}
}