package au.net.causal.rezzie.parameter.impl.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import au.net.causal.rezzie.parameter.impl.jaxb.NumberFormatType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberGroupingType;
import au.net.causal.rezzie.parameter.impl.jaxb.NumberRoundingMode;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Documented
public @interface NumberFormat
{
	public NumberFormatType type() default NumberFormatType.NUMBER;
	public NumberGroupingType grouping() default NumberGroupingType.DEFAULT;
	public NumberRoundingMode roundingMode() default NumberRoundingMode.DEFAULT;
	
	public int minimumIntegerDigits() default -1;
	public int maximumIntegerDigits() default -1;
	public int minimumFractionDigits() default -1;
	public int maximumFractionDigits() default -1;
	
	public String pattern() default "";
	public String locale() default "";
}
