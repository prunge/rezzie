package au.net.causal.rezzie.parameter;

import java.util.Locale;

import au.net.causal.rezzie.service.annotation.ServiceSpecification;

@ServiceSpecification
public abstract class ParameterProcessor<P, C>
{
	private final Class<P> parameterType;
	private final Class<C> configurationType;

	protected ParameterProcessor(Class<P> parameterType, Class<C> configurationType)
	{
		if (parameterType == null)
			throw new NullPointerException("parameterType == null");
		if (configurationType == null)
			throw new NullPointerException("configurationType == null");
		
		this.parameterType = parameterType;
		this.configurationType = configurationType;
	}
	
	public Class<P> getParameterType()
	{
		return(parameterType);
	}

	public Class<C> getConfigurationType()
	{
		return(configurationType);
	}

	public abstract String parameterToString(P parameter, C configuration, ParameterMetadata<P> metadata, Locale locale);
}
