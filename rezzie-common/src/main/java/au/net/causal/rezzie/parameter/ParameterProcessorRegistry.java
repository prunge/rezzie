package au.net.causal.rezzie.parameter;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class ParameterProcessorRegistry
{
	private final Map<Class<?>, ParameterProcessor<?, ?>> parameterTypeProcessorMap = new HashMap<>();

	public ParameterProcessorRegistry(ClassLoader classLoader)
	{
		//Reflection can only load raw types
		@SuppressWarnings("rawtypes")
		ServiceLoader<ParameterProcessor> loader = ServiceLoader.load(ParameterProcessor.class, classLoader);
		for (ParameterProcessor<?, ?> processor : loader)
		{
			parameterTypeProcessorMap.put(processor.getParameterType(), processor);
		}
	}
	
	public ParameterProcessorRegistry()
	{
		this(Thread.currentThread().getContextClassLoader());
	}
	
	public <P> ParameterProcessor<? super P, ?> getProcessor(Class<P> parameterType)
	{
		if (parameterType == null)
			throw new NullPointerException("parameterType == null");
		
		//Safe because we guarantee we store processors to their parameter type in the map
		@SuppressWarnings({ "unchecked", "rawtypes" })
		ParameterProcessor<? super P, ?> processor = (ParameterProcessor)parameterTypeProcessorMap.get(parameterType);
		
		if (processor != null)
			return(processor);
		
		//If none, then check superclasses
		Class<? super P> superClass = parameterType.getSuperclass();
		if (superClass != null)
		{
			processor = getProcessor(superClass);
			if (processor != null)
				return(processor);
		}
		
		//If stll none, check interfaces
		for (Class<?> rawInterface : parameterType.getInterfaces())
		{
			//Safe because the interfaces are from parameterType
			@SuppressWarnings("unchecked")
			Class<? super P> safeInterface = (Class<? super P>)rawInterface;
			processor = getProcessor(safeInterface);
			
			if (processor != null)
				return(processor);
		}

		//No processor found if we get here
		return(null);
	}
	
	@Override
	public String toString()
	{
		return(parameterTypeProcessorMap.toString());
	}
	
}
