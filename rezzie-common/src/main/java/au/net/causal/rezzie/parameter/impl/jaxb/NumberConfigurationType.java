package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlAttribute;

import au.net.causal.rezzie.parameter.impl.annotation.NumberFormat;

public class NumberConfigurationType
{
	private NumberFormatType type;
	private NumberGroupingType grouping;
	private NumberRoundingMode roundingMode;
	
	private Integer minimumIntegerDigits;
	private Integer maximumIntegerDigits;
	private Integer minimumFractionDigits;
	private Integer maximumFractionDigits;
	
	private String pattern;
	private String locale;

	@XmlAttribute
	public Integer getMinimumIntegerDigits()
	{
		return(minimumIntegerDigits);
	}

	public void setMinimumIntegerDigits(Integer minimumIntegerDigits)
	{
		this.minimumIntegerDigits = minimumIntegerDigits;
	}

	@XmlAttribute
	public Integer getMaximumIntegerDigits()
	{
		return(maximumIntegerDigits);
	}

	public void setMaximumIntegerDigits(Integer maximumIntegerDigits)
	{
		this.maximumIntegerDigits = maximumIntegerDigits;
	}

	@XmlAttribute
	public Integer getMinimumFractionDigits()
	{
		return(minimumFractionDigits);
	}

	public void setMinimumFractionDigits(Integer minimumFractionDigits)
	{
		this.minimumFractionDigits = minimumFractionDigits;
	}

	@XmlAttribute
	public Integer getMaximumFractionDigits()
	{
		return(maximumFractionDigits);
	}

	public void setMaximumFractionDigits(Integer maximumFractionDigits)
	{
		this.maximumFractionDigits = maximumFractionDigits;
	}

	@XmlAttribute
	public NumberRoundingMode getRoundingMode()
	{
		return(roundingMode);
	}
	
	public void setRoundingMode(NumberRoundingMode roundingMode)
	{
		this.roundingMode = roundingMode;
	}
	
	@XmlAttribute
	public NumberGroupingType getGrouping()
	{
		return(grouping);
	}
	
	public void setGrouping(NumberGroupingType grouping)
	{
		this.grouping = grouping;
	}
	
	@XmlAttribute
	public NumberFormatType getType()
	{
		return(type);
	}
	
	public void setType(NumberFormatType type)
	{
		this.type = type;
	}
	
	@XmlAttribute
	public String getPattern()
	{
		return(pattern);
	}

	public void setPattern(String pattern)
	{
		this.pattern = pattern;
	}

	@XmlAttribute
	public String getLocale()
	{
		return(locale);
	}

	public void setLocale(String locale)
	{
		this.locale = locale;
	}
	
	public static NumberConfigurationType fromAnnotation(NumberFormat annotation)
	{
		if (annotation == null)
			throw new NullPointerException("annotation == null");
		
		NumberConfigurationType config = new NumberConfigurationType();
		if (!annotation.pattern().isEmpty())
			config.setPattern(annotation.pattern());
		
		if (annotation.minimumFractionDigits() >= 0)
			config.setMinimumFractionDigits(annotation.minimumFractionDigits());
		if (annotation.maximumFractionDigits() >= 0)
			config.setMaximumFractionDigits(annotation.maximumFractionDigits());
		if (annotation.minimumIntegerDigits() >= 0)
			config.setMinimumIntegerDigits(annotation.minimumIntegerDigits());
		if (annotation.maximumIntegerDigits() >= 0)
			config.setMaximumIntegerDigits(annotation.maximumIntegerDigits());
		
		config.setRoundingMode(annotation.roundingMode());
		config.setGrouping(annotation.grouping());
		config.setType(annotation.type());
		
		if (!annotation.locale().isEmpty())
			config.setLocale(annotation.locale());
		
		return(config);
	}
	
	public void merge(NumberConfigurationType parentConfig)
	{
		if (getPattern() == null)
			setPattern(parentConfig.getPattern());
		
		if (getMinimumFractionDigits() == null || getMinimumFractionDigits() < 0)
			setMinimumFractionDigits(parentConfig.getMinimumFractionDigits());
		if (getMaximumFractionDigits() == null || getMaximumFractionDigits() < 0)
			setMaximumFractionDigits(parentConfig.getMaximumFractionDigits());
		if (getMinimumIntegerDigits() == null || getMinimumIntegerDigits() < 0)
			setMinimumIntegerDigits(parentConfig.getMinimumIntegerDigits());
		if (getMaximumIntegerDigits() == null || getMaximumIntegerDigits() < 0)
			setMaximumIntegerDigits(parentConfig.getMaximumIntegerDigits());
		
		if (getGrouping() == null)
			setGrouping(parentConfig.getGrouping());
		if (getRoundingMode() == null)
			setRoundingMode(parentConfig.getRoundingMode());
		
		if (getType() == null)
			setType(parentConfig.getType());
		
		if (getLocale() == null)
			setLocale(parentConfig.getLocale());
	}
}
