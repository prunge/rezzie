package au.net.causal.rezzie.parameter.impl.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import au.net.causal.rezzie.parameter.impl.jaxb.DateFormatStyle;
import au.net.causal.rezzie.parameter.impl.jaxb.DateFormatType;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Documented
public @interface DateFormat
{
	public DateFormatType type() default DateFormatType.DATE_AND_TIME;
	public DateFormatStyle dateStyle() default DateFormatStyle.MEDIUM;
	public DateFormatStyle timeStyle() default DateFormatStyle.MEDIUM;
	public String pattern() default "";
	public String locale() default "";
}
