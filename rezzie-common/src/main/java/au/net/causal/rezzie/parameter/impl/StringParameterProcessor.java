package au.net.causal.rezzie.parameter.impl;

import java.util.Locale;

import au.net.causal.rezzie.parameter.ZeroConfigurationParameterProcessor;
import au.net.causal.rezzie.service.annotation.ServiceImplementation;

@ServiceImplementation
public class StringParameterProcessor extends ZeroConfigurationParameterProcessor<String>
{
	public StringParameterProcessor()
	{
		super(String.class);
	}
	
	@Override
	protected String parameterToString(String parameter, Locale locale)
	{
		return(parameter);
	}
}
