package au.net.causal.rezzie.parameter.impl.jaxb;

import java.math.RoundingMode;

import javax.xml.bind.annotation.XmlEnumValue;

public enum NumberRoundingMode
{
	@XmlEnumValue("default")
	DEFAULT(null),

	@XmlEnumValue("ceiling")
	CEILING(RoundingMode.CEILING),
	
	@XmlEnumValue("down")
	DOWN(RoundingMode.DOWN),
	
	@XmlEnumValue("floor")
	FLOOR(RoundingMode.FLOOR),
	
	@XmlEnumValue("half-down")
	HALF_DOWN(RoundingMode.HALF_DOWN),
	
	@XmlEnumValue("half-even")
	HALF_EVEN(RoundingMode.HALF_EVEN),
	
	@XmlEnumValue("half-up")
	HALF_UP(RoundingMode.HALF_UP),
	
	@XmlEnumValue("up")
	UP(RoundingMode.UP);
	
	//Intentionally do not include 'UNNECESSARY' since it wouldn't be useful when formatting resource bundles

	private final RoundingMode mode;
	
	private NumberRoundingMode(RoundingMode mode)
	{
		this.mode = mode;
	}

	public RoundingMode getMode()
	{
		return(mode);
	}
}