package au.net.causal.rezzie.parameter.impl.jaxb;

import javax.xml.bind.annotation.XmlEnumValue;

public enum NumberFormatType
{
	@XmlEnumValue("default")
	DEFAULT,
	
	@XmlEnumValue("number")
	NUMBER,

	@XmlEnumValue("integer")
	INTEGER,
	
	@XmlEnumValue("percent")
	PERCENT;
}