package au.net.causal.rezzie.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Controls how whitespace is processed when reading Rezzie XML reosurce bundles.
 * <p>
 * 
 * Values for this annotation on more specific structures (e.g. on a resource method instead of the class) will take precendence.  This handling can also
 * be overridden in the bundles themselves.
 * <p>
 * 
 * By default, the {@link WhiteSpaceMode#COLLAPSE} rule is used for bundle text and {@link WhiteSpaceMode#PRESERVE} is used for parameters 
 * if no whitespace handling rules are explicitly specified.
 * 
 * @author prunge
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Documented
public @interface WhiteSpace
{
	/**
	 * Controls how white space is processed in bundle text.
	 */
	public WhiteSpaceMode text() default WhiteSpaceMode.DEFAULT;
	
	/**
	 * Controls how white space is processed in parameters.
	 */
	public WhiteSpaceMode parameters() default WhiteSpaceMode.DEFAULT;
}
