package au.net.causal.rezzie.parameter;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;

/**
 * Provides access to metadata annotations on resource bundle parameters and their methods, bundle interfaces and packages.
 * 
 * @author prunge
 * 
 * @param <P> the parameter type.
 */
public interface ParameterMetadata<P>
{
	/**
	 * Finds an annotation on the parameter.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the annotation of the specified type that is declared on the parameter, or <code>null</code> if the parameter does not have one.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A getParameterAnnotation(Class<A> annotationType);
	
	/**
	 * Returns all annotations present on the parameter.
	 * 
	 * @return all annotations on the parameter.  Returns an empty collection if there are none.  The returned collection is unmodifiable.
	 */
	public Collection<? extends Annotation> getParameterAnnotations();
	
	/**
	 * Finds an annotation on the bundle's method the parameter is on.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the annotation of the specified type that is declared on the method of the parameter, or <code>null</code> if the method does not have one.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A getMethodAnnotation(Class<A> annotationType);
	
	/**
	 * Returns all annotations present on the parameter's method.
	 * 
	 * @return all annotations on the parameter's method.  Returns an empty collection if there are none.  The returned collection is unmodifiable.
	 */
	public Collection<? extends Annotation> getMethodAnnotations();
	
	/**
	 * Finds an annotation on the bundle interface being processed.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the annotation of the specified type that is declared on the bundle interface, or <code>null</code> if the interface does not have one.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A getBundleAnnotation(Class<A> annotationType);
	
	/**
	 * Returns all annotations present on the bundle interface being processed.
	 * 
	 * @return all annotations on the bundle interface.  Returns an empty collection if there are none.  The returned collection is unmodifiable.
	 */
	public Collection<? extends Annotation> getBundleAnnotations();
	
	/**
	 * Finds an annotation on the package of the bundle interface being processed.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the annotation of the specified type that is declared on the package, or <code>null</code> if the package does not have one.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A getPackageAnnotation(Class<A> annotationType);
	
	/**
	 * Returns all annotations present on the package of the bundle interface being processed.
	 * 
	 * @return all annotations on the package.  Returns an empty collection if there are none.  The returned collection is unmodifiable.
	 */
	public Collection<? extends Annotation> getPackageAnnotations();
	
	/**
	 * Scans the parameter, method, bundle interface and package for an annotation of the specified type, returning the first one it finds.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return the first annotation found, or <code>null</code> if no annotation of the specified type was found.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> A findFirstAnnotation(Class<A> annotationType);
	
	/**
	 * Scans the parameter, method, bundle interface and package for an annotation of the specified type, returning all annotations found.
	 * The order of the returned list is from parameter, then method, then bundle interface and then package.  i.e. from most specific to least
	 * specific.
	 * 
	 * @param annotationType the annotation type.
	 * 
	 * @return a list of all annotations found.  Returns an empty list if none were found.  The returned list is unmodifiable.
	 * 
	 * @throws NullPointerException if <code>annotationType</code> is null.
	 */
	public <A extends Annotation> List<? extends A> findAllAnnotations(Class<A> annotationType);
	
	/**
	 * Returns the actual parameter type as declared on the bundle's method.  This may be a subclass of the parameter processor's supported type.
	 *  
	 * @return the parameter type.
	 */
	public Class<? extends P> getActualParameterType();
}
