package au.net.causal.rezzie.parameter.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.jvnet.jaxb.reflection.JAXBModelFactory;
import org.jvnet.jaxb.reflection.model.core.EnumConstant;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeEnumLeafInfo;
import org.jvnet.jaxb.reflection.model.runtime.RuntimeTypeInfoSet;
import org.jvnet.jaxb.reflection.runtime.IllegalAnnotationsException;

import au.net.causal.rezzie.parameter.ParameterMetadata;
import au.net.causal.rezzie.parameter.ZeroConfigurationChoiceParameterProcessor;
import au.net.causal.rezzie.parameter.impl.jaxb.EnumChoiceType;
import au.net.causal.rezzie.service.annotation.ServiceImplementation;

@ServiceImplementation
public class EnumParameterProcessor extends ZeroConfigurationChoiceParameterProcessor<Enum, EnumChoiceType>
{
	public EnumParameterProcessor()
	{
		super(Enum.class, EnumChoiceType.class);
	}
	
	private <E extends Enum<E>> Map<String, E> readXmlEnumValues(Class<E> enumType)
	{
		try
		{
			Map<String, E> map = new HashMap<>();
			
			RuntimeTypeInfoSet infoSet = JAXBModelFactory.create(enumType);
			RuntimeEnumLeafInfo enumInfo = infoSet.enums().get(enumType);
			for (EnumConstant<?, ?> c : enumInfo.getConstants())
			{
				String enumName = c.getName();
				String xmlValue = c.getLexicalValue();
				E enumValue = Enum.valueOf(enumType, enumName);
				map.put(xmlValue, enumValue);
			}
			
			return(map);
		}
		catch (IllegalAnnotationsException e)
		{
			//TODO a better exception
			throw new RuntimeException("Invalid JAXB annotations on " + enumType.getCanonicalName() + ": " + e, e);
		}
	}
	
	@Override
	public boolean applyChoice(Enum parameter, EnumChoiceType choiceConfiguration, ParameterMetadata<Enum> parameterMetadata)
	{
		//For the enum type being processed, look up the JAXB info
		//TODO really need to cache this
		Class<? extends Enum> enumType = parameterMetadata.getActualParameterType();
		Map<String, ? extends Enum<?>> valueMap = readXmlEnumValues(enumType);
		
		String item = choiceConfiguration.getEquals();
		Enum expectedValue = valueMap.get(item);
		return(parameter == expectedValue);
	}
	
	@Override
	protected String parameterToString(Enum parameter, Locale locale)
	{
		return(parameter.toString());
	}
}
