package au.net.causal.rezzie.parameter;

public interface ChoiceSupport<P, C>
{
	public Class<P> getParameterType();
	public Class<C> getChoiceConfigurationType();
	public boolean applyChoice(P parameter, C choiceConfiguration, ParameterMetadata<P> parameterMetadata);
}
