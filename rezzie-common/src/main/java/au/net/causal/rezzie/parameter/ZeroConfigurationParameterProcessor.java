package au.net.causal.rezzie.parameter;

import java.util.Locale;

import au.net.causal.rezzie.parameter.impl.jaxb.EmptyType;

public abstract class ZeroConfigurationParameterProcessor<P> extends ParameterProcessor<P, EmptyType>
{
	protected ZeroConfigurationParameterProcessor(Class<P> parameterType)
	{
		super(parameterType, EmptyType.class);
	}
	
	@Override
	public final String parameterToString(P parameter, EmptyType configuration, ParameterMetadata<P> metadata, Locale locale)
	{
		return(parameterToString(parameter, locale));
	}
	
	protected abstract String parameterToString(P parameter, Locale locale);
}
