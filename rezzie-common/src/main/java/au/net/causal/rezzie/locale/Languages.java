package au.net.causal.rezzie.locale;

public final class Languages
{
	/**
	 * Private constructor to prevent instantiation.
	 */
	private Languages()
	{
	}
	
	public static final String ENGLISH = "en";
    public static final String FRENCH = "fr";
    public static final String GERMAN = "de";
    public static final String ITALIAN = "it";
    public static final String JAPANESE = "ja";
    public static final String KOREAN = "ko";
    public static final String CHINESE = "zh";
}
