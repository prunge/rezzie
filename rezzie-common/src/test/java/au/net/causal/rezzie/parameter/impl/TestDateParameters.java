package au.net.causal.rezzie.parameter.impl;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.junit.Test;

import au.net.causal.rezzie.parameter.impl.jaxb.DateConfigurationType;

public class TestDateParameters
{
	/**
	 * Really only tests that JAXB doesn't throw an exception when generating the schema, which can happen if the annotations are used incorrectly.
	 * 
	 * @throws Exception if an error occurs. 
	 */
	@Test
	public void testSchemaGeneration()
	throws Exception
	{
		JAXBContext context = JAXBContext.newInstance(DateConfigurationType.class);
		context.generateSchema(new SchemaOutputResolver()
		{
			@Override
			public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
			{
				StreamResult result = new StreamResult(System.out);
				result.setSystemId(new File(suggestedFileName));
				return(result);
			}
		});
	}

}
