package au.net.causal.rezzie.annotation;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit tests for whitespace handling.
 * 
 * @author prunge
 */
public class WhiteSpaceModeTest
{
	@Test
	public void testPreserve()
	{
		String s = WhiteSpaceMode.PRESERVE.processString("\t\tHello, how are you?  I am good.", true, true);
		assertEquals("Wrong result.", "\t\tHello, how are you?  I am good.", s);
	}
	
	@Test
	public void testReplace()
	{
		String s = WhiteSpaceMode.REPLACE.processString("\t\tHello, how are you?  I am good.", true, true);
		assertEquals("Wrong result.", "  Hello, how are you?  I am good.", s);
	}

	@Test
	public void testCollapse()
	{
		String s = WhiteSpaceMode.COLLAPSE.processString("\t\tHello, how are you?  I am good.", true, true);
		assertEquals("Wrong result.", "Hello, how are you? I am good.", s);
	}
	
	@Test
	public void testCollapseNotEnd()
	{
		String s = WhiteSpaceMode.COLLAPSE.processString("\t\tHello\n", true, false);
		assertEquals("Wrong result.", "Hello ", s);
	}
	
	@Test
	public void testCollapseNotStart()
	{
		String s = WhiteSpaceMode.COLLAPSE.processString("\t\thow are you?\n", false, true);
		assertEquals("Wrong result.", " how are you?", s);
	}
}
