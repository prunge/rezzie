package au.net.causal.rezzie.locator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestRezzieResourceLocator
{
	@Test
	public void testRelativizeBaseDirectory()
	{
		RezzieResourceLocator loc = new RezzieResourceLocator();
		String rel = loc.relativizeFromFile("META-INF/aaa/bbb/ccc.txt", "META-INF/aaa/galah.txt");
		assertEquals("Wrong result.", "bbb/ccc.txt", rel);
	}

	@Test
	public void testRelativizeSameFile()
	{
		RezzieResourceLocator loc = new RezzieResourceLocator();
		String rel = loc.relativizeFromFile("META-INF/aaa/bbb/ccc.txt", "META-INF/aaa/bbb/ccc.txt");
		assertEquals("Wrong result.", "ccc.txt", rel);
	}
	
	@Test
	public void testRelativizeNoCommon()
	{
		RezzieResourceLocator loc = new RezzieResourceLocator();
		String rel = loc.relativizeFromFile("META-INF/aaa/bbb/ccc.txt", "WEB-INF/web.xml");
		assertEquals("Wrong result.", "../META-INF/aaa/bbb/ccc.txt", rel);
	}
	
	@Test
	public void testRelativizeTwoFilesInSameDir()
	{
		RezzieResourceLocator loc = new RezzieResourceLocator();
		String rel = loc.relativizeFromFile("META-INF/aaa/bbb/ccc.txt", "META-INF/aaa/bbb/ddd.txt");
		assertEquals("Wrong result.", "ccc.txt", rel);
	}
	
	@Test
	public void testRelativizeToParent()
	{
		RezzieResourceLocator loc = new RezzieResourceLocator();
		String rel = loc.relativizeFromFile("META-INF/aaa/bbb/ccc.txt", "META-INF/aaa/ddd/eee.txt");
		assertEquals("Wrong result.", "../bbb/ccc.txt", rel);
	}
}
