package au.net.causal.rezzie.jsf.apt;

import java.beans.Introspector;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.jsf.annotation.JsfConfiguration;
import au.net.causal.rezzie.jsf.annotation.JsfMasterBean;
import au.net.causal.rezzie.jsf.apt.MasterBeanConfiguration.BeanEntry;

@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes("au.net.causal.rezzie.annotation.ResourceBundleTemplate")
@SupportedOptions(RezzieJsfBeanProcessor.OPTION_DEBUG)
public class RezzieJsfBeanProcessor extends AbstractProcessor
{
	public static final String OPTION_DEBUG = "au.net.causal.rezzie.jsf.apt.RezzieJsfBeanProcessor.debug";
	
	private boolean debugMode;
	
	@Override
	public void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);
		debugMode = Boolean.parseBoolean(processingEnv.getOptions().get(OPTION_DEBUG));
	}
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		Map<String, MasterBeanConfiguration> masterBeanMap = new LinkedHashMap<>();
		for (Element element : roundEnv.getElementsAnnotatedWith(ResourceBundleTemplate.class))
		{
			if (element.getKind() == ElementKind.INTERFACE && element instanceof TypeElement) 
				generateManagedBean((TypeElement)element, masterBeanMap);
		}
		
		//Now generate the master beans
		for (Map.Entry<String, MasterBeanConfiguration> entry : masterBeanMap.entrySet())
		{
			generateMasterBean(entry.getKey(), entry.getValue());
		}
		
		return(false);
	}
	
	private void generateMasterBean(String masterBeanClassName, MasterBeanConfiguration masterBeanConfiguration)
	{
		String masterBeanPackageName;
		String masterBeanSimpleName;
		int lastDotIndex = masterBeanClassName.lastIndexOf('.');
		if (lastDotIndex < 0)
		{
			masterBeanPackageName = "";
			masterBeanSimpleName = masterBeanClassName;
		}
		else
		{
			masterBeanPackageName = masterBeanClassName.substring(0, lastDotIndex);
			masterBeanSimpleName = masterBeanClassName.substring(lastDotIndex + 1);
		}
		
		try
		{
			JavaFileObject sourceFile = processingEnv.getFiler().createSourceFile(masterBeanClassName);
			try (Writer w = sourceFile.openWriter())
			{
				CodeWriter cw = new CodeWriter(w);
				writeMasterBean(masterBeanPackageName, masterBeanSimpleName, masterBeanConfiguration, cw);
				if (cw.getWriterException() != null)
					throw cw.getWriterException();
			}
		}
		catch (IOException e)
		{
			for (TypeElement originatingElement : masterBeanConfiguration.getOriginatingElements())
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error writing master bean source file " + masterBeanClassName + ": " + e, originatingElement);
			}
		}
	}
	
	private void writeMasterBean(String packageName, String simpleName, MasterBeanConfiguration config, CodeWriter w)
	throws IOException
	{
		debug("Writing master bean " + packageName + "." + simpleName);
		
		if (packageName != null && !packageName.isEmpty())
		{
			w.write("package ");
			w.write(packageName);
			w.write(";");
			w.writeNewLine();
			w.writeNewLine();
		}
		
		//Annotations
		w.write("@javax.faces.bean.ManagedBean");
		
		//Master bean name
		try
		{
			String masterBeanName = config.getMasterBeanName();
			if (masterBeanName != null)
			{
				w.write("(name=");
				w.write(processingEnv.getElementUtils().getConstantExpression(masterBeanName));
				w.write(")");
			}
		}
		catch (MasterBeanNameConflictException e)
		{
			//This happens when multiple beans define conflicting names
			for (TypeElement originatingElement : e.getOriginatingElements())
			{
				AnnotationMirror am = findAnnotationMirror(originatingElement, JsfMasterBean.class);
				if (am != null)
					processingEnv.getMessager().printMessage(Kind.ERROR, "Conflicting master bean names " + e.getNames() + ".  Ensure master bean naming is consistent.", originatingElement, am);
				else
					processingEnv.getMessager().printMessage(Kind.ERROR, "Conflicting master bean names " + e.getNames() + ".  Ensure master bean naming is consistent.", originatingElement);
			}
		}
	
		w.writeNewLine();
		w.write("@javax.faces.bean.RequestScoped");
		w.writeNewLine();
		
		w.write("public class ");
		w.write(simpleName);
		w.writeNewLine();
		w.write("{");
		w.writeNewLine();
		w.indent();
		
		//Fields
		for (BeanEntry entry : config.getEntries())
		{
			w.write("private final ");
			w.write(entry.getClassName());
			w.write(" ");
			w.write(entry.getBeanName());
			w.write(" = new ");
			w.write(entry.getClassName());
			w.write("();");
			w.writeNewLine();
		}
		w.writeNewLine();
		
		//Getters
		for (BeanEntry entry : config.getEntries())
		{
			w.write("public ");
			w.write(entry.getClassName());
			w.write(" ");
			w.write("get");
			w.write(capitalize(entry.getBeanName()));
			w.write("()");
			w.writeNewLine();
			w.write("{");
			w.writeNewLine();
			w.indent();
			
			w.write("return(");
			w.write(entry.getBeanName());
			w.write(");");
			w.writeNewLine();
			
			w.unindent();
			w.write("}");
			w.writeNewLine();
			w.writeNewLine();
		}
		
		w.unindent();
		w.write("}");
		w.writeNewLine();
	}
	
	private AnnotationMirror findAnnotationMirror(Element element, Class<? extends Annotation> annotationType)
	{
		for (AnnotationMirror am : element.getAnnotationMirrors())
		{
			if (am.getAnnotationType() != null)
			{
				if (annotationType.getCanonicalName().equals(am.getAnnotationType().toString()))
						return(am);
			}
		}
		
		return(null);
	}
	
	private void generateManagedBean(TypeElement bundleInterfaceElement, Map<String, MasterBeanConfiguration> masterBeanMap)
	{
		PackageElement pkg = processingEnv.getElementUtils().getPackageOf(bundleInterfaceElement);
		String className = bundleInterfaceElement.getSimpleName().toString();
		String packageName;
		if (pkg == null)
			packageName = "jsf";
		else
			packageName = pkg.getQualifiedName().toString() + ".jsf";
		
		//Master bean?
		JsfMasterBean mb = bundleInterfaceElement.getAnnotation(JsfMasterBean.class);
		if (mb == null && pkg != null)
			mb = pkg.getAnnotation(JsfMasterBean.class);
		
		MasterBeanConfiguration mbc;
		String masterBeanName;
		if (mb != null)
		{	
			String masterBeanClassName = mb.className();
			if (masterBeanClassName == null || masterBeanClassName.isEmpty())
				masterBeanClassName = defaultJsfMasterBeanClassName(packageName, className);
				
			mbc = masterBeanMap.get(masterBeanClassName);
			if (mbc == null)
			{
				mbc = new MasterBeanConfiguration(masterBeanClassName);
				masterBeanMap.put(masterBeanClassName, mbc);
			}
			masterBeanName = mb.beanName();
			if (masterBeanName != null && masterBeanName.isEmpty())
				masterBeanName = null;
		}
		else
		{
			mbc = null;
			masterBeanName = null;
		}
		
		try
		{
			JavaFileObject sourceFile = processingEnv.getFiler().createSourceFile(packageName + "." + className, bundleInterfaceElement);
			try (Writer w = sourceFile.openWriter())
			{
				CodeWriter cw = new CodeWriter(w);
				writeManagedBean(bundleInterfaceElement, packageName, className, mbc, masterBeanName, cw);
				if (cw.getWriterException() != null)
					throw cw.getWriterException();
			}
		}
		catch (IOException e)
		{
			processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error writing source file " + packageName + "." + className + ": " + e, bundleInterfaceElement);
		}
	}
	
	private void writeManagedBean(TypeElement bundleInterfaceElement, String managedBeanPackage, String managedBeanSimpleName, 
					MasterBeanConfiguration masterBeanConfiguration, String masterBeanName, CodeWriter w)
	{
		debug("Writing managed bean " + managedBeanPackage + "." + managedBeanSimpleName);
		
		if (managedBeanPackage != null && !managedBeanPackage.isEmpty())
		{
			w.write("package ");
			w.write(managedBeanPackage);
			w.write(";");
			w.writeNewLine();
			w.writeNewLine();
		}
		
		//Annotations
		JsfConfiguration config = bundleInterfaceElement.getAnnotation(JsfConfiguration.class);
		
		if (masterBeanConfiguration == null)
		{
			w.write("@javax.faces.bean.ManagedBean");
			
			//Look up the JSF configuration
			if (config != null && config.beanName() != null && !config.beanName().isEmpty())
			{
				w.write("(name=");
				w.write(processingEnv.getElementUtils().getConstantExpression(config.beanName()));
				w.write(")");
			}
		
			w.writeNewLine();
			w.write("@javax.faces.bean.RequestScoped");
			w.writeNewLine();
		}
		else
		{
			//Will not be a managed bean but need to register in the master bean configuration
			String beanName;
			if (config == null || config.beanName() == null || config.beanName().isEmpty())
				beanName = defaultJsfBeanName(managedBeanSimpleName);
			else
				beanName = config.beanName();
			
			
			
			masterBeanConfiguration.putBean(beanName, managedBeanPackage + "." + managedBeanSimpleName, bundleInterfaceElement, masterBeanName);
		}
				
		w.write("public class ");
		w.write(managedBeanSimpleName);
		w.write(" extends au.net.causal.rezzie.jsf.AbstractRezzieBean");
		w.writeNewLine();
		w.write("{");
		w.writeNewLine();
		w.indent();
		
		//Bundle variable
		w.write("private final ");
		w.write(bundleInterfaceElement.getQualifiedName().toString());
		w.write(" bundle;");
		w.writeNewLine();
		w.writeNewLine();
		
		//Constructor
		w.write("public ");
		w.write(managedBeanSimpleName);
		w.write("()");
		w.writeNewLine();
		w.write("{");
		w.writeNewLine();
		w.indent();
		
		w.write("bundle = getBundle(");
		w.write(bundleInterfaceElement.getQualifiedName().toString());
		w.write(".class);");
		w.writeNewLine();
		
		w.unindent();
		w.write("}");
		w.writeNewLine();
		w.writeNewLine();
		
		for (Element element : bundleInterfaceElement.getEnclosedElements())
		{
			if (element.getKind() == ElementKind.METHOD && element instanceof ExecutableElement)
			{
				ExecutableElement method = (ExecutableElement)element;
				writeManagedBeanMethod(method, bundleInterfaceElement, w);
				w.writeNewLine();
			}
		}
		
		w.unindent();
		w.write("}");
		w.writeNewLine();
	}
	
	private String defaultJsfBeanName(String simpleClassName)
	{
		return(Introspector.decapitalize(simpleClassName));
	}
	
	private String defaultJsfMasterBeanClassName(String packageName, String simpleName)
	{
		return(packageName + ".RezzieMessages");
	}
	
	private void writeManagedBeanMethod(ExecutableElement method, TypeElement bundleInterfaceElement, CodeWriter w)
	{
		//If the method has no parameters, generate a getter instead to make it easy to use in EL expressions
		String generatedMethodName;
		if (method.getParameters().isEmpty())
			generatedMethodName = "get" + capitalize(method.getSimpleName().toString());
		else
			generatedMethodName = method.getSimpleName().toString();
		
		w.write("public ");
		w.write(method.getReturnType().toString());
		w.write(" ");
		w.write(generatedMethodName);
		w.write("(");
		for (Iterator<? extends VariableElement> i = method.getParameters().iterator(); i.hasNext();)
		{
			VariableElement parameter = i.next();
			w.write(parameter.asType().toString());
			w.write(" ");
			w.write(parameter.getSimpleName().toString());
			if (i.hasNext())
				w.write(", ");
		}
		w.write(")");
		w.writeNewLine();
		w.write("{");
		w.writeNewLine();
		w.indent();
		
		w.write("return(bundle.");
		w.write(method.getSimpleName().toString());
		w.write("(");
		for (Iterator<? extends VariableElement> i = method.getParameters().iterator(); i.hasNext();)
		{
			VariableElement parameter = i.next();
			w.write(parameter.getSimpleName().toString());
			if (i.hasNext())
				w.write(", ");
		}
		w.write("));");
		w.writeNewLine();
		
		w.unindent();
		w.write("}");
		w.writeNewLine();
	}
	
	private static String capitalize(String s)
	{
		if (s.isEmpty())
			return(s);
		
		return(s.substring(0, 1).toUpperCase(Locale.ENGLISH) + s.substring(1));
	}

	private void debug(String message)
	{
		if (debugMode)
			processingEnv.getMessager().printMessage(Kind.NOTE, message);
	}
}
