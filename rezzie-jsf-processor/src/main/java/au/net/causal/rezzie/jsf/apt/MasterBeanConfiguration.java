package au.net.causal.rezzie.jsf.apt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.TypeElement;

public class MasterBeanConfiguration
{
	private final String className;
	
	private final Map<String, BeanEntry> beanNameToClassNameMap = new LinkedHashMap<>();
	private final List<TypeElement> originatingElements = new ArrayList<>();
	
	/**
	 * Maps master bean candidate names to the list of type elements that defined the name.
	 */
	private final Map<String, List<TypeElement>> masterBeanNameCandidateMap = new HashMap<>();
	
	public MasterBeanConfiguration(String className)
	{
		if (className == null)
			throw new NullPointerException("className == null");
		if (className.isEmpty())
			throw new IllegalArgumentException("className cannot be empty.");
		
		this.className = className;
	}
	
	public String getClassName()
	{
		return(className);
	}
	
	public void putBean(String beanName, String className, TypeElement originatingElement, String masterBeanName)
	{
		beanNameToClassNameMap.put(beanName, new BeanEntry(beanName, className));
		originatingElements.add(originatingElement);
		
		if (masterBeanName != null && !masterBeanName.isEmpty())
		{
			List<TypeElement> nameDefiningElements = masterBeanNameCandidateMap.get(masterBeanName);
			if (nameDefiningElements == null)
			{
				nameDefiningElements = new ArrayList<>();
				masterBeanNameCandidateMap.put(masterBeanName, nameDefiningElements);
			}
			nameDefiningElements.add(originatingElement);
		}
	}
	
	public String getMasterBeanName()
	throws MasterBeanNameConflictException
	{
		if (masterBeanNameCandidateMap.isEmpty())
			return(null);
		else if (masterBeanNameCandidateMap.size() == 1)
			return(masterBeanNameCandidateMap.entrySet().iterator().next().getKey());
		else
		{
			List<TypeElement> allElements = new ArrayList<>();
			for (List<TypeElement> curList : masterBeanNameCandidateMap.values())
			{
				allElements.addAll(curList);
			}
			throw new MasterBeanNameConflictException(masterBeanNameCandidateMap.keySet(), allElements);
		}
	}
	
	public Collection<? extends BeanEntry> getEntries()
	{
		return(Collections.unmodifiableCollection(beanNameToClassNameMap.values()));
	}
	
	public Collection<? extends TypeElement> getOriginatingElements()
	{
		return(Collections.unmodifiableList(originatingElements));
	}
	
	public static class BeanEntry
	{
		private final String beanName;
		private final String className;
		
		public BeanEntry(String beanName, String className)
		{
			this.beanName = beanName;
			this.className = className;
		}
		
		public String getBeanName()
		{
			return(beanName);
		}
		
		public String getClassName()
		{
			return(className);
		}
	}
}
