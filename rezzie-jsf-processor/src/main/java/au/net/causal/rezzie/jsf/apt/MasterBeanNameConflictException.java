package au.net.causal.rezzie.jsf.apt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.lang.model.element.TypeElement;

public class MasterBeanNameConflictException extends Exception
{
	private final Set<String> names;
	private final Collection<? extends TypeElement> originatingElements;
	
	public MasterBeanNameConflictException(Set<String> names, Collection<? extends TypeElement> originatingElements)
	{
		this.names = Collections.unmodifiableSet(new LinkedHashSet<>(names));
		this.originatingElements = Collections.unmodifiableList(new ArrayList<>(originatingElements));
	}

	public Set<String> getNames()
	{
		return(names);
	}

	public Collection<? extends TypeElement> getOriginatingElements()
	{
		return(originatingElements);
	}
}
