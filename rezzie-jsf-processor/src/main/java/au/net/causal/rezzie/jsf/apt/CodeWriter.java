package au.net.causal.rezzie.jsf.apt;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer to easily write source code.  Includes methods for managing indentation, counting line numbers and handling
 * new lines.
 * <p>
 * 
 * Exceptions are not thrown from any of the write methods for convenience.  After writing, the {@link #getWriterException()}
 * method should be called to check if any exception occurred when writing to the underlying writer.
 * 
 * @author prunge
 */
public class CodeWriter
{
	private final Writer writer;
	private IOException writerException;
	
	private String newLine = System.lineSeparator();
	private boolean onNewLine = true;
	
	private int indentLevel;
	
	private int lineCounter = 1;
	private int columnCounter = 1;
	
	/**
	 * Creates a <code>CodeWriter</code>.
	 * 
	 * @param writer the underlying writer to write to.
	 * 
	 * @throws NullPointerException if <code>writer</code> is null.
	 */
	public CodeWriter(Writer writer)
	{
		if (writer == null)
			throw new NullPointerException("writer == null");
		
		this.writer = writer;
	}
	
	/**
	 * Indents subsequently written lines of code.
	 * <p>
	 * 
	 * Multiple indentation is supported.
	 * 
	 * @see #unindent()
	 */
	public void indent()
	{
		indentLevel++;
	}
	
	/**
	 * Undoes one indentation level.
	 * 
	 * @see #indent()
	 */
	public void unindent()
	{
		indentLevel--;
	}
	
	/**
	 * Writes characters.
	 * 
	 * @param s the characters to write.
	 */
	public void write(char[] s)
	{
		write(new String(s));
	}
	
	/**
	 * Writes a string.
	 * 
	 * @param s the string to write.
	 */
	public void write(String s)
	{
		if (writerException != null)
			return;
		
		try
		{
			if (onNewLine)
			{
				for (int i = 0; i < indentLevel; i++)
				{
					writer.write("\t");
					columnCounter++;
				}
			}
			
			writer.write(s);
			columnCounter += s.length();
		}
		catch (IOException e)
		{
			writerException = e;
		}
		
		onNewLine = false;
	}
	
	/**
	 * Writes a new line.
	 */
	public void writeNewLine()
	{
		write(newLine);
		onNewLine = true;
		lineCounter++;
		columnCounter = 1;
	}
	
	/**
	 * Returns any I/O exception that occurred while writing to the underlying writer.
	 * 
	 * @return an exception, or <code>null</code> if there was none.
	 */
	public IOException getWriterException()
	{
		return(writerException);
	}
	
	/**
	 * Returns the current line number being written to.  This value is one-based, which means the first line in the 
	 * file is line 1, the second is line 2, etc.
	 * 
	 * @return the current line number.
	 */
	public int getCurrentLineNumber()
	{
		return(lineCounter);
	}
	
	/**
	 * Returns the current column number being written to.  This value is one-based, which means the first column
	 * is 1, the second is 2, etc.
	 * 
	 * @return the current column number.
	 */
	public int getCurrentColumnNumber()
	{
		return(columnCounter);
	}
}
