package au.net.causal.rezzie.service.processor;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import au.net.causal.rezzie.service.annotation.ServiceSpecification;

@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes("au.net.causal.rezzie.service.annotation.ServiceSpecification")
public class ServiceSpecificationProcessor extends AbstractProcessor
{
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		for (Element element : roundEnv.getElementsAnnotatedWith(ServiceSpecification.class))
		{
			if (element.getKind() == ElementKind.CLASS || element.getKind() == ElementKind.INTERFACE)
			{
				TypeElement type = (TypeElement)element;
				validateType(type);
			}
		}
		
		return(false);
	}
	
	private void validateType(TypeElement type)
	{
		//Should be abstract or interface
		if (type.getKind() == ElementKind.CLASS && !type.getModifiers().contains(Modifier.ABSTRACT))
			processingEnv.getMessager().printMessage(Kind.WARNING, "Service specifications should be abstract.", type);
		
		//Must be public
		if (!type.getModifiers().contains(Modifier.PUBLIC))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Service specifications must be public.", type);
		
		//If nested, must be static
		if (type.getNestingKind().isNested() && !type.getModifiers().contains(Modifier.STATIC))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Nested class service specifications must be static.", type);
	
		//Should not be final
		if (type.getModifiers().contains(Modifier.FINAL))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Service specifications must not be final.", type);
	}
}
