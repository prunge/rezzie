package au.net.causal.rezzie.service.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import au.net.causal.rezzie.service.annotation.ServiceImplementation;
import au.net.causal.rezzie.service.annotation.ServiceSpecification;

@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedAnnotationTypes("au.net.causal.rezzie.service.annotation.ServiceImplementation")
@SupportedOptions(ServiceImplementationProcessor.OPTION_DEBUG)
public class ServiceImplementationProcessor extends AbstractProcessor
{
	public static final String OPTION_DEBUG = "au.net.causal.rezzie.service.processor.ServiceImplementationProcessor.debug";
	
	private boolean debugMode;
	
	@Override
	public void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);
		debugMode = Boolean.parseBoolean(processingEnv.getOptions().get(OPTION_DEBUG));
	}
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
		Map<TypeElement, Set<TypeElement>> serviceImplementationMap = new LinkedHashMap<>();
		
		for (Element element : roundEnv.getElementsAnnotatedWith(ServiceImplementation.class))
		{
			if (element.getKind() != ElementKind.CLASS)
				processingEnv.getMessager().printMessage(Kind.ERROR, "Service implementations must be non-abstract classes.", element);
			else
			{
				TypeElement type = (TypeElement)element;
				processType(type, serviceImplementationMap);
			}
		}
		
		for (Map.Entry<TypeElement, Set<TypeElement>> entry : serviceImplementationMap.entrySet())
		{
			try
			{
				writeServiceFile(entry.getKey(), entry.getValue());
			}
			catch (IOException e)
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "I/O error writing service file for " + entry.getKey().asType() + ": " + e);
			}
		}
		
		return(false);
	}
	
	private void writeServiceFile(TypeElement serviceSpec, Collection<? extends TypeElement> serviceImplementations)
	throws IOException
	{
		String fileName = "META-INF/services/" + processingEnv.getElementUtils().getBinaryName(serviceSpec);
		debug("Preparing to generate " + fileName);
		FileObject file = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", fileName);
		
		//Incrememental compilation - if there already exists a file with this name then read it and add to its contents
		//Otherwise, such as in Eclipse, if we are doing incremental compilation the service file will be incomplete
		Set<String> existingImplementations = attemptToReadServiceFile(file);
		debug("Found existing implementations: " + existingImplementations);
				
		//Service files must be written in UTF-8 regardless of the compiler encoding
		Set<String> processedNames = new HashSet<>();
		try (Writer w = new OutputStreamWriter(file.openOutputStream(), StandardCharsets.UTF_8))
		{
			for (String existingImplementation : existingImplementations)
			{
				if (!processedNames.contains(existingImplementation))
				{
					w.append(existingImplementation);
					w.append('\n');
					processedNames.add(existingImplementation);
				}
			}
			
			for (TypeElement serviceImplementation : serviceImplementations)
			{ 
				String name = processingEnv.getElementUtils().getBinaryName(serviceImplementation).toString();
				if (!processedNames.contains(name))
				{
					w.append(name);
					w.append('\n');
				}
			}
		}
	}
	
	private Set<String> attemptToReadServiceFile(FileObject file)
	throws IOException
	{
		//First try to open the file using standard API
		try
		{
			try (BufferedReader r = new BufferedReader(new InputStreamReader(file.openInputStream(), StandardCharsets.UTF_8)))
			{
				return(readServiceFile(r));
			}
		}
		catch (IllegalStateException e)
		{
			//Occurs if file object is output only
			debug("Tried to open " + file.toUri() + " using APT API but it is write-only.");
		}
		
		//Next try to use the URI directly
		try
		{
			Path path = Paths.get(file.toUri());
			if (Files.exists(path))
			{
				try (BufferedReader r = Files.newBufferedReader(path, StandardCharsets.UTF_8))
				{
					return(readServiceFile(r));
				}
			}
			else
				debug("File " + path + " does not exist so cannot read the service file.");
		}
		catch (FileSystemNotFoundException e)
		{
			//Some weird custom protocol URI, have to give up
			debug("Cannot open URI " + file.toUri());
		}
		
		//If we get here we give up
		return(Collections.emptySet());
		
	}
	
	private Set<String> readServiceFile(BufferedReader r)
	throws IOException
	{
		Set<String> items = new LinkedHashSet<>();
		
		String line;
		do
		{
			line = r.readLine();
			if (line != null)
			{
				//Trim comments
				int commentStart = line.indexOf('#');
				if (commentStart >= 0)
					line = line.substring(0, commentStart);
				
				line = line.trim();
				
				if (!line.isEmpty())
					items.add(line);
			}
		}
		while (line != null);
		
		return(items);
	}
	
	private AnnotationMirror getAnnotationMirrorOfType(TypeElement type, Class<? extends Annotation> annotationType)
	{
		List<? extends AnnotationMirror> annotations = type.getAnnotationMirrors();
		for (AnnotationMirror annotation : annotations)
		{
			DeclaredType curAnnotationType = annotation.getAnnotationType();
			Element annotationElement = curAnnotationType.asElement();
			if (annotationElement.getKind() == ElementKind.ANNOTATION_TYPE && annotationElement instanceof TypeElement)
			{
				TypeElement annotationTypeElement = (TypeElement)annotationElement;
				if (annotationType.getCanonicalName().equals(annotationTypeElement.getQualifiedName().toString()))
					return(annotation);
			}
		}
		
		return(null);
	}
	
	
	private void processType(TypeElement type, Map<TypeElement, Set<TypeElement>> serviceImplementationMap)
	{
		validateType(type);
		
		//Find service specification marked class or interface in the tree
		Set<? extends TypeElement> specs = getSpecsFromServiceImplementationAnnotation(type);
		if (specs.isEmpty())
			specs = findServiceSpecifications(type);
		
		if (specs.isEmpty())
			processingEnv.getMessager().printMessage(Kind.ERROR, "No service specification class or interface found for the service implementation.", type);
		
		debug("Service spec for " + type.asType() + ": " + specs);
		
		for (TypeElement spec : specs)
		{
			Set<TypeElement> impls = serviceImplementationMap.get(spec);
			if (impls == null)
			{
				impls = new LinkedHashSet<>();
				serviceImplementationMap.put(spec, impls);
			}
			impls.add(type);
		}
	}
	
	private AnnotationValue getAnnotationValue(AnnotationMirror annotation, String attributeName)
	{
		for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : processingEnv.getElementUtils().getElementValuesWithDefaults(annotation).entrySet())
		{
			if (attributeName.equals(entry.getKey().getSimpleName().toString()))
				return(entry.getValue());
		}
		return(null);
	}
	
	private Set<? extends TypeElement> getSpecsFromServiceImplementationAnnotation(TypeElement type)
	{
		AnnotationMirror siMirror = getAnnotationMirrorOfType(type, ServiceImplementation.class);
		AnnotationValue av = getAnnotationValue(siMirror, "value");
		List<DeclaredType> avTypes = new ArrayList<>();
		if (av != null && av.getValue() != null)
		{
			if (av.getValue() instanceof List)
			{
				for (Object avElement : (List<?>)av.getValue())
				{
					if (avElement instanceof AnnotationValue)
						avElement = ((AnnotationValue)avElement).getValue();
					if (avElement instanceof TypeMirror)
					{
						TypeMirror tm = (TypeMirror)avElement;
						if (tm.getKind() == TypeKind.DECLARED && tm instanceof DeclaredType)
							avTypes.add((DeclaredType)tm);
					}
				}
			}
			else if (av.getValue() instanceof TypeMirror)
			{
				TypeMirror tm = (TypeMirror)av.getValue();
				if (tm.getKind() == TypeKind.DECLARED && tm instanceof DeclaredType)
					avTypes.add((DeclaredType)tm);
			}
		}
		if (avTypes.isEmpty())
			return(Collections.emptySet());
		else
		{
			LinkedHashSet<TypeElement> avSpecs = new LinkedHashSet<>();
			for (DeclaredType avType : avTypes)
			{
				Element avElement = avType.asElement();
				if ((avElement.getKind() == ElementKind.CLASS || avElement.getKind() == ElementKind.INTERFACE) && 
					avElement instanceof TypeElement)
				{
					avSpecs.add((TypeElement)avElement);
				}
			}
			return(avSpecs);
		}
	}
	
	private void debug(String message)
	{
		if (debugMode)
			processingEnv.getMessager().printMessage(Kind.NOTE, message);
	}
	
	/**
	 * Find all supertypes and superinterfaces that are marked with the {@link ServiceSpecification} annotation.
	 *  
	 * @param implementationType the implementation type to process.
	 * 
	 * @return the specification types.
	 */
	private Set<? extends TypeElement> findServiceSpecifications(TypeElement implementationType)
	{
		Set<TypeElement> specs = new LinkedHashSet<>();
		Set<TypeElement> processed = new HashSet<>();
		
		findServiceSpecifications(implementationType, processed, specs);
		
		return(specs);
	}
	
	private void findServiceSpecifications(TypeElement typeToProcess, Set<? super TypeElement> processedTypes, Set<? super TypeElement> serviceSpecTypes)
	{
		boolean isActuallyNew = processedTypes.add(typeToProcess);
		if (!isActuallyNew)
			return;
		
		if (typeToProcess.getAnnotation(ServiceSpecification.class) != null)
			serviceSpecTypes.add(typeToProcess);
		
		//Process interfaces
		for (TypeMirror interfaceType : typeToProcess.getInterfaces())
		{
			if (interfaceType.getKind() == TypeKind.DECLARED && interfaceType instanceof DeclaredType)
			{
				DeclaredType dInterfaceType = (DeclaredType)interfaceType;
				Element element = dInterfaceType.asElement();
				if (element.getKind() == ElementKind.INTERFACE && element instanceof TypeElement)
				{
					TypeElement interfaceElement = (TypeElement)element;
					findServiceSpecifications(interfaceElement, processedTypes, serviceSpecTypes);
				}
			}
		}
		
		//Process superclass
		TypeMirror superType = typeToProcess.getSuperclass();
		if (superType.getKind() == TypeKind.DECLARED && superType instanceof DeclaredType)
		{
			DeclaredType dSuperType = (DeclaredType)superType;
			Element element = dSuperType.asElement();
			if (element.getKind() == ElementKind.CLASS && element instanceof TypeElement)
			{
				TypeElement superTypeElement = (TypeElement)element;
				findServiceSpecifications(superTypeElement, processedTypes, serviceSpecTypes);
			}
		}
	}
	
	private void validateType(TypeElement type)
	{
		//Must not be abstract
		if (type.getModifiers().contains(Modifier.ABSTRACT))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Service implementations must be not be abstract.", type);
		
		//Must be public
		if (!type.getModifiers().contains(Modifier.PUBLIC))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Service implementations must be public.", type);
		
		//If nested, must be static
		if (type.getNestingKind().isNested() && !type.getModifiers().contains(Modifier.STATIC))
			processingEnv.getMessager().printMessage(Kind.ERROR, "Nested class service implementations must be static.", type);
	
		final Set<ExecutableElement> invalidConstructors = new HashSet<>();
		final Set<ExecutableElement> validConstructors = new HashSet<>();
		
		for (Element element : type.getEnclosedElements())
		{
			if (element.getKind() == ElementKind.CONSTRUCTOR && element instanceof ExecutableElement)
			{
				ExecutableElement constructor = (ExecutableElement)element;
				if (constructor.getModifiers().contains(Modifier.PUBLIC) && constructor.getParameters().isEmpty())
					validConstructors.add(constructor);
				else
					invalidConstructors.add(constructor);
			}
		}
		
		//Normally I'd do this but due to some strange reason (compiler bugs?) it doesn't always work
		/*
		type.accept(new ElementKindVisitor7<Void, Void>()
		{
			@Override
			public Void visitExecutableAsConstructor(ExecutableElement e, Void p)
			{
				if (e.getModifiers().contains(Modifier.PUBLIC) && e.getParameters().isEmpty())
					validConstructors.add(e);
				else
					invalidConstructors.add(e);
				
				return super.visitExecutableAsConstructor(e, p);
			}
		}, null);
		*/
		
		//Public zero-arg constructor happens by default
		if (!invalidConstructors.isEmpty() && validConstructors.isEmpty())
		{
			for (ExecutableElement constructor : invalidConstructors)
			{
				processingEnv.getMessager().printMessage(Kind.ERROR, "Service implementations must have a public zero-argument constructor.", constructor);
			}
		}
	}
}
