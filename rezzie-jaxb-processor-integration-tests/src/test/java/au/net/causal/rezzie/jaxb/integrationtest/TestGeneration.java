package au.net.causal.rezzie.jaxb.integrationtest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.junit.Test;

import au.net.causal.rezzie.jaxb.integrationtest.pkg.MyClass;

/**
 * Tests that JAXB works with the generated jaxb.index file.  If no jaxb.index file is generated the context instantiation from the package will fail
 * with an error.
 * 
 * @author prunge
 */
public class TestGeneration
{
	@Test
	public void test()
	throws Exception
	{
		final StringWriter buf = new StringWriter();
		
		JAXBContext context = JAXBContext.newInstance(MyClass.class.getPackage().getName());
		context.generateSchema(new SchemaOutputResolver()
		{
			@Override
			public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
			{
				StreamResult result = new StreamResult(buf);
				result.setSystemId(suggestedFileName);
				return(result);
			}
		});
		
		System.out.println(buf);
		
		//Really basic tests just to make sure the classes were picked up
		assertTrue("Missing type.", buf.toString().contains("complexType name=\"myXmlType\""));
		assertTrue("Missing type.", buf.toString().contains("complexType name=\"myClass\""));
		assertTrue("Missing type.", buf.toString().contains("complexType name=\"myClass3\""));
	}

}
