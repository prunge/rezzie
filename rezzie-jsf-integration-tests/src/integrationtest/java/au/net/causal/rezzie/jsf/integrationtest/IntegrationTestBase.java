package au.net.causal.rezzie.jsf.integrationtest;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.ws.commons.util.NamespaceContextImpl;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class IntegrationTestBase
{
	private static final Logger log = LoggerFactory.getLogger(IntegrationTestBase.class);
	
	private static URL serverUrl;
	private static DocumentBuilderFactory documentBuilderFactory;
	private static TransformerFactory transformerFactory;
	private static XPath xPath;
	
	@BeforeClass
	public static void setUpUrl()
	throws MalformedURLException
	{
		serverUrl = new URL(System.getProperty("tomcat.integrationtest.baseUrl", "http://localhost:8080/rezzie-jsf-integration-tests/"));
		log.info("Server URL: " + serverUrl);
	}
	
	@BeforeClass
	public static void setUpXml()
	{
		documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		
		XPathFactory xPathFactory = XPathFactory.newInstance();
		xPath = xPathFactory.newXPath();
		NamespaceContextImpl nci = new NamespaceContextImpl();
		nci.startPrefixMapping("h", "http://www.w3.org/1999/xhtml");
		nci.startPrefixMapping("", "http://www.w3.org/1999/xhtml");
		xPath.setNamespaceContext(nci);
		
		transformerFactory = TransformerFactory.newInstance();
	}
	
	protected URL getServerUrl()
	{
		return(serverUrl);
	}
	
	protected Document sendGetRequest(String path, Locale locale)
	throws IOException, ParserConfigurationException, SAXException
	{
		URL url = new URL(getServerUrl(), path);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setRequestMethod("GET");
		//con.setRequestProperty("Accept-Language", locale.toLanguageTag());
		//en-au,en-us;q=0.7,en;q=0.3
		con.setRequestProperty("Accept-Language", locale.toLanguageTag() + ";q=1.0");

		try (InputStream is = con.getInputStream())
		{
			return(documentBuilderFactory.newDocumentBuilder().parse(is, url.toExternalForm()));
		}
	}
	
	protected XPath getXPath()
	{
		return(xPath);
	}
	
	protected String documentToString(Document doc)
	throws IOException, TransformerException
	{
		StringWriter sw = new StringWriter();
		transformerFactory.newTransformer().transform(new DOMSource(doc), new StreamResult(sw));
		return(sw.toString());
	}
}
