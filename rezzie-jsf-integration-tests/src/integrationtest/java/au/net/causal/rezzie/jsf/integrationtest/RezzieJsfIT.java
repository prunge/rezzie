package au.net.causal.rezzie.jsf.integrationtest;

import static org.junit.Assert.*;

import java.util.Locale;

import javax.xml.xpath.XPath;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import au.net.causal.rezzie.locale.Countries;
import au.net.causal.rezzie.locale.Languages;

public class RezzieJsfIT extends IntegrationTestBase
{
	private static final Logger log = LoggerFactory.getLogger(RezzieJsfIT.class);
	
	private XPath xPath;
	
	@Before
	public void setUpXPath()
	{
		xPath = getXPath();
	}
	
	@Test
	public void testSimpleBundleResource()
	throws Exception
	{
		log.info("Simple integration test.");
		
		Document doc = sendGetRequest("simple1.xhtml", Locale.ENGLISH);
		//log.info("Result: " + documentToString(doc));
		
		String result = xPath.evaluate("/h:html/h:body/h:p", doc);
		assertEquals("Wrong result.", "Greeting: Good morning", result);
	}
	
	@Test
	public void testParameter()
	throws Exception
	{
		Document doc = sendGetRequest("param1.xhtml", Locale.ENGLISH);
		String result = xPath.evaluate("/h:html/h:body/h:p", doc);
		assertEquals("Wrong result.", "Counter: You have selected 5 items", result);
	}
	
	@Test
	public void testLocalePickup()
	throws Exception
	{
		Document doc = sendGetRequest("simple1.xhtml", new Locale(Languages.ENGLISH, Countries.AUSTRALIA));
		String result = xPath.evaluate("/h:html/h:body/h:p", doc);
		assertEquals("Wrong result.", "Greeting: G'day mate", result);
	}

}
