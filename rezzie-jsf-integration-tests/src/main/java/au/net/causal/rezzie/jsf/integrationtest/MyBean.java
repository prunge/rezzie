package au.net.causal.rezzie.jsf.integrationtest;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import au.net.causal.rezzie.jsf.AbstractRezzieBean;

@ManagedBean
@RequestScoped
public class MyBean extends AbstractRezzieBean
{
	private String name = "John Galah";
	private String localeName;
	
	@PostConstruct
	private void init()
	{
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		localeName = locale.toLanguageTag();
	}
	
	public String getLocaleName()
	{
		return(localeName);
	}
	
	public String name2()
	{
		return(name);
	}
	
	public String getName()
	{
		return(name);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String doSomething(String s)
	{
		return("Hello " + s);
	}
	
	public String doCounting(int value)
	{
		return("Count: " + (value + 1));
	}
}
