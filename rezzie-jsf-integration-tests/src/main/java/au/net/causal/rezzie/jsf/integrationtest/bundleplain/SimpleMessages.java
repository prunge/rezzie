package au.net.causal.rezzie.jsf.integrationtest.bundleplain;

import au.net.causal.rezzie.annotation.DefaultLocale;
import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.locale.Languages;

@DefaultLocale(language=Languages.ENGLISH)
@ResourceBundleTemplate
public interface SimpleMessages
{
	public String greeting();
	public String countMessage(int count);
}
