package au.net.causal.rezzie.jsf.integrationtest;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import au.net.causal.rezzie.jsf.integrationtest.bundle.jsf.RezzieMessages;

@ManagedBean
@RequestScoped
public class MyServiceBean implements Serializable
{
	@ManagedProperty("#{msg}")
	private RezzieMessages messages;
	
	public void setMessages(RezzieMessages messages)
	{
		this.messages = messages;
	}

	public String getServiceGreeting()
	{
		return("Greetings from the service layer: " + messages.getApp().getGreeting());
	}
}
