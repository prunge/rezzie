package au.net.causal.rezzie.jsf.integrationtest;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import au.net.causal.rezzie.jsf.AbstractRezzieBean;
import au.net.causal.rezzie.jsf.integrationtest.bundle.AppMessages;

@ManagedBean
@RequestScoped
public class MyProtoBean extends AbstractRezzieBean
{
	private final AppMessages bundle;
	
	public MyProtoBean()
	{
		bundle = getBundle(AppMessages.class);
	}
	
	public String getGreeting()
	{
		return(bundle.greeting());
	}
}
