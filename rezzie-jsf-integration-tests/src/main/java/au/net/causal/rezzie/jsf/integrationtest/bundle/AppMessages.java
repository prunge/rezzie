package au.net.causal.rezzie.jsf.integrationtest.bundle;

import java.util.Date;

import au.net.causal.rezzie.annotation.DefaultLocale;
import au.net.causal.rezzie.annotation.ResourceBundleTemplate;
import au.net.causal.rezzie.jsf.annotation.JsfConfiguration;
import au.net.causal.rezzie.jsf.annotation.JsfMasterBean;
import au.net.causal.rezzie.locale.Languages;

@DefaultLocale(language=Languages.ENGLISH)
@ResourceBundleTemplate
@JsfConfiguration(beanName="app")
@JsfMasterBean(beanName="msg")
public interface AppMessages
{
	public String greeting();
	public String countMessage(int count);
	public String dateMessage(Date date);
}
